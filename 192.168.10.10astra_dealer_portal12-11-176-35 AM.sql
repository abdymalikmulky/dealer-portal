# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 192.168.10.10 (MySQL 5.7.19-0ubuntu0.16.04.1)
# Database: astra_dealer_portal
# Generation Time: 2017-12-10 23:35:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dealers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dealers`;

CREATE TABLE `dealers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `company_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `PIC_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `PIC_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `PIC_phone` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dealers_user_id_foreign` (`user_id`),
  KEY `dealers_bdm_id_foreign` (`created_by`),
  CONSTRAINT `dealers_bdm_id_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `dealers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `dealers` WRITE;
/*!40000 ALTER TABLE `dealers` DISABLE KEYS */;

INSERT INTO `dealers` (`id`, `user_id`, `company_name`, `company_address`, `company_phone`, `PIC_name`, `PIC_email`, `PIC_phone`, `created_by`, `created_at`, `updated_at`)
VALUES
	(8,18,'PT Dealer Jos ROSO','Jalan merdeka','08123123121','asdfaf','asdfsad@gasd.com','123123',13,'2017-11-27 22:51:02','2017-12-10 14:25:01'),
	(10,19,'Dealer Yoman TBK','','','','','',13,'2017-11-27 22:51:25','2017-12-03 06:55:49'),
	(11,22,'PT Dealer Panutanque','Jalan jaksa negara','0812312334','Joko','jokos@gmail.com','08123123',20,'2017-12-09 07:09:41','2017-12-09 07:10:30');

/*!40000 ALTER TABLE `dealers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(6,'2017_10_07_132701_create_products_table',3),
	(7,'2017_10_07_133626_create_products_table',4),
	(8,'2017_10_07_133759_create_products_table',5),
	(9,'2017_10_07_143131_create_stocks_table',6),
	(10,'2017_10_07_143358_create_products_table',7),
	(11,'2017_10_07_143851_create_products_table',8),
	(13,'2017_10_08_123026_create_liga_reseller_product_table',10),
	(15,'2017_10_09_103809_create_resellers_table',12),
	(16,'2017_10_10_000612_create_sell_out_table',13),
	(43,'2014_10_12_000000_create_users_table',14),
	(44,'2014_10_12_100000_create_password_resets_table',14),
	(45,'2017_10_02_210928_create_roles_table',14),
	(46,'2017_10_02_211210_create_user_roles_table',14),
	(47,'2017_10_07_041232_create_status_table',14),
	(48,'2017_10_07_144205_create_products_table',14),
	(49,'2017_10_09_103501_create_dealers_table',14),
	(50,'2017_11_26_114359_create_user_detail',15);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `product_code` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `SN` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_code` (`product_code`),
  KEY `products_user_id_foreign` (`user_id`),
  CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `user_id`, `product_code`, `SN`, `name`, `type`, `photo`, `created_at`, `updated_at`)
VALUES
	(28,8,'S2110','999999','DocuCentre S2110','Unit',NULL,'2017-12-10 03:08:53','2017-12-10 03:08:53'),
	(29,8,'S2520','888888','DocuCentre S2520/S2320','Unit',NULL,'2017-12-10 03:08:53','2017-12-10 03:08:53'),
	(30,8,'5070','777777','DocuCentre-V 5070/4070','Unit',NULL,'2017-12-10 03:08:53','2017-12-10 03:08:53'),
	(31,8,'CT201594','1111','Supplies CT201594','Consumable',NULL,'2017-12-10 03:08:53','2017-12-10 03:08:53'),
	(32,8,'CT201593','2222','Supplies CT201593','Consumable',NULL,'2017-12-10 03:08:53','2017-12-10 03:08:53'),
	(33,8,'CT2015929','9999','Supplies CT2015929','unit',NULL,'2017-12-10 03:08:53','2017-12-10 03:53:40');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'SUPERADMIN','SUPERMAN SYSTEM',NULL,'2017-10-07 10:16:53'),
	(2,'PM','Product Marketing',NULL,NULL),
	(3,'COS','Channel Operational Support',NULL,NULL),
	(4,'BDM','BDM',NULL,NULL),
	(5,'Dealer','Dealer client astra','2017-11-26 08:41:35','2017-11-26 08:41:35');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;

INSERT INTO `status` (`id`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'ACTIVE','OBJECT ACTIVE',NULL,NULL),
	(2,'NOT_ACTIVE','OBJECT NOT ACTIVE',NULL,NULL),
	(3,'CLOSED','OBJECT CLOSED',NULL,NULL),
	(4,'PENDING','OBJECT PENDING',NULL,NULL),
	(5,'REJECTED','OBJECT REJECTED',NULL,NULL),
	(6,'APPROVED','OBJECT APPROVED',NULL,NULL);

/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `NIP` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_details_user_id_foreign` (`user_id`),
  KEY `user_details_created_by_foreign` (`created_by`),
  CONSTRAINT `user_details_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `user_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;

INSERT INTO `user_details` (`id`, `user_id`, `NIP`, `phone`, `photo`, `address`, `information`, `created_by`, `created_at`, `updated_at`)
VALUES
	(5,8,'1456543-23','08123128438','photo/rCrNlhHZm56xpUIcV9kjXQ8TKCnclt46cSl5Gg5T.png','pakmamanjos',NULL,1,'2017-11-26 13:06:22','2017-12-09 07:15:12'),
	(8,11,'1119990005','081231234925','photo/default.png','Jalan Rayas','-',8,'2017-11-26 13:09:20','2017-12-02 05:37:22'),
	(9,12,'77556622','08123758343','photo/default.png','Jalan merdeka',NULL,8,'2017-11-26 13:10:25','2017-12-03 06:24:53'),
	(10,13,'855759','0812381284','photo/default.png','Jalan manggkuasdf',NULL,11,'2017-11-26 13:14:18','2017-11-26 13:14:18'),
	(12,14,'8582939123912','081231238124','photo/default.png','Jalanan aja',NULL,11,'2017-11-26 13:15:18','2017-11-26 13:15:18'),
	(13,15,'9585958','018239124184','photo/84x6T4E6Svpj8CMYt9xPpNiK5oM3Swj0EE8mmxim.jpeg','jalan raya',NULL,12,'2017-11-26 13:17:08','2017-12-09 06:55:25'),
	(14,20,'343354','3432423','photo/fY5W6P8O1EeQbB3p51IPLMKDj8vuDg0q5TMUjZ68.jpeg','bdmbarat2',NULL,12,'2017-12-09 06:02:44','2017-12-09 07:01:23'),
	(16,23,'083453485','081238545845','photo/Ml6mtp5otnMZUWBUAFao0qRdb99brsna5Zp4Djst.jpeg','costenggara',NULL,8,'2017-12-09 07:11:47','2017-12-09 07:12:33');

/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_roles_user_id_foreign` (`user_id`),
  KEY `user_roles_role_id_foreign` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;

INSERT INTO `user_roles` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`)
VALUES
	(3,1,1,NULL,NULL),
	(8,8,2,'2017-11-26 13:06:22','2017-11-26 13:06:22'),
	(13,13,4,'2017-11-26 13:14:18','2017-11-26 13:14:18'),
	(15,14,4,'2017-11-26 13:15:18','2017-11-26 13:15:18'),
	(29,11,3,'2017-12-02 05:37:22','2017-12-02 05:37:22'),
	(31,18,5,'2017-12-02 05:38:55','2017-12-02 05:38:55'),
	(32,12,3,'2017-12-03 06:24:53','2017-12-03 06:24:53'),
	(33,19,5,'2017-12-03 06:55:49','2017-12-03 06:55:49'),
	(46,15,4,'2017-12-09 06:55:25','2017-12-09 06:55:25'),
	(47,20,4,'2017-12-09 06:56:05','2017-12-09 06:56:05'),
	(49,22,5,'2017-12-09 07:10:30','2017-12-09 07:10:30'),
	(50,23,3,'2017-12-09 07:11:47','2017-12-09 07:11:47');

/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `name`, `email`, `password`, `status_id`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'superman','JL Superman','super@astra.com','$2y$10$2lD3GmvrA6bPm6o787avTOd/QS5JSWyaicUBkhQLYO03R4PbCmKpy',1,'3No1ria8b37hz2xEJ9E3B8k8JHn6w8f9VLpTHsHQlFZBDSb9XjAltmrWHzYs','2017-10-02 21:19:34','2017-10-09 10:23:32'),
	(8,'pakmamanjos','Pak Maman','pakmaman@astra.com','$2y$10$ozJx7txKFDBvqpW8Ee9h2enrCVLCxylBcSS/7Y3Jz2pUF.4h6DIm2',1,'qDC1uQqFzaBUyYAUTtUL4iO0aqHDt3jgyTLRWC7zSb6VGVvLNyX3RnMd459W','2017-11-26 13:06:22','2017-12-09 07:15:12'),
	(11,'costimurs','Cos Timurs','cos-timurs@mailinator.com','$2y$10$wxTx/lu21kB22XXs.ulycOO6P5T9qhgxZTCD.1cIj.LDvDFw2cG7e',1,'yPyXLmyq7oFhs68nulVG2aZi7JWVUUjpGVObx2kL8Q2eImt5jC5bdvZwE2KR','2017-11-26 13:09:20','2017-12-02 05:34:29'),
	(12,'cosbaratdaya','Cos Barat Daya','cos-barat@mailinator.com','$2y$10$K0KLhqCkiIjx/wE/EHQG1e61OOgeWV6Gezz32ysOcDupiJ4r1leiG',1,'6SZI91n402ZqTh1NGxFeZE550hLTqp1MCod4bffDsSJSKrxNNK9umi9yWOrz','2017-11-26 13:10:25','2017-12-03 06:24:53'),
	(13,'bdmtimur1','BDM Timur Jos','bdmtimur1@mailinator.com','$2y$10$u4V8mODtdDRrgNVgUwBBMeoU/7RNSuR9RSWqmTyKXBxFjLQIZ1/Um',1,'dntHboMvcp5sey5ZA6eZByydTxXtr6TGkADj0xP6fONBucvKmioUTPjXCgNw','2017-11-26 13:14:18','2017-11-26 13:14:18'),
	(14,'bdmtimur2','BDM Timur Cuy','bdmtimur2@mailinator.com','$2y$10$8zeMxGYA2d.D9jxPL56SrelC7rxp1sEv5bru2kNMHGvv15ffm0erC',1,NULL,'2017-11-26 13:15:03','2017-11-26 13:15:18'),
	(15,'bdmbarat1','BDM Barat 1','bdmbarat1@mailinator.com','$2y$10$qy4sN0nIUmROAsjI5anene4hRvriEidf9CuCwZ1HgC.ITIJDxpZLi',1,'g64mvQvcHHOEptCxIqPPqrP51gZCBFlvQnFiGrB8rOcwwJDogbXbgVQopkk7','2017-11-26 13:17:08','2017-11-26 13:17:08'),
	(18,'dealerjos','PT Dealer Jos ROSO','asdfsad@gasd.com','$2y$10$fBiZkuqa0F7cFtPZlA3bfOhHhTYS9Le1YNjtR8ybpMn3CbIt6HY7.',1,'rOvKefwLWdtPikUdL3OAiyk9zpJdl4at9Wna6KZtXakUr7n0JQYUjyxVpNhx','2017-11-27 22:32:34','2017-12-10 14:25:01'),
	(19,'bambangdealer','Dealer Yoman TBK','bambang@mailinator.com','$2y$10$J.AZm83XhIUf02rAjcp8T.uEy8sZO25Ly7uTzQ9hSRtVUhPyF1B1m',1,'nIZzDTENZhk7zRwDUy3Sav13hjupTN0kpc66zZZyMTYYxPz4acIkcrd6iHxP','2017-11-27 22:33:44','2017-12-03 08:03:50'),
	(20,'bdmbarat2','Jokowi','bdmbarat2@mailinator.com','$2y$10$3Jy1hrZOy0rIhe.wZQkaJOfmLp7r/WpeDZrn6ODIHMRSdegO0Czr.',1,'uEeb3CwbV5f9Txho079sgkhp3dST5YRTksMcHElDJvVb74bp0LfNbQUQuHVA','2017-12-09 06:02:44','2017-12-09 07:01:23'),
	(22,'dealerakannya','PT Dealer Panutanque','jokos@gmail.com','$2y$10$.VhWC8/J8kDb7pVEQMQDrOMhcuKQEZ.ZUt2Ks7lLuASEYzxXvP.ZG',1,NULL,'2017-12-09 07:09:41','2017-12-09 07:10:30'),
	(23,'costenggara','Cos Tenggara Jos','costenggara@mailinator.com','$2y$10$GkPAiCNSDaRu.BIZDpDPJ.q.gC7n3Obyp3TMMVUAxqeV9OJhQhaRG',1,'PEnSol0yP4hUnY1ikvIevrBEsiKZcbahYX7k9973Q0BuQpFcGS0xA5Eep5PQ','2017-12-09 07:11:47','2017-12-09 07:12:33');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
