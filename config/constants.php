<?php

    //URL
    define('ADMIN_PREFIX_URL', 'artsanimda/');

    // SESSION KEY
    define('SESSION_LOGIN_SUCCESS', 'login_success');
    define('SESSION_LOGIN_MESSAGE', 'login_message');

    return array(
        'SUPERADMIN' => 1,
        'PM' => 2,
        'COS' => 3,
        'BDM' => 4,
        'DEALER' => 5,
        'ACTIVE' => 1,
        'NOT_ACTIVE' => 2,
        'CLOSED' => 3,
        'PENDING' => 4,
        'REJECTED' => 5,
        'APPROVED' => 6
    );



?>