<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//For storage url
use Illuminate\Support\Facades\Auth;

Route::get('storage/{type}/{filename}', function ($type, $filename)
{

   $path = storage_path('app/' . $type ."/". $filename);


    if (!File::exists($path)) {
        abort(404);

    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

//Front
Route::get('/', 'HomeController@index')->name('home');

//ADMIN
Auth::routes();
Route::group(['middleware' => 'admin'], function () {

	Route::get(ADMIN_PREFIX_URL, 'admin\DashboardController@index')->name('dashboard');

	//User
    Route::get(ADMIN_PREFIX_URL . 'userparent/{parentId}/{roleId}', 'admin\UserController@getUserByParent')->name('user.parent');
    Route::get(ADMIN_PREFIX_URL . 'profile', 'admin\UserController@profile')->name('profile');
    Route::patch(ADMIN_PREFIX_URL . 'updatePassword/{dealerId}', 'admin\UserController@updatePassword')->name('user.update.password');
	Route::resource(ADMIN_PREFIX_URL . 'user', 'admin\UserController', [
	    'names' => [
	        'index' => 'user',
	        'create' => 'user.create',
	        'show' => 'user.show',
	        'store' => 'user.store',
	        'edit' => 'user.edit',
	        'update' => 'user.update',
	        'destroy' => 'user.delete'
	    ]
	]);

    //Role
    Route::resource(ADMIN_PREFIX_URL . 'role', 'admin\RoleController', [
        'names' => [
            'index' => 'role',
            'create' => 'role.create',
            'store' => 'role.store',
            'edit' => 'role.edit',
            'update' => 'role.update',
            'destroy' => 'role.delete'
        ]
    ]);


    //Product
    Route::post(ADMIN_PREFIX_URL . 'product/import', 'admin\ProductController@import')->name('product.import');
    Route::get(ADMIN_PREFIX_URL . 'product/category/{category}', 'admin\ProductController@productByCategory')->name('product.by.category');
    Route::resource(ADMIN_PREFIX_URL . 'product', 'admin\ProductController', [
        'names' => [
            'index' => 'product',
            'create' => 'product.create',
            'store' => 'product.store',
            'edit' => 'product.edit',
            'update' => 'product.update',
            'destroy' => 'product.delete'
        ]
    ]);

    //Sell Through
    Route::get(ADMIN_PREFIX_URL . 'sellthrough/product/{category}', 'admin\SellThroughController@sellThroughProductByCategory')->name('sellthrough.product.category');
    Route::post(ADMIN_PREFIX_URL . 'sellthrough/import', 'admin\SellThroughController@import')->name('sellthrough.import');
    Route::get(ADMIN_PREFIX_URL . 'sellthrough/importExcel', 'admin\SellThroughController@importExcel')->name('sellthrough.import.excel');
    Route::get(ADMIN_PREFIX_URL . 'sellthrough/detail/create/{sellThroughId}', 'admin\SellThroughController@createDetail')->name('sellthrough.detail.create');
    Route::post(ADMIN_PREFIX_URL . 'sellthrough/detail/store/{sellThroughId}', 'admin\SellThroughController@storeDetail')->name('sellthrough.detail.store');
    Route::get(ADMIN_PREFIX_URL . 'sellthrough/detail/edit/{sellThroughDetailId}', 'admin\SellThroughController@editDetail')->name('sellthrough.detail.edit');
    Route::patch(ADMIN_PREFIX_URL . 'sellthrough/detail/update/{sellThroughId}/{sellThroughDetailId}', 'admin\SellThroughController@updateDetail')->name('sellthrough.detail.update');
    Route::delete(ADMIN_PREFIX_URL . 'sellthrough/detail/delete/{sellThroughId}/{sellThroughDetailId}', 'admin\SellThroughController@destroyDetail')->name('sellthrough.detail.delete');
    Route::resource(ADMIN_PREFIX_URL . 'sellthrough', 'admin\SellThroughController', [
        'names' => [
            'index' => 'sellthrough',
            'create' => 'sellthrough.create',
            'store' => 'sellthrough.store',
            'edit' => 'sellthrough.edit',
            'update' => 'sellthrough.update',
            'destroy' => 'sellthrough.delete'
        ]
    ]);


    //Stock of product
    Route::resource(ADMIN_PREFIX_URL . 'stockproduct', 'admin\StockOfProductController', [
        'names' => [
            'index' => 'stockproduct',
            'create' => 'stockproduct.create',
            'store' => 'stockproduct.store',
            'edit' => 'stockproduct.edit',
            'update' => 'stockproduct.update',
            'destroy' => 'stockproduct.delete'
        ]
    ]);


    //Sell Out
    Route::get(ADMIN_PREFIX_URL . 'sellout/create/addproduct', 'admin\SellOutController@addProduct')->name('sellout.create.addproduct');
    Route::post(ADMIN_PREFIX_URL . 'sellout/store/product', 'admin\SellOutController@storeProduct')->name('sellout.store.product');
    Route::get(ADMIN_PREFIX_URL . 'sellout/delete/product/{index}', 'admin\SellOutController@deleteProduct')->name('sellout.delete.product');
    
    Route::get(ADMIN_PREFIX_URL . 'sellout/delete/document/{id}', 'admin\SellOutController@deleteDocument')->name('sellout.delete.document');

    Route::resource(ADMIN_PREFIX_URL . 'sellout', 'admin\SellOutController', [
        'names' => [
            'index' => 'sellout',
            'create' => 'sellout.create',
            'store' => 'sellout.store',
            'edit' => 'sellout.edit',
            'update' => 'sellout.update',
            'destroy' => 'sellout.delete'
        ]
    ]);

    //Achievement
    Route::get(ADMIN_PREFIX_URL . 'achievement/report', 'admin\AchievementController@report')->name('achievement.report');
    Route::resource(ADMIN_PREFIX_URL . 'achievement', 'admin\AchievementController', [
        'names' => [
            'index' => 'achievement',
            'create' => 'achievement.create',
            'store' => 'achievement.store',
            'edit' => 'achievement.edit',
            'update' => 'achievement.update',
            'destroy' => 'achievement.delete'
        ]
    ]);

    //API Achievement
    Route::get(ADMIN_PREFIX_URL . 'achievement/report/yearly', 'admin\AchievementController@getAchievementYearly')->name('achievement.yearly');
    Route::get(ADMIN_PREFIX_URL . 'achievement/report/quarterly/{year}', 'admin\AchievementController@getAchievementQuarterly')->name('achievement.quarterly');
    Route::get(ADMIN_PREFIX_URL . 'achievement/report/annually/{year}', 'admin\AchievementController@getAchievementAnnually')->name('achievement.anually');
    Route::get(ADMIN_PREFIX_URL . 'achievement/report/byrole/{role}', 'admin\AchievementController@getAchivementByRole')->name('achievement.byrole');
    Route::get(ADMIN_PREFIX_URL . 'achievement/report/data/{responseType}/{userId}/{year}/{month}/{tren}', 'admin\AchievementController@getAchievementReport')->name('achievement.data.report');
    Route::get(ADMIN_PREFIX_URL . 'achievement/report/data/table', 'admin\AchievementController@getReportTable')->name('achievement.data.report.table');





    //Marketing Program
    Route::resource(ADMIN_PREFIX_URL . 'slide', 'admin\SlideController', [
        'names' => [
            'index' => 'slide',
            'create' => 'slide.create',
            'store' => 'slide.store',
            'edit' => 'slide.edit',
            'update' => 'slide.update',
            'destroy' => 'slide.delete'
        ]
    ]);

    //Tips & Trick
    Route::resource(ADMIN_PREFIX_URL . 'tipstrick', 'admin\TipsTrickController', [
        'names' => [
            'index' => 'tipstrick',
            'create' => 'tipstrick.create',
            'store' => 'tipstrick.store',
            'edit' => 'tipstrick.edit',
            'update' => 'tipstrick.update',
            'destroy' => 'tipstrick.delete'
        ]
    ]);

    //Sales Tools
    Route::resource(ADMIN_PREFIX_URL . 'salestool', 'admin\SalesToolController', [
        'names' => [
            'index' => 'salestool',
            'create' => 'salestool.create',
            'store' => 'salestool.store',
            'edit' => 'salestool.edit',
            'update' => 'salestool.update',
            'destroy' => 'salestool.delete'
        ]
    ]);


    //Messages
    Route::get(ADMIN_PREFIX_URL . 'message/sent', 'admin\MessageController@sent')->name('message.sent');
    Route::resource(ADMIN_PREFIX_URL . 'message', 'admin\MessageController', [
        'names' => [
            'index' => 'message',
            'create' => 'message.compose',
            'store' => 'message.store',
            'edit' => 'message.edit',
            'update' => 'message.update',
            'destroy' => 'message.delete'
        ]
    ]);


    //Notification
    Route::get(ADMIN_PREFIX_URL . 'getNotifications', function() {
        return view('admin.layouts.notifications.list_notification');
    })->name('notification.lists');
    Route::get(ADMIN_PREFIX_URL . 'markAsRead/{id?}', function($id = ''){
        if($id != '') {
            echo Auth::user()->unreadNotifications->where('id', $id)->markAsRead();
        }

    });

});
