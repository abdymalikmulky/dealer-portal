$(document).ready(function(){

    /*[BEGIN PROFILE]*/
	$('.profile-edit').click(function(e) {
        hideShowInputEditFormProfile(true)

		e.preventDefault();
	});
	$('.profile-save-cancel').click(function(e) {
        hideShowInputEditFormProfile(false)
		e.preventDefault();
	});


    if( $('.profile_error_input').length > 0) {
        hideShowInputEditFormProfile(true)
    }


	function hideShowInputEditFormProfile(show) {
        $('.profile-box-edit').show();
        $('.profile-box-save').hide();

        $('.profile-page .data-value').show();
        $('.profile-page .data-input').hide();

        if(show) {
            $('.form-add-website').show();
            
            $('.profile-box-edit').hide();
            $('.profile-box-save').show();

            $('.profile-page .data-value').hide();
            $('.profile-page .data-input').show();
        }
	}

    $('.add-website').click(function() {
        $boxFormWebsite = $('#box-form-website').clone();
        // $boxFormWebsite.find('.show-website').remove();
        $('.form-website').append($boxFormWebsite);
        return false;
    });

    $(document).on("keyup", 'input[type=number]', function(e) {
        if ($(this).val().length == 1 && e.which == 48 ){
          $(this).val('');
        }
        if ($(this).val() < 0){
          $(this).val('');
        }
    });
    $(document).on("keydown", 'input[type=number]', function(e) {
        

        
        if(e.which == 190 || $(this).val() == ".") {
            // console.log($(this).val());
            // $(this).val($(this).val().replace(".", ""));
            e.preventDefault();
        }

    });

    $(document).on("keyup", '.company_website', function() {
        $val = $(this).val();
        $(this).next().attr('href', $val);
    });
    $(document).on("click", '.delete-company-website', function() {
        var $confirmation = confirm('Are you sure want to delete this website?');
        if($confirmation) {
            if($('.company_website').length>1) {
                $(this).parent().remove();
                console.log($(this).parent().prev().html());
            } else {
                alert('Webiste is required')
            }
        }
        return false;
    });

    /*[END PROFILE]*/


    /*[BEGIN PRODUCT]*/
    $formProduct = $('.form-list-product');
    
    $formProductContent = $('.form-list-product').html();
    $formProductSnContent = $('.form-sn').html();
    $formProductTitle = $('.field-list-product legend').html();
    $totalProduct = 1;
    
    loadDataProduct();

    //Add And Remove Product
    $('.add-product').click(function() {
        addProduct($(this));

        //Remove Product
        $('.remove-product').click(function() {
            removeProduct($(this));
            return false;
        });

        //Add SN
        $('.sellthrough-qty').keyup(function() {
            addProductSerialNumbers($(this));
            return false;
        });

        //change product category
        $('.list-product-type').change(function() {
            $this = $(this);
            $value = $this.val();

            loadDataProduct($value, $this);

            return false;
        });

        return false;
    });

    //add product item
    $('.add-product-item').click(function(){ 
        $this = $(this);
        $parentBox = $this.parent().parent().parent();

        addProductItem($this);

        $qty = $parentBox.find('.sellthrough-qty').html();
        $revenuePerProduct = $parentBox.find('.sellthrough-revenue').val();
        
        setRevenueTotal($parentBox, $qty, $revenuePerProduct);

        return false;
    });
    $(document).on("click", '.product-item-delete', function() {
        $this = $(this);
        $parentBox = $this.parent().parent().parent().parent().parent();

        deleteProductItem($this);

        $qty = $parentBox.find('.sellthrough-qty').html();
        $revenuePerProduct = $parentBox.find('.sellthrough-revenue').val();

        setRevenueTotal($parentBox, $qty, $revenuePerProduct);

        return false;
    });

    function addProductItem($this) {
        $('.form-product-sn').append($formProductSnContent);
        updateProductQTY();
    }
    function deleteProductItem($this) {
        $this.parent().parent().parent().remove();
        updateProductQTY();
    }
    function updateProductQTY() {
        $formSNCount = $('.serial-number').length-1;
        $('.product-qty').html($formSNCount);
    }

    //Add SN
    $(document).on("keyup", '.sellthrough-qty', function() {
        $parentBox = $(this).parent().parent().parent();
        $qty = $(this).val();
        $revenuePerProduct = $parentBox.find('.sellthrough-revenue').val();

        addProductSerialNumbers($(this));
        setRevenueTotal($parentBox, $qty, $revenuePerProduct);

        return false;
    });

    $(document).on("keyup", '.sellthrough-qty-nonunit', function() {
        $parentBox = $(this).parent().parent().parent();
        $qty = $(this).val();
        $revenuePerProduct = $parentBox.find('.sellthrough-revenue').val();

        setRevenueTotal($parentBox, $qty, $revenuePerProduct);

        return false;
    });


    //Set Revenue Total
    $(document).on("keyup", '.sellthrough-revenue', function() {
        $parentBox = $(this).parent().parent().parent();
        $qty = $parentBox.find('.sellthrough-qty').val();
        if ($qty === "") {
            $qty = $parentBox.find('.sellthrough-qty').html();
        }
        if($qty==null) {
            $qty = $parentBox.find('.sellthrough-qty-nonunit').val();
        }
        $revenuePerProduct = $(this).val();
        
        setRevenueTotal($parentBox, $qty, $revenuePerProduct);
    });
    
    //change product category
    $('.list-product-type').change(function() {
        $this = $(this);
        $value = $this.val();

        loadDataProduct($value, $this);
    });

    function addProduct($this) {
        $totalProduct = $('div.form-list-product div.form-item-product').length + 1;
        $('.form-total-product').html($totalProduct);
        $formProduct.append($formProductContent);

        $productType = $('.form-item-product').last().find('#product_type').val();
        
        loadDataProduct($productType);
    }
    function addProductSerialNumbers($this) {
        $parentBox = $this.parent().parent().parent();
        $productSNBox = $parentBox.find('.form-product-sn');
        var $indexParent = $('.form-item-product').index($this.parent().parent().parent());
        if($indexParent < 0) {
            $indexParent = 0;
        }
        console.log("INDEX " + $indexParent);
        var $formCount = $this.val();
        $productSNBox.html('');
        for(var i=0; i<$formCount; i++) {
            $productSNBox.append($formProductSnContent);
        }
        $productSNBox.find('input.serial-number')
        .attr('name', 'sn['+$indexParent+'][]');
    }
    function removeProduct($this) {
        $this.parent().parent().remove();
        $totalProduct = $('div.form-list-product div.form-item-product').length; 
        $('.form-total-product').html($totalProduct); 
    }
    function setRevenueTotal($parentBox, $qty, $revenue) {
        console.log("RESULT" + $qty + "-" +$revenue);

        $totalRevenueElement = $parentBox.find('.sellthrough-totalrevenue');
        $totalRevenue = $qty * $revenuePerProduct;
        $totalRevenueElement.html(convertToRupiah($totalRevenue));
    }

    function loadDataProduct($productType = "", $thisElement = null) {
        $urlPrefix = '/product/category/';
        if($productType=="") {
            $formProductCategory = $('#product_type').val();
        } else {
            $formProductCategory = $productType;
        }

        

        $.ajax({
            url : $('.base_url').val() + $urlPrefix + $formProductCategory,
            type : 'GET',
            success : function(data) {
                renderDataProduct(data, $thisElement);
                
                setTimeout(function(){
                    $('.select-filter').multiselect({
                        enableFiltering: true,
                        enableCaseInsensitiveFiltering: true,
                        maxHeight: 200,
                        buttonWidth: '100%',
                        buttonClass: 'btn btn-default'
                    })
                }, 1000);
            }
        });
    }

    function execDataProductDumm($totalProduct, $urlPrefixFunction) {
        $formProduct.show();

        var $total = $totalProduct.val();
        $formProductCategory = $('#product_type').val();
        $formProduct = $('.form-list-product');

        if($total < 0) {
            $total = 1;
            $totalProduct.val(1);
        }
        $formProduct.html('');

        $('.field-list-product legend').html($formProductTitle + ' ( ' + $total + ' Product)')
        for (var i = 0; i < $total; i++) {
            $formProduct.append($formProductContent + "<hr/>");
        }
        $.ajax({
            url : $('.base_url').val() + $urlPrefixFunction + $formProductCategory,
            type : 'GET',
            success : function(data) {
                if($urlPrefixFunction == '/product/category/') {
                    renderDataProduct(data)
                } else {
                    renderDataDealerProductStock(data)
                }

                setTimeout(function(){
                    $('.select-filter').multiselect({
                        enableFiltering: true,
                        enableCaseInsensitiveFiltering: true,
                        maxHeight: 200,
                        buttonClass: 'btn btn-default'
                    });
                }, 1000);
            }
        });
    }

    function renderDataDealerProductStock(data) {
        
        $.each( data, function( key, product ) {
            $('.list-product').append('<option  value="' + product.id + '">' 
                + product.product_code + ' - ' + product.type + ' - ' 
                + product.qty +'</option>')
        });

    }
    function renderDataProduct(data, $thisElement) {
        var selectElement = $('.list-product');
        var isSelected = "";
        var productId = selectElement.attr('data-product-id');

        if($thisElement!=null) {
            $thisElement.parent().parent().next().find('.list-product').html('');
        }
        $.each( data, function( key, product ) {
            isSelected = "";
            if(product.id == productId) {
                isSelected = "selected";
            } 

            if($thisElement==null) {
                $('.list-product').append('<option '+isSelected+' value="' + product.id + '">' + product.product_code + ' - ' + product.type + '</option>');
            }else {
                $thisElement.parent().parent().next().find('.list-product').append('<option '+isSelected+' value="' + product.id + '">' + product.product_code + ' - ' + product.type + '</option>')
            }
        });

    }
    /*[END PRODUCT]*/


    /*[BEGIN SELLOUT]*/
    $('#product_stock').change(function(){
        $productCode = $(this).find(':selected').attr('product_code');
        $productQty = $(this).find(':selected').attr('stock_remaining');
        $('#submit_add_product').removeAttr('disabled');

        if($(this).val() == ""){
            $('#desc_product').hide();
        }else {
            $('#desc_product').show();
            $('#desc_product_code').html($productCode);
            $('#desc_product_remaining').html($productQty);

            //$('#qty').val($productQty);
            $("#qty").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
            $('#qty').bind('keyup mouseup', function () {
                console.log($(this).val());
                console.log($productQty);
                $thisVal = parseInt($(this).val());
                if($(this).val() == "") {
                    $thisVal = 0;
                }
                if($thisVal > parseInt($productQty)) {
                    console.log(true);
                    $(this).parent().parent().addClass('has-error has-feedback')
                    $(this).after('<span class="fa fa-close form-control-feedback add-product-feedback" aria-hidden="true">');
                    $('#desc_product').addClass('text-danger');

                    $('#submit_add_product').attr('disabled', 'disabled');

                } else {
                    console.log(false);
                    $(this).parent().parent().removeClass('has-error has-feedback')
                    $('.add-product-feedback').remove();
                    $('#desc_product').removeClass('text-danger');

                    $('#submit_add_product').removeAttr('disabled');
                }
                $('#desc_product_remaining').html($productQty-$thisVal);
            });
        }
    });
    $('.sellout-sn').change(function() {
        $this = $('.sellout-sn option:selected');
        var $productId = $this.attr('product_id');
        var $productType = $this.attr('product_type');
        var $productCode = $this.attr('product_code');
        $('.product-by-sn').html($productType + ' - ' + $productCode);
        $('#product_code').val($productId);
    }); 


    /*[END SELLOUT]*/

    

    /*[BEGIN NOTIFICATION]*/
    notificationReadStatusActionTrigger();
    
    setInterval(function(){ 
        loadNotification();
    }, 3000);
    
    function notificationReadStatusActionTrigger() {
        $('.notification').click(function(e){
            $baseUrl = $('.base_url').val();
            $urlPage = $(this).attr('url');
            $notificationId = $(this).attr('id');
            //mark as read
            $.get($baseUrl + '/markAsRead/' + $notificationId, function(data) {

                window.location.href = $urlPage;
            });
            e.preventDefault();
        });
    }

    function loadNotification() {
        $.ajax({
            url : $('.base_url').val() + '/getNotifications',
            type : 'GET',
            success : function(data) {
                console.log(data);
                
                $('.menu-notification').html(data);

                $notificationCount = $('.menu-notification').find('li').attr('notificationCount');
                $('.count-notification').html($notificationCount);

                notificationReadStatusActionTrigger()
            }
        });
    }
    /*[END NOTIFICATION]*/


    $('.date_input').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        todayHighlight: true
    });

    $('.year_input').datepicker({
        format: "yyyy",
        autoclose: true,
        todayHighlight: true,
        viewMode: "years",
        minViewMode: "years"
    });




    /*[BEGIN ACHIEVEMENT]*/
    $defaultValueBDM = $('.achievement-filter-default-bdm').html();
    $defaultValueBP = $('.achievement-filter-default-bp').html();
    $('.achievement-filter-cos').change(function() {
        getUserParent($(this));
    });
    $('.achievement-filter-bdm').change(function() {
        getUserParent($(this));
    });

    function getUserParent($this) {
        $thisVal = $this.val();
        $roleVal = $this.attr('role');

        $parentUserParentId = "";
        if($roleVal==4) { //BDM
            $parentUserParentId = $('.achievement-filter-cos').val();
        }
        
        if($thisVal!="") {
            $.ajax({
                url : $('.base_url').val() + '/userparent/' + $thisVal + '/' + $roleVal,
                data : {'parent_user_parent_id': $parentUserParentId},
                type : 'GET',
                success : function(data) {
                    if(data.length > 0) {
                        if($roleVal==3) { //COS
                            $('.achievement-filter-bdm').html($defaultValueBDM)
                        }
                        if($roleVal<=4) { //BDM
                            $('.achievement-filter-bp').html($defaultValueBP)
                        }
                        $.each( data, function( key, userData ) {
                            console.log(userData);
                            if($roleVal==3) { //COS
                                $('.achievement-filter-bdm').append('<option value="' + userData.id + '">' + userData.name +'</option>')
                            }
                            if($roleVal==4) { //BDM
                                $('.achievement-filter-bp').append('<option value="' + userData.id + '">' + userData.company_name +'</option>')
                            }
                        });
                    } else {
                        if($roleVal==3) { //COS
                            $('.achievement-filter-bdm').html('<option value="">Dont have BDM</option>')
                            $('.achievement-filter-bp').html('<option value="">Dont have Business Partner</option>')
                        }
                        if($roleVal==4) { //BDM
                            $('.achievement-filter-bp').html('<option value="">Dont have Business Partner</option>')
                        }
                    }
                }
            });
        } else {
            if($roleVal==3) { //COS
                $('.achievement-filter-bdm').html('<option value="">Choose COS first</option"');
            }
            if($roleVal==4) { //BDM
                $('.achievement-filter-bp').html('<option value="">Choose BDM first</option"');
            }
        }
    }
    $('.box-achievement-detail').click(function() {
        $('.box-achievement-warning-charttable').hide();
        $('.box-achievement-warning-charttable').next().hide();
        $('.box-achievement-warning-charttable').next().fadeIn();

        var target = $(this).attr('target');
        var category = $(this).attr('category');
        var categoryColor = "#e74c3c"; //consumable
        
        var actualValue = $(this).attr('actual_value');
        var planValue = $(this).attr('plan_value');
        var actualLYValue = $(this).attr('last_year_value');

        var dataAchievement = $(this).attr('data-value');


        // CHART
        if(category=="unitmono") {
            categoryColor = "#3498db";
        } else if(category=="unitcolor") {
            categoryColor = "#1abc9c";
        } else if(category=="unitmonocolor") {
            categoryColor = "#f39c12";
        }

        var data = [{data: [[0, 0]], color: "rgba(255,255,255,0)"}, 
            {data: [[1, actualLYValue]], color: categoryColor}, 
            {data: [[2, planValue]], color: categoryColor},
            {data: [[3, actualValue]], color: categoryColor},
            {data: [[4,0]], color: "rgba(255,255,255,0)"}];
        $.plot("#bar-chart", data, {
          grid: {
            borderWidth: 1,
            borderColor: "#f3f3f3",
            tickColor: "#f3f3f3"
          },
          series: {
            bars: {
              show: true,
              barWidth: 0.7,
              align: "center",
              lineWidth: 0,
                fillColor: {
                    colors: [{
                        opacity: 1.0
                    }, {
                        opacity: 1.0
                    }]
                }
            }
          },
          xaxis: {
            mode: "categories",
            tickLength: 0,
            ticks: [[0,""],[1,"Last Year"],[2,"Plan"],[3,"Actual"],[4,""]]
          }
        });

        //TABLE
        $.ajax({
            url : $('.base_url').val() + '/achievement/report/data/table',
            type : 'GET',
            data : {'data' : dataAchievement, 'target': target},
            success : function(data) {
                $('.box-achievement-report-table').html(data);
            }
        });


        // DEsign
        $('.box-achievement-charttable').css('border-top-color', categoryColor); //color resume chart table

        //pembeda clicked/active
        $('.small-box').css('opacity', '0.7'); 
        $('.box-achievement-icon').css('color', 'rgba(0, 0, 0, 0.15)');
        $('.box-achievement-icon').css('font-size', '60px'); 
        $('.small-box > .inner').css('font-weight', 'normal'); 
        $('.small-box > .inner h3').css('font-weight', 'normal'); 

        $(this).parent().css('opacity', '1'); 
        $(this).prev().prev().css('color', 'rgba(0, 0, 0, 0.4)'); 
        $(this).prev().prev().css('font-size', '75px'); 
        $(this).prev().prev().prev().css('font-weight', 'bold'); 
        $(this).prev().prev().prev().children().css('font-weight', 'bold'); 
        return false;
    });    
    /*[END ACHIEVEMENT]*/



    function convertToRupiah(angka)
    {
        var rupiah = '';        
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
    }

    /**
     * Usage example:
     * alert(convertToRupiah(10000000)); -> Rp. 10.000.000
     */
     
    function convertToAngka(rupiah)
    {
        return parseInt(rupiah.replace(/[^0-9]/g, ''), 10);
    }

    // FORM URL
    
});
function checkURL($url) {
  var string = $url.value;
  if (!~string.indexOf("http")) {
    string = "http://" + string;
  }
  $url.value = string;
  return $url
}