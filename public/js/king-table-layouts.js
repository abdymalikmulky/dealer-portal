$(document).ready(function(){

    // $('#table-data').DataTable({
    //     "paging": true,
    //     "lengthChange": true,
    //     "ordering": true,
    //     "info": true,
    //     "autoWidth": false
    // });


    $.fn.dataTable.moment( 'D MMM YYYY' );

    $exportColumns = [ 1, 2, 3, 4 ];
    if(window.location.href.includes("artsanimda/sellout")) {
        $exportColumns = [ 1, 2, 3, 4, 5, 6, 7 ];
    }
    if(window.location.href.includes("artsanimda/stockproduct")) {
        $exportColumns = [ 1, 2 ];
    }
    $exportOption = [
            {
                extend: 'print',
                exportOptions: {
                    columns: $exportColumns
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: $exportColumns
                }
            },
            {
                extend: 'csv',
                exportOptions: {
                    columns: $exportColumns
                }
            }
        ];
    if(!window.location.href.includes("artsanimda/sellout") && !window.location.href.includes("artsanimda/sellthrough") && !window.location.href.includes("artsanimda/stockproduct")) {
        $exportOption = [];
    }
    var tableData = $('#table-data').DataTable( {
        "paging": true,
        "lengthChange": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        }],
        "order": [[ 0, 'asc' ]],
        "dom": 'Bfrtip',
        "buttons": $exportOption,
        initComplete: function () {
            // this.api().columns().search('Pertama', true, false ).draw();
            this.api().columns().every( function (incre) {
                var column = this;
                if(incre>=0) {
                    var select = $('<select><option value="">- Filter -</option></select>');
                    //console.log($(column.footer()));
                        select.appendTo( $(column.footer()).empty() ).on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            console.log(val);
                            column.search( val ? '^'+val+'$' : '', true, false ).draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }
            } );
        },
        "footerCallback": function ( row, data, start, end, display ) {
            let columnIndex = 6;
           if(window.location.href.includes("artsanimda/sellout")) {
                if($('.role_id').val() <= 2) {
                //    columnIndex = 7;
                }
                var dataSatuan = "Unit";

                var api = this.api(), data;
                
     
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                // Total over all pages
                total = api
                    .column(columnIndex)
                    .data()
                    .reduce( function (a, b) {
                        console.log("data a: " + a);
                        console.log("data b: " + b);
                        
                        if(b.includes("pcs")) {
                            dataSatuan = "Pcs"
                        }
                        
                        valueA = a;

                        var splitValue = b.split(">");
                        valueB = parseInt(splitValue[1]);

                        console.log(intVal(valueA));
                        console.log(intVal(valueB));

                        return intVal(a) + intVal(valueB);
                    }, 0 );

     
                // Total over this page
                pageTotal = api
                    .column( columnIndex, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                         valueA = a;

                        var splitValue = b.split(">");
                        valueB = parseInt(splitValue[1]);

                        return intVal(a) + intVal(valueB);
                    }, 0 );
     
                // Update footer
                $( api.column(columnIndex).footer() ).html(
                    'Total : ' + pageTotal +' from '+ total + ' ' + dataSatuan
                );

                console.log("PageTotal : " + pageTotal);
                console.log("Total : " + total);
            }
        }
    });
    tableData.on( 'order.dt search.dt', function () {
        tableData.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    //Filter Sell Through
    $('#filter-by-bdm').change(function(){
        var valBDMId = $(this).val();
        var currentHref = $('.base_url').val() + '/' + $('.current_route').val();
        var hrefWithBdm = currentHref + '?bdm=' + valBDMId
        if($('.is_init_stock').val() != '') {
            hrefWithBdm += '&isInitStock=' + $('.is_init_stock').val();
        }
        if($('.product_type').val() != '') {
            hrefWithBdm += '&product_type=' + $('.product_type').val();
        }

        window.location.href = hrefWithBdm;
    });
    $('#filter-by-user').change(function(){
        var $table = $('#table-data').DataTable();
        var $valUser = $(this).val();
        var $valKategori = $('#filter-category').val();
        
        var $valSearch = $valUser + ' ' + $valKategori;

        $table.search($valSearch).draw();
    });
    $('#filter-category').change(function(){
        var $table = $('#table-data').DataTable();
        var $valUser = $('#filter-by-user').val();
        var $valKategori = $(this).val();
        
        var $valSearch = $valUser + ' ' + $valKategori;

        $table.search($valSearch).draw();
    });
    

    //Filter SellOut
    $('#filter-by-month').change(function(){
        var $table = $('#table-data').DataTable();
        var $valMonth = $(this).val();
        var $valYear = $('#filter-by-year').val();
        
        var $valSearch = $valMonth + ' ' + $valYear;

        $table.search($valSearch).draw();
    });
    $('#filter-by-year').change(function(){
        var $table = $('#table-data').DataTable();
        var $valMonth = $('#filter-by-month').val();
        var $valYear = $(this).val();
        
        var $valSearch = $valMonth + ' ' + $valYear;

        $table.search($valSearch).draw();
    });

    $('#filter-category-product').change(function(){
        var $table = $('#table-data').DataTable();
        var $valKategori = $(this).val();
        var $valSearch = $valKategori;
        $table.search($valSearch).draw();
    });


    $('#table-data-message').DataTable({
        "paging": true,
        "lengthChange": false,
        "ordering": true,
        "info": false,
        "autoWidth": true
    });

    function convertToRupiah(angka)
    {
        var rupiah = '';        
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
        return rupiah.split('',rupiah.length-1).reverse().join('');
    }
});
