$(document).ready(function(){


	//*******************************************
	/*	FLOT CHART
	/********************************************/

    function labelFormatter(label, series) {
        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
            + label
            + "<br>"
            + Math.round(series.percent) + "%</div>";
    }

    var table;
    var isTableLoaded = false;

    var $byUserData = []
	if( $('.sales-chart').length > 0 ) {
        $placeholder = $('.sales-chart');
        $placeholder.attr('data-ctype', '#week');

        var $byUser = $("#filter-by_user").val();
        var $target = $("#filter-target").val();
        var $category = $("#filter-category").val();
        var $tren = $("#filter-tren").val();
        var $value = $("#filter-value").val();
        var $year = $('#filter-year').val();
        var $quarter = $('#filter-quarter').val();
        var $month = $('#filter-month').val();


        loadChartByTime($byUser, $target, $category, $tren, $year, $quarter, $month);

        $('.chart-filter').click(function(e){
        	e.preventDefault();
            $byUser = $("#filter-by_user").val();
            $target = $("#filter-target").val();
            $category = $("#filter-category").val();
            $tren = $("#filter-tren").val();
            $value = $("#filter-value").val();
            $year = $('#filter-year').val();
            $quarter = $('#filter-quarter').val();
            $month = $('#filter-month').val();



            loadChartByTime($byUser, $target, $category, $tren, $year, $quarter, $month);

        });


        /*UI  FilterLogic*/
        $('#filter-month').parent().hide();
        $('#filter-quarter').parent().hide();

        $("#filter-tren").change(function() {
            if($("#filter-by_user").val() == "me") {
                if($(this).val() == "yearly") {
                    $('#filter-month').parent().hide();
                } else {
                    $('#filter-year').parent().show();
                }
            } else {
                if($(this).val() == "yearly") {
                    $('#filter-month').parent().hide();
                    $('#filter-quarter').parent().hide();
                    $('#filter-year').parent().show();
                } else if($(this).val() == "quarterly") {
                    $('#filter-month').parent().hide();
                    $('#filter-quarter').parent().show();
                    $('#filter-year').parent().show();
                } else  {
                    $('#filter-month').parent().show();
                    $('#filter-quarter').parent().hide();
                    $('#filter-year').parent().show();
                }
            }

        });

        $("#filter-by_user").change(function() {
            $('#filter-year').parent().show();
            $('#filter-tren').val('yearly');
            if($("#filter-by_user").val() == "me") {
                $('#filter-quarter').parent().hide();
                $('#filter-month').parent().hide();
            }
        });

        /*UI  FilterLogic*/
	}

    function loadChartByTime($byUser, $target, $category, $tren, $year, $quarter, $month) {

        $byUserData = [];
	    var $value = null;
        var $valueActual = null;

        var $expect = [];
        var $actual = [];

        if($byUser == "me") {
            $urlPath =  '/achievement/report/yearly';
            if($tren == "monthly") {
                $urlPath =  '/achievement/report/annually/' + $year;
            } else if($tren == "quarterly") {
                $urlPath =  '/achievement/report/quarterly/' + $year;
            }
        } else {
            $urlPath =  '/achievement/report/byrole/' + $byUser;
        }


        $.ajax({
            url : $('.base_url').val() + $urlPath,
			data : { 'category' : $category, 'tren' : $tren, 'year' : $year, 'quarter' : $quarter, 'month' : $month },
            type : 'GET',
            success : function(data) {
                if (data.hasOwnProperty("error")) {
                    alert(data.message);
                } else {
                    $.each( data, function( key, achievement ) {
                        $score = 0;

                        //By Target
                        $score = achievement.totalCount;
                        $scoreActual = $score - 25;
                        if($target == "revenue") {
                            $score = achievement.totalRevenue;
                            $scoreActual = $score - 1000000;
                        }

                        //By Tren
                        if($byUser == "me") {
                            if($tren == "monthly") {
                                $value = [gt(achievement.year, achievement.month, achievement.day), $score];
                                $valueActual = [gt(achievement.year, achievement.month, achievement.day), $scoreActual];
                            } else if($tren == "quarterly") {
                                $value = [achievement.quarter, $score];
                                $valueActual = [achievement.quarter, $scoreActual/2];
                            } else if($tren == "yearly") {
                                console.log(achievement.year);
                                $value = [achievement.year, $score];
                                $valueActual = [achievement.year, $scoreActual/2];
                            }
                        } else if($byUser == "dealer") {
                            $byUserData[key] = [key + 1, achievement.dealer];
                            $value = [key + 1, $score];
                            $valueActual = [key + 1, $scoreActual];
                        }

                        $expect[key] = $value;
                        $actual[key] = $valueActual;
                    });



                    configChart($byUser, $byUserData, $tren, $expect, $actual)
                }
            }
        });
    }

    function configChart($byUser, $byUserData, $tren, $expectData, $actualData) {
        //Generate Chart

        $isUseTime = false;
        $plotTicks = [];
        if($byUser == "me") {
            if($tren == "monthly") {
                $isUseTime = true;
            }
            if($tren == "quarterly") {
                $plotTicks = [[1, 'Q1'], [2, 'Q2'], [3, 'Q3'], [4, 'Q4']];
            }
        } else {
            $plotTicksDumb = [[1, 'Q1'], [2, 'Q2'], [3, 'Q3'], [4, 'Q4']];
            $plotTicks = $byUserData;
            console.log('plotticks');
            console.log($plotTicksDumb);
            console.log($plotTicks);
        }


        generateTable($expectData, $actualData);
        generateChart($placeholder, $expectData, $actualData, $isUseTime, $plotTicks);
	}

	function generateTable($expectData, $actualData) {

        if(isTableLoaded) {
            table.destroy();
        }
        isTableLoaded = true;

	    var dataSet = [];

        for (var i =0; i < $expectData.length; i++) {
            var tren = $expectData[i][0];
            console.log('tren');
            console.log(tren);
            if($byUserData.length > 0) // check by dealer atau bukan, kalo > 0 == by dealer
            {
                for (var userIndex = 0; userIndex < $byUserData.length; userIndex++) {
                    if($byUserData[userIndex][0] == tren) {
                        tren = $byUserData[userIndex][1];
                        break;
                    }
                }
            } else {
                if($tren=="quarterly") {
                    tren = "Q" + tren;
                } else if($tren=="monthly") {
                    tren = getIndonesianMonth(timestampToMonth(tren));
                }
            }

            var expectValue = $expectData[i][1];
            var actualValue = $actualData[i][1];
            var growth = (i*50) + "% (dummy)";
            var rowSet = [];
            rowSet[0] = tren;
            rowSet[1] = toRp(expectValue);
            rowSet[2] = toRp(actualValue);
            rowSet[3] = growth;

            dataSet[i] = rowSet;

        }
        console.log('byUser');
        console.log($byUserData);


        setTimeout(function() {
            table = $('#table-achievement').DataTable({
                retrive: true,
                data: dataSet,
                columns: [
                    { title: getIndonesianTren($tren) },
                    { title: "Target" },
                    { title: "Achievement" },
                    { title: "Growth" }
                ]
            });
        }, 1000);
    }

    function generateChart(placeholder, $dataExpect, $dataActual, $isUseTime, $plotTicks) {
        var target = $dataExpect;
        var actual = $dataActual;

        $plotMode = null;
        $plotTimezone = null;

        if($isUseTime) {
            $plotMode = "time";
            $plotTimezone = "browser";
        }
        if($plotTicks.length == 0) {
            $plotTicks = null;
        }


        var plot = $.plot(placeholder,
            [
                {
                    data: target,
                    label: "Target",
                    bars: {
                        show: true,
                        fill: false,
                        barWidth: 0,
                        align: "center",
                        lineWidth: 35
                    }
                },
                {
                    data: actual,
                    label: "Aktual"
                }
            ],

            {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: false
                    },
                    points: {
                        show: true,
                        lineWidth: 3,
                        fill: true,
                        fillColor: "#fafafa"
                    },
                    shadowSize: 0
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    borderWidth: 0,
                    tickColor: "#E4E4E4",

                },
                colors: ["rgba(189, 195, 199, 0.4)", "rgba(22, 160, 133,1.0)"],
                yaxis: {
                    font: { color: "#555" },
                    ticks: 8
                },
                xaxis: {
                    mode: $plotMode,
        			timezone: $plotTimezone,
                    font: { color: "#555" },
                    tickColor: "#fafafa",
                    ticks: $plotTicks,
                    tickDecimals: 0,
                    autoscaleMargin: 0.1
                },
                legend: {
                    labelBoxBorderColor: "transparent",
                    backgroundColor: "transparent"
                },
                tooltip: true,
                tooltipOpts: {
                    content: '%s: %y'
                }
            }
        );
    }

    //Belum di pakai
    function setToggleFilterChart($tabAnchor, $ulElementId, e) {
        e.preventDefault();
        $chartType = $($tabAnchor).attr('href');

        $($ulElementId + ' li').removeClass('active');
        $tabAnchor.parents('li').addClass('active');
        $tabAnchor.parent().parent().next().val($chartType);
    }




    // get day function
    function gt(y, m, d) {
        return new Date(y, m-1, d).getTime();
    }

    function timestampToMonth(timestamp) {
        var date = new Date(timestamp);
        var month = date.getMonth();
        return month;
    }

    function getIndonesianMonth (monthNumber) {
        var indonesianMonth = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        return indonesianMonth[monthNumber];
    }

    function getIndonesianTren (englishTren) {
	    if(englishTren=="yearly") {
	        return "Anually";
        } else if(englishTren=="quarterly") {
            return "Quarter";
        } else if(englishTren=="monthly") {
            return "Monthly";
        }
    }

    function randomVal(){
        return Math.floor( Math.random() * 200 );
    }


    function toRp(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
    }

}); // end ready function