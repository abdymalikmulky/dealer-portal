$(document).ready(function(){

	/* intentionally created on separated js file for the next theme updates */

    $(".select2").select2();
	/************************
	/*	SELECT DROPDOWN
	/************************/
    $('.select-role').multiselect({
        buttonClass: 'btn btn-default full-width text-left',
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 200
    });
    $('.select-regencies').multiselect({
        buttonClass: 'btn btn-default full-width text-left',
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 200,
        buttonWidth: '100%'
    });

    $('.select-multi').multiselect({
        buttonClass: 'btn btn-default full-width text-left',
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 200,
        buttonWidth: '100%'

    });

    $('.daterange-picker').daterangepicker({
        use24hours: true,
        timePicker: true,
        timePickerIncrement: 10,
        format: 'YYYY-MM-DD HH:mm',
        pick12HourFormat: false
    });

    $('.input-qty').keyup(function() {
        if($(this).val() <= 0 && $(this).val().length==1) {
            $(this).val(1);
        }
    });

});