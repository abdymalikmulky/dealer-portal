<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_out', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dealer_id')->unsigned(); //constraint userid
            $table->string('category');
            $table->date('transaction_date');
            $table->string('cust_company_name');
            $table->string('cust_company_address');
            $table->string('cust_phone');
            $table->string('cust_name');
            $table->string('cust_email');
            $table->integer('created_by')->unsigned(); //created by bdm
            $table->timestamps();

            $table->foreign('dealer_id')->references('id')->on('dealers');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_out');
    }
}
