<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellOutDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_out_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_out_id')->unsigned(); //constraint userid
            $table->string('path');
            $table->timestamps();

            $table->foreign('sell_out_id')->references('id')->on('sell_out');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_out_documents');
    }
}
