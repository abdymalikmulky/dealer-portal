<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); //constraint userid
            $table->string('company_name');
            $table->string('company_address');
            $table->string('company_phone');
            $table->string('PIC_name');
            $table->string('PIC_email');
            $table->integer('bdm_id')->unsigned(); //created by bdm
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('bdm_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealers');
    }
}
