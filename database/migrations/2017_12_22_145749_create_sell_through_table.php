<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellThroughTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_through', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dealer_id')->unsigned(); //constraint userid
            $table->string('category');
            $table->boolean('isInitStock');
            $table->integer('revenue');
            $table->date('transaction_date');
            $table->integer('created_by')->unsigned(); //created by bdm
            $table->timestamps();

            $table->foreign('dealer_id')->references('id')->on('dealers');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_through');
    }
}
