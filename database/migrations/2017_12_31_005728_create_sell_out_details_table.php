<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellOutDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_out_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_out_id')->unsigned(); //constraint userid
            $table->integer('sell_through_detail_id')->unsigned(); //constraint userid
            $table->string('qty');
            $table->timestamps();

            $table->foreign('sell_out_id')->references('id')->on('sell_out');
            $table->foreign('sell_through_detail_id')->references('id')->on('sell_through_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_out_detail');
    }
}
