<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievement_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('achievement_id')->unsigned(); //constraint userid
            $table->integer('month_id');
            $table->string('target_unit_revenue');
            $table->string('target_consumable_revenue');
            $table->string('target_optional_revenue');
            $table->string('target_unit_count');
            $table->string('target_consumable_count');
            $table->string('target_optional_count');
            $table->timestamps();

            $table->foreign('achievement_id')->references('id')->on('achievements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievement_details');
    }
}
