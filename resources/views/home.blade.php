@extends('layouts.app')

@section('content')


    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($slides as $index => $slide)
                <li data-target="#myCarousel" data-slide-to="{{ $index }}" class="@if($index==1) active @endif"></li>
            @endforeach
        </ol>
        <div class="carousel-inner" role="listbox">
            @foreach($slides as $index => $slide)
                <div class="carousel-item @if($index==1) active @endif" onclick="window.open('@if($slide->url == "") {{ route('slide.show', $slide->id) }} @else {{$slide->url}} @endif', '_blank')">
                    <img class="first-slide" src="{{ storage($slide->picture) }}" alt="First slide">
                    <div class="container">
                        {{-- <div class="carousel-caption d-none d-md-block text-left">
                            <p>{{ strip_tags(str_limit($slide->description, 200, "...")) }}</p>
                        </div> --}}
                    </div>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="marketing">

        <!-- Three columns of text below the carousel -->
        <div class="row marketing">
          <div class="col-lg-4">
            <a href="http://astragraphia.co.id/" target="_blank">
                <img class="home-image-menu" src="{{ storage('static_image/marketing-ag.png') }}" alt="Generic placeholder image">
                <p class="home-image-menu-desc">Astragraphia</p>
            </a>

          </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <a href="http://documentsolution.com" target="_blank">
                    <img class="home-image-menu" src="{{ storage('static_image/marketing-ds.png') }}" alt="Generic placeholder image">
                    <p class="home-image-menu-desc text-black">Document Solution</p>
                </a>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <a href="https://www.documentsolution.com/product/docucentre-s2110" target="_blank">
                    <img class="home-image-menu" src="{{ storage('static_image/marketing-product.png') }}" alt="Generic placeholder image">
                    <p class="home-image-menu-desc text-black">DocuCentre S2110</p>
                </a>
            </div><!-- /.col-lg-4 -->

        </div><!-- /.row -->


      </div><!-- /.container -->


@endsection
<script type="text/javascript">
    @if(Session::has('error'))
        alert('{{Session::get('error')}}');
    @endif

</script>