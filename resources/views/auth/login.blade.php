@extends('auth.template')
@section('title')
Login
@endsection

@section('content')
    <div class="login-box center-block">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}"> 
            {{ csrf_field() }}
            {{-- <p class="title">Use your username</p> --}}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="username" class="control-label sr-only">Email</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="text" placeholder="Username" id="username" class="form-control" name="username">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    </div>
                    @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <label for="password" class="control-label sr-only">Password</label>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="password" placeholder="password" id="password" name="password" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <button type="submit" class="btn btn-custom-primary btn-lg btn-block btn-auth">
                <i class="fa fa-arrow-circle-o-right"></i> Login
            </button>

        </form>

        <div class="links">
            <p><a class="btn btn-link" href="{{ route('password.request') }}">Forgot Your Password?</a></p>
        </div>
    </div>
    
@endsection