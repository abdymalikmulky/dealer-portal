<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

    <head>
        <title>@yield('title') | Dealer Portal</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="KingAdmin Dashboard">
        <meta name="author" content="The Develovers">

        <!-- CSS -->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style type="text/css">
            body {
                overflow-y: hidden;
            }
        </style>
        <!--[if lte IE 9]>
            <link href="assets/css/main-ie.css" rel="stylesheet" type="text/css" />
            <link href="assets/css/main-ie-part2.css" rel="stylesheet" type="text/css" />
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/kingadmin-favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/kingadmin-favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/kingadmin-favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/kingadmin-favicon57x57.png">
        <link rel="shortcut icon" href="assets/ico/favicon.png">

    </head>

    <body>
        <div class="wrapper full-page-wrapper page-auth page-login text-center">
            <div class="inner-page">
                <div class="logo">
                    <h1>DEALER PORTAL</h1>
                </div>

                <div class="separator">&nbsp;</div>
                <h2>@yield('title')</h2>

				@yield('content')

            </div>
        </div>

        <footer class="footer">&copy; Dealer Portal</footer>

        <!-- Javascript -->
        <script src="{{ asset('js/jquery/jquery-2.1.0.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap/bootstrap.js') }}"></script>
        <script src="{{ asset('js/plugins/modernizr/modernizr.js') }}"></script>
    </body>

</html>
