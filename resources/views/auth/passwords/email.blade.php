@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-xs-12 col-md-6 box-form-reset">   
            <h2>Reset Password</h2> 
            <hr/>
            <form class="form-horizontal form-reset" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="control-label sr-only">Email</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                           <input id="email" placeholder="Input your email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <button class="btn btn-block custom-button" type="submit">Reset Password</button>
                    </div>

                </div>
                <div class="links">
                    <p>I remember it ! Lets <a href="#" class="login-link">Login</a></p>
                </div>
            </form>
        </div>
        <div class="col-md-3"></div> 
    </div>
    

@endsection
