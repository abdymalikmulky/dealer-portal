@extends('auth.template')
@section('title')
Reset Password
@endsection

@section('content')
<div class="login-box center-block">
    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label sr-only">Email</label>
            <div class="col-sm-12">
                <div class="input-group">
                   <input id="email" placeholder="Input your email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus required>
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                </div>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label sr-only">Password</label>
            <div class="col-sm-12">
                <div class="input-group">
                   <input id="password" placeholder="Input your password" type="password" class="form-control" name="password" value="{{ old('password') }}" autofocus required>
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                </div>
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password_confirmation" class="control-label sr-only">Confirm Password</label>
            <div class="col-sm-12">
                <div class="input-group">
                   <input id="password_confirmation" placeholder="Input your password_confirmation" type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" autofocus required>
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                </div>
                @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                @endif
            </div>
        </div>
     

        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-custom-primary btn-lg btn-block btn-auth">
                    <i class="fa fa-arrow-circle-o-right"></i> Reset Password
                </button>
            </div>
        </div>
        <div class="links">
            <p>I remember it ! Lets <a href="{{ route('login') }}">Login</a></p>
        </div>
    </form>
</div>
@endsection
