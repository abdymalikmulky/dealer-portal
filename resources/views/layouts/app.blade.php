<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap4/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="header-top">
       <div class="container">
           <div class="row">
               <div class="col-md-6 col-6 header-left">
                   <a target="_blank" href="https://www.documentsolution.com"><i class="fa fa-globe"></i> Go to Corporate Site</a>
               </div>
               <div class="col-md-6 col-6 header-right">
                   <div class="row header-tools">
                        @if(!Auth::check())
                       <a class="header-login login-toggle" href="#"><i class="fa fa-lock"></i> Login</a>
                       @else
                       Welcome, <a href="{{ url('artsanimda') }}" title="Enter Admin Page">   &nbsp; {{ Auth::user()->name }}</a>
                       @endif
                   </div>
               </div>
           </div>
       </div>
   </div>
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <div class="container">
            {{-- <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button> --}}
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('images/logo.png') }}" height="50px"/>
            </a>
            <nav class="nav-desktop">
                <ul>
                    <li>
                        <a href="https://www.documentsolution.com/content/about/company-profile" target="_blank">About </a>
                    </li>
                    <li>
                        <a href="https://www.documentsolution.com/contact" target="_blank">Contact Us </a>
                    </li>
                </ul>
            </nav>
             @if(!Auth::check())
                
                <form class="form-inline login-form" role="form" method="POST" action="{{ route('login') }}">
                    <div class="caret"></div>
                    <h4>LOGIN</h4>
                    {{ csrf_field() }}
                    <div class="input-group mb-2 mr-sm-2">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                        <input class="form-control" type="text" placeholder="Username" name="username">
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                        <input class="form-control" type="password" placeholder="Password" name="password">
                    </div>
                    <div class="row">
                        <div class="col-md-6 login-forgot-box">
                            <a href="{{ route('password.request') }}">Forgot Password</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn custom-button" type="submit">LOGIN</button>
                        </div>
                    </div>
                    
                </form>
            @endif
        </div>
        
    </nav>
    <div id="app" class="container container-home">
        @yield('content')
        <div class="footer-content">
            <img src="{{ asset('images/footer.png') }}" width="100%"/></footer>
        </div>
    </div>
    <!-- FOOTER -->

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/bootstrap4/bootstrap.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        $(".container-home").height($(document).height()-200);

        var $icon = "";
        var $title = "";
        var $message = "";
        @if (app('request')->session()->has(SESSION_LOGIN_SUCCESS))  
            @if (app('request')->session()->get(SESSION_LOGIN_SUCCESS))  
                $title = "Success";
                $icon = "success"
            @else
                $title = "Failed";
                $icon = "warning"
            @endif

            $message = "{{ app('request')->session()->get(SESSION_LOGIN_MESSAGE) }}"
            swal({
              title: $title,
              text: $message,
              icon: $icon,
              dangerMode: true
            });

        @endif
        

        // Login toggle
        $('.login-toggle').click(function() {
            if($('.login-form').is(":visible")) {
                $('.login-form').hide();
            } else {
                $('.login-form').fadeIn();
            }
            return false;
        });

        $('.login-link').click(function(){
            if($('.login-form').is(":visible")) {
                $('.login-form').hide();
            } else {
                $('.login-form').fadeIn();
            }
            return false;
        });
    </script>
</body>
</html>
