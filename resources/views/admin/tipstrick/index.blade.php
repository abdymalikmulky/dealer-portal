@extends('admin.layouts.app')

@section('page_title', $title)
@section('page_desc', 'Data ' . strtolower($title))

@section('content')
@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
	<div class="table-action-add">
		<a href="{{ route('tipstrick.create') }}">
			<button type="button" class="btn btn-primary right"><span class="fa fa-plus"/>  Add</button>
		</a>
	</div>
@endif
<div class="row">
    <div class="col-md-12 ">
		<div class="box" style="margin-top:10px;">

			<ul class="news">
				@foreach($tipsTricks as $tipsTrick)
				<li>
					<div class="row">
						<a href="{{ route('tipstrick.show', $tipsTrick->id) }}">
							<div class="col-md-3 tt-image">
								<img src="{{ storage($tipsTrick->picture) }}" alt="Picture"/>
							</div>
							<div class="col-md-9 ">
								<p class="tt-title">{{ $tipsTrick->title }}</p>
								<p class="tt-date">
									<i class="fa fa-user"></i> {{ $tipsTrick->user->name }} | {{ date("d M Y H:m:s", strtotime($tipsTrick->created_at))  }}
								</p>
								<div class="tt-content">
									{{ strip_tags(str_limit($tipsTrick->content, 700, "..."))  }} <a href='{{ route('tipstrick.show', $tipsTrick->id) }}' style="color:skyblue">Baca Selengkapnya</a>
								</div>
								@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
								<div class="tt-action">
									<a href="{{ route('tipstrick.edit', $tipsTrick->id) }}" title="Edit" class="element-floating-left">
										<button class="btn btn-xs btn-warning">
											<span class="fa fa-edit"/> Edit
										</button>
									</a>

									<form action="{{ route('tipstrick.delete', $tipsTrick->id) }}" method="post" class="element-floating-left">
										{{csrf_field()}}
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  {{ $title }} {{ $tipsTrick->id }} - {{$tipsTrick->title }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
									</form>
								</div>
								@endif
							</div>
						</a>
					</div>
					<div style="clear: both"></div>
				</li>
					<hr/>
				@endforeach
			</ul>

    </div>
    </div>
</div>
@endsection
