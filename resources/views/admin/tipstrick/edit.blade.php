@extends('admin.layouts.app')

@section('page_title', 'Edit '.  $title )
@section('page_desc', 'Ubah '.  $title )

@section('content')

	<div class="row text-editor">
		<div class="col-md-12">
			<div class="box">
				{{--<div class="box-header">--}}
					{{--<h3 class="box-title"> Ubah {{ $tipsTrick->title }}</h3>--}}
				{{--</div>--}}
				<div class="box-body">
					<form class="form-horizontal" method="POST" slide="form" action="{{ route('tipstrick.update', $tipsTrick->id) }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="PATCH">
						<fieldset>
							<legend>Data {{ $title }} #{{ $tipsTrick->id }}</legend>
							<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
								<label for="title" class="col-sm-3 control-label">Title <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="text" value="{{ $tipsTrick->title }}" name="title" class="form-control" id="title" placeholder="Title {{ $title }}">
									@if ($errors->has('title'))
										<span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
								<label for="content" class="col-sm-3 control-label">Content <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<textarea type="content"  name="content" class="form-control textarea" id="content" placeholder="Content {{ $title }}">{{ $tipsTrick->content }}</textarea>
									@if ($errors->has('content'))
										<span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<label for="photo" class="col-sm-3 control-label">Picture <span class="color-red">*</span> </label>
								<div class="col-md-9">
									<img src="{{ storage($tipsTrick->picture) }}" width="450px"/>
									<br/>
									<br/>
									<input type="file" id="photo" name="photo">
									<p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB</em></p>
									@if ($errors->has('photo'))
										<span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block">Save</button>
								</div>
							</div>
						</fieldset>
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
