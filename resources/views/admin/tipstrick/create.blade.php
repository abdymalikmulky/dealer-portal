@extends('admin.layouts.app')

@section('page_title', 'Add '.  $title )
@section('page_desc', 'Add '.  $title .' pada sistem')

@section('content')

<div class="row text-editor">
    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Add {{ $title }}</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" slide="form" action="{{ route('tipstrick.store') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<fieldset>
						<legend>Data {{ $title }}</legend>
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
							<label for="title" class="col-sm-3 control-label">Title <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="title" class="form-control" id="title" placeholder="Title {{ $title }}">
								@if ($errors->has('title'))
									<span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
							<label for="content" class="col-sm-3 control-label">Content <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<textarea type="content" name="content" class="form-control textarea" id="content" placeholder="Content {{ $title }}"></textarea>
								@if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group">
							<label for="photo" class="col-sm-3 control-label">Picture <span class="color-red">*</span> </label>
							<div class="col-md-9">
								<input type="file" id="photo" name="photo">
								<p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB</em></p>
								@if ($errors->has('photo'))
									<span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-block">Save</button>
							</div>
						</div>
					</fieldset>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
