@extends('admin.layouts.app')

@section('page_title', 'Add')
@section('page_desc', 'Add role pada sistem')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"> Add Role</h3>
				</div>
				<div class="box-body">
					<form class="form-horizontal" method="POST" role="form" action="{{ route('role.update', $role->id) }}">
						{{ csrf_field() }}
						<fieldset>
							<legend>Data Role</legend>
							<input name="_method" type="hidden" value="PATCH">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-sm-3 control-label">Nama Role</label>
								<div class="col-sm-9">
									<input type="text" name="name" class="form-control" id="name" placeholder="Nama Role" required value="{{ $role->name }}">
									@if ($errors->has('name'))
										<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
								<label for="desc" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-9">
									<textarea type="desc" name="desc" class="form-control" id="desc" placeholder="Description Role" required>{{ $role->description}}</textarea>
									@if ($errors->has('desc'))
										<span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block">Save</button>
								</div>
							</div>
						</fieldset>

					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
