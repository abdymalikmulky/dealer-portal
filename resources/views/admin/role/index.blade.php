@extends('admin.layouts.app')

@section('page_title', 'Role')
@section('page_desc', 'Data role')

@section('content')
<div class="table-action-add">

	<a href="{{ route('role.create') }}">
		<button type="button" class="btn btn-primary right"><span class="fa fa-plus"/>  Add</button>
	</a>
</div>
<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> Data Role</h3> </em>
			</div>
			<div class="box-body">


				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Description</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($roles as $role)
						<tr>
							<td>{{$role->id}}</td>
							<td>{{$role->name}}</td>
							<td>{{$role->description}}</td>
							<td>
								<a href="{{ route('role.edit', $role->id) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a> 

								<form action="{{ route('role.delete', $role->id) }}" method="post" class="element-floating-left">
								{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  role {{ $role->id }} - {{$role->name }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</div>
    </div>
</div>
@endsection
