@extends('admin.layouts.app')

{{--@section('page_title', 'Dashboard')--}}
{{--@section('page_desc', 'Ranking Data')--}}

@section('content')
<style>

    /* MARKETING CONTENT
    -------------------------------------------------- */

    /* Center align the text within the three columns below the carousel */
    .marketing {
        margin-bottom: 1.5rem;
        text-align: center;
    }
    .marketing div.col-lg-4 {
        margin-top: 10px;


    }
    .marketing h2 {
        font-weight: normal;
    }
    .marketing .col-lg-4 p {
        margin-right: .75rem;
        margin-left: .75rem;
    }

    .home-image-menu {
        width: 100%;
        height: 175px;
    }
    .home-image-menu-desc {
        position: absolute;
        bottom: 0;
        left: 7px;
        color: white;
        font-size: 18px;
        width: 91.5%;
        color: #fff;
        text-align: left;
        padding-left: 10px;
        background: rgba(0,0,0,0.4);
        margin-left:100px;
        padding: 5px 10px;
    }

</style>

<div class="row">
    <div class="col-md-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @foreach($slides as $index => $slide)
                    <li data-target="#carousel-example-generic" data-slide-to="{{ $index }}" class="@if($index==1) active @endif"></li>
                @endforeach
            </ol>

            <!-- Wrapper for slides -->

            <div class="carousel-inner" role="listbox">
                @foreach($slides as $index => $slide)
                    <div class="item @if($index==1) active @endif">
                        <a href="@if($slide->url == "") {{ route('slide.show', $slide->id) }} @else {{$slide->url}} @endif" target="_blank">
                            <img src="{{ storage($slide->picture) }}" alt="First slide">
                            <div class="carousel-caption">
                                {{-- <p>{{ strip_tags(str_limit($slide->description, 200, "...")) }}</p> --}}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="marketing">

            <!-- Three columns of text below the carousel -->
            <div class="row marketing">
              <div class="col-lg-4">
                <a href="http://astragraphia.co.id/" target="_blank">
                    <img class="home-image-menu" src="{{ storage('static_image/marketing-ag.png') }}" alt="Generic placeholder image">
                    <p class="home-image-menu-desc">Astragraphia</p>
                </a>

              </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <a href="http://documentsolution.com" target="_blank">
                        <img class="home-image-menu" src="{{ storage('static_image/marketing-ds.png') }}" alt="Generic placeholder image">
                        <p class="home-image-menu-desc">Document Solution</p>
                    </a>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <a href="https://www.documentsolution.com/product/docucentre-s2110" target="_blank">
                        <img class="home-image-menu" src="{{ storage('static_image/marketing-product.png') }}" alt="Generic placeholder image">
                        <p class="home-image-menu-desc">DocuCentre S2110</p>
                    </a>
                </div><!-- /.col-lg-4 -->

            </div><!-- /.row -->
        </div><!-- /.container -->



    </div>
</div>
@endsection
