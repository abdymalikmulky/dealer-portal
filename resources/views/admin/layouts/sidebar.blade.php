
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                @if(Auth::user()->roles[0]->id != Config::get('constants.DEALER'))
                    <img src="{{ storage(Auth::user()->user_detail->photo) }}" class="img-circle" alt="" width="50px"/>
                @else
                    <img src="{{ storage(Auth::user()->dealer->company_photo) }}" class="img-circle" alt="" width="50px"/>
                @endif

            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <small> {{ Auth::user()->roles[0]->name }}</small>
                {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            </div>
        </div>
        <hr style="margin-top:5px; margin-bottom:5px"/>
        <!-- sidebar menu: : style can be found in sidebar.less -->

        <ul class="sidebar-menu">
            <li class="header">MENU</li>


            <li class="@if(strpos(Route::currentRouteName(), "dashboard") !== false) active @endif">
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-home"></i>
                    <span>Home</span>
                </a>
            </li>

            {{--Management USER--}}
            @if(Auth::user()->roles[0]->id == Config::get('constants.SUPERADMIN'))
                <li class="treeview">
                    <a href="#" >
                        <i class="fa fa-user"></i><span>Management User & Role</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('user') }}"><span>User</span></a></li>
                        <li><a href="{{ route('role') }}"><span>Role</span></a></li>
                    </ul>
                </li>
            @elseif(Auth::user()->roles[0]->id == Config::get('constants.DEALER'))
                {{--Maybe for reseller--}}
            @else
                <li class="@if(strpos(Route::currentRouteName(), "user") !== false) active @endif"><a href="{{ route('user') }}"><i class="fa fa-user"></i><span>

                            {{--@if(Auth::user()->roles[0]->id == Config::get('constants.PM'))--}}
                                {{--Management COS--}}
                            {{--@elseif(Auth::user()->roles[0]->id == Config::get('constants.COS'))--}}
                                {{--Channel Management--}}
                            {{--@else--}}
                                {{--Channel Management--}}
                            {{--@endif--}}
                            Channel Management
                        </span>
                    </a>
                </li>
            @endif


            {{--MARKETING PROGRAM--}}
            <li class="@if(Route::currentRouteName()=="slide") active @endif">
                <a href="{{ route('slide') }}">
                    <i class="fa fa-gift"></i>
                    <span>Marketing Program</span>
                </a>
            </li>

            @if(Auth::user()->roles[0]->id == Config::get('constants.PM'))
                <li class="treeview @if(strpos(Route::currentRouteName(), "product") !== false || strpos(Route::currentRouteName(), "tipstrick") !== false || strpos(Route::currentRouteName(), "salestool") !== false) active @endif">
                    <a href="#"><i class="fa fa-newspaper-o"></i> <span>Business Solution</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li @if(strpos(Route::currentRouteName(), "product") !== false) class="active" @endif><a href="{{ route('product') }}"><i class="fa fa-print"></i><span>Product</span></a></li>
                        {{--<li @if(strpos(Route::currentRouteName(), "slide") !== false) class="active" @endif><a href="{{ route('slide') }}"><i class="fa fa-gift"></i><span>Marketing Program</span></a></li>--}}
                        <li @if(strpos(Route::currentRouteName(), "tipstrick") !== false) class="active" @endif><a href="{{ route('tipstrick') }}"><i class="fa fa-question-circle"></i><span>Tips & Trick</span></a></li>
                        <li @if(strpos(Route::currentRouteName(), "salestool") !== false) class="active" @endif><a href="{{ route('salestool') }}"><i class="fa fa-file"></i><span>Sales Tools</span></a></li>
                    </ul>
                </li>
            {{--  SELL TRHOUGH --}}
            @endif

            @if(Auth::user()->roles[0]->id < Config::get('constants.DEALER'))
                <li class="treeview @if(strpos(Route::currentRouteName(), "sellthrough") !== false) active @endif">
                    <a href="#">
                        <i class="fa fa-user fa-exchange"></i><span>Sell Through</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li @if(app('request')->input('isInitStock')) class="active" @endif><a href="{{ route('sellthrough', array('isInitStock' => true)) }}"><span>Stock on Hand</span></a></li>

                        @foreach($productCategories as $productCategory)
                            <li @if(app('request')->input('product_type') == $productCategory->category) class="active" @endif><a href="{{ route('sellthrough', array('product_type' => $productCategory->category)) }}"><span>{{ $productCategory->description }}</span></a></li>
                        @endforeach
                    </ul>
                </li>
            @endif

            {{-- SELLOUT  --}}
            @if(Auth::user()->roles[0]->id <= Config::get('constants.DEALER'))

                <li class="treeview @if(strpos(Route::currentRouteName(), "sellout") !== false) active @endif">
                    <a href="#">
                        <i class="fa fa-user fa-exchange"></i><span>Sell Out</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($productCategories as $productCategory)
                            <li @if(app('request')->input('product_type') == $productCategory->category) class="active" @endif>
                                <a href="{{ route('sellout', array('product_type' => $productCategory->category)) }}"><span>{{ $productCategory->description }}</span></a>
                            </li>
                        @endforeach

                    </ul>
                </li>
            @endif


            @if(Auth::user()->roles[0]->id == Config::get('constants.DEALER'))
            <li class="treeview @if(Route::currentRouteName()=="stockproduct") active @endif">
                    <a href="#" >
                        <i class="fa fa-user fa-print"></i><span>Stock Product</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($productCategories as $productCategory)
                            <li @if(app('request')->input('product_type') == $productCategory->category) class="active" @endif><a href="{{ route('stockproduct', array('product_type' => $productCategory->category)) }}"><span>{{ $productCategory->description }}</span></a></li>
                        @endforeach
                    </ul>
                </li>
            @endif

            {{--ACHIEVEMENT--}}
        @if(Auth::user()->roles[0]->id == Config::get('constants.BDM'))
            <li class="@if(Route::currentRouteName()=="achievement") active @endif"><a href="{{ route('achievement') }}"><i class="fa fa-trophy"></i><span>Plan Setting</span></a></li>
        @endif
        
        <li class="@if(Route::currentRouteName()=="achievement.report") active @endif"><a href="{{ route('achievement.report') }}"><i class="fa fa-dashboard"></i><span>Achievement Report</span></a></li>
        


        @if(Auth::user()->roles[0]->id != Config::get('constants.PM'))
            <li class="treeview @if(Route::currentRouteName()=="salestool" || strpos(Route::currentRouteName(), "tipstrick") !== false)) active @endif">
                <a href="#" ><i class="fa fa-newspaper-o"></i><span>Business Solution</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    
                    <li class="@if(Route::currentRouteName()=="salestool") active @endif">
                        <a href="{{ route('salestool') }}"><i class="fa fa-file"></i><span>Sales Tools</span><i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li class="@if(Route::currentRouteName()=="salestool") active @endif">
                                <a href="{{ route('salestool') }}"><i class="fa fa-file"></i><span>Product Knowledge</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="@if(strpos(Route::currentRouteName(), "tipstrick") !== false) active @endif">
                        <a href="{{ route('tipstrick') }}"><i class="fa fa-question-circle"></i><span>Tips & Trick</span></a>
                    </li>
                </ul>
            </li>
            @endif


            <li class="@if(Route::currentRouteName()=="message") active @endif"><a href="{{ route('message') }}"><i class="fa fa-envelope"></i><span>Messages</span></a></li>







        </ul>
    </section>
    <!-- /.sidebar -->
</aside>