<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <title>Dealer Portal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Dealer Portal">
    <meta name="author" content="@abdymalikmulky">

    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">

    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('css/main.css') }}" rel="stylesheet">--}}

    <link href="{{ asset('css/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/buttons.dataTables.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/adminlte/AdminLTE.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adminlte/_all-skins.css') }}" rel="stylesheet">

    <link href="{{ asset('css/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">

    
    <link href="{{ asset('css/my-custom-styles.css') }}" rel="stylesheet">
</head>
<style type="text/css">
    #footer {
        position: relative;
        width: 110%;
        bottom: 0;
        margin-top: 25px;
        margin-bottom: : 25px;
      }
</style>

<body class="hold-transition skin-black sidebar-mini skin-black-light    
">
    <input type="hidden" value="{{ url('/artsanimda') }}" class="base_url">
    <input type="hidden" value="{{ Auth::user()->roles[0]->id }}" class="role_id">
    <div id="app" class="wrapper">
        {{-- TOP BAR MENU --}}
        @include('admin.layouts.topbar')

        @include('admin.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield('page_title')
                    {{--<small>@yield('page_desc')</small>--}}
                </h1>
                {{--<ol class="breadcrumb">--}}
                    {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                    {{--<li><a href="#">Examples</a></li>--}}
                    {{--<li class="active">Blank page</li>--}}
                {{--</ol>--}}
            </section>

            <!-- Main content -->
            <section class="content" >
                @yield('content')


            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        
    </div>
    <!-- FOOTER -->
    <div style="margin-top:0.5%;text-align:center" id="footer" class="footer">
        <img src="{{ asset('images/footer.png') }}" width="400px"/></footer>
    </div>



    <!-- Scripts -->
    <script src="{{ asset('js/jquery/jquery-2.1.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/modernizr/modernizr.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-tour.custom.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/plugins/moment.min.js') }}"></script>

    {{-- Data Table --}}
    <script src="{{ asset('js/plugins/datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/datetime-moment.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/datatable-currency.js') }}"></script>
    {{-- EXPORT --}}
    <script src="{{ asset('js/plugins/datatable/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/export/jszip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatable/export/buttons.print.min.js') }}"></script>
    {{-- Data Table --}}


    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.time.min.js') }}"></script>
    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.animator.js') }}"></script>
    <script src="{{ asset('js/plugins/stat/flot/jquery.flot.categories.js') }}"></script>



    <script src="{{ asset('js/king-common.js') }}"></script>
    <script src="{{ asset('js/king-form-layouts.js') }}"></script>
    <script src="{{ asset('js/king-table-layouts.js') }}"></script>
    <script src="{{ asset('js/king-chart-stat.js') }}"></script>
    <script src="{{ asset('js/king-elements.js') }}"></script>

    <script src="{{ asset('js/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('js/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/adminlte/plugins/fastclick/fastclick.js') }}"></script>
    <script src="{{ asset('js/adminlte/app.min.js') }}"></script>
    <script src="{{ asset('js/adminlte/demo.js') }}"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="{{ asset('js/custom.js') }}"></script>
    <script type="text/javascript">
        var $icon = "";
        var $title = "";
        var $message = "";
        @if (app('request')->session()->has(SESSION_LOGIN_SUCCESS))  
            @if (app('request')->session()->get(SESSION_LOGIN_SUCCESS))  
                $title = "Success";
                $icon = "success"
            @else
                $title = "Failed";
                $icon = "warning"
            @endif

            $message = "{{ app('request')->session()->get(SESSION_LOGIN_MESSAGE) }}"
            swal({
              title: $title,
              text: $message,
              icon: $icon
            });

        @endif
        
    </script>
</body>


</html>

