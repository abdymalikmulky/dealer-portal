<!-- TOP BAR -->
<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
        <img src="{{ asset('images/logo.png') }}" width="200px"/>
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success"><span class="msg-count">
                        {{ count($myMessages) }}
                        </span></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <span class="msg-count">{{ count($myMessages) }}</span> messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                @foreach($myMessages as $message)
                                <li><!-- start message -->
                                    <a href="{{ route('message.show', $message->id) }}">
                                        <div class="pull-left">
                                        @if($message->sender->roles[0]->id != Config::get('constants.DEALER'))
                                            <img src="{{ storage($message->sender->user_detail->photo) }}" class="img-circle" alt="User Avatar" width="30px"/>
                                        </div>
                                        @else
                                        @endif
                                        <h4>
                                            {{ $message->subject }}
                                            <small><i class="fa fa-clock-o"></i> {{ $message->timeago($message->created_at) }}</small>
                                        </h4>
                                        <p>{{ strip_tags(str_limit($message->message, 30, "..."))  }}</p>
                                        <small style="color:silver">Dari : {{ $message->sender->name }}</small>
                                    </a>
                                </li>
                                <!-- end message -->
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer"><a href="{{ route('message') }}">Show All</a></li>
                    </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger count-notification">{{ count(Auth::user()->unreadNotifications) }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <span class="count-notification">{{ count(Auth::user()->unreadNotifications) }}</span> notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu menu-notification">
                                @foreach(Auth::user()->unreadNotifications as $notification)
                                <li>
                                    @include('admin.layouts.notifications.' . snake_case(class_basename($notification->type)))
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        {{-- <li class="footer"><a href="#">View all</a></li> --}}
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        @if(Auth::user()->roles[0]->id != Config::get('constants.DEALER'))
                            <img src="{{ storage(Auth::user()->user_detail->photo) }}" class="user-image" alt="User Avatar" width="30px"/>
                        @else
                            <img src="{{ storage(Auth::user()->dealer->company_photo) }}" class="user-image" alt="User Avatar" width="30px"/>
                        @endif
                        {{--<span class="hidden-xs name">{{ Auth::user()->name }} | {{ Auth::user()->roles[0]->name }}</span> <span class="caret"></span>--}}
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">

                            @if(Auth::user()->roles[0]->id != Config::get('constants.DEALER'))
                            <img src="{{ storage(Auth::user()->user_detail->photo) }}" class="img-circle" alt="User Avatar" width="50px"/>
                            @else
                            <img src="{{ storage(Auth::user()->dealer->company_photo) }}" class="user-circle" alt="User Avatar" width="50px"/>
                            @endif

                            <p>
                                {{ Auth::user()->name }}
                                <small>{{ Auth::user()->roles[0]->description }}</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('profile') }}" class="btn btn-default btn-flat"><i class="fa fa-power-user"></i> Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="#"
                                   onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                    <i class="fa fa-power-off"></i>
                                    Sign out
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>

                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                {{--<li>--}}
                    {{--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</header>
<!-- /top -->
