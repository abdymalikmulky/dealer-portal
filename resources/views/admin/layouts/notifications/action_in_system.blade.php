
@php
	$url = "";
	$message = "";
	$time = "";
	$notificationData = $notification->data;
	$time = \App\Util::timeago($notification->created_at);
	
	
	if(array_key_exists('achievement', $notificationData)) {
		$key = 'achievement';
		$achievement = $notificationData[$key];
		$message = "was edited Achievement " . $achievement['year'];
		$url = route('achievement.edit', $achievement['id']);

	} else if(array_key_exists('sellthrough', $notificationData)) {	
		$key = 'sellthrough';
		$message = "was input " . $notificationData[$key]['category'];
		$url = route('stockproduct', array('product_type' => $notificationData[$key]['category']));
	}
@endphp

<a href="#" url="{{ $url }}" class="notification" id="{{ $notification->id }}" title="{{ $time }}">
    {{ $notification->data['user']['name'] }} {{ $message }} | <small>  {{ $time }} </small>
</a>

