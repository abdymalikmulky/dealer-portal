@foreach(Auth::user()->unreadNotifications as $notification)
	{{-- {{$notification}}
	<pre>
	{{ print_r($notification->data) }}
	</pre>
	<br> --}}
    <li notificationCount="<?php echo count(Auth::user()->unreadNotifications) ?>">
        @include('admin.layouts.notifications.' . snake_case(class_basename($notification->type)))
    </li>
@endforeach