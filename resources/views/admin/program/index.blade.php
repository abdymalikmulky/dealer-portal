@extends('admin.layouts.app')

@section('page_title', 'Program/Event')
@section('page_desc', 'Data Program')

@section('content')

<div class="main-content">
	<div class="bottom-30px">
		<div class="table-action-add">
			<a href="{{ route('program.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> Create New Project</a>
		</div>
	</div>
	<div class="table-responsive">
		<!-- PROJECT TABLE -->
		<table class="table colored-header datatable project-list">
			<thead>
			<tr>
				<th>Name</th>
				<th>Jenis</th>
				<th>Mulai</th>
				<th>Berakhir</th>
				<th>Oleh</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			@foreach($programs as $program)
				<tr>
					<td><a href="{{ route('ligareseller.program', $program->id) }}" title="{{ $program->desc }}">{{ $program->name }}</a></td>
					<td>{{ $program->type }}</td>
					<td>{{ $program->start }}</td>
					<td>{{ $program->end }}</td>
					<td><a href="#">{{ $program->user->name }}</a></td>
					<td><span class="label @if($program->status->name == "PENDING") label-warning @elseif($program->status->name == "CLOSED") label-danger @else label-success @endif">{{ $program->status->name }}</span></td>
					<td>
						<a href="{{ route('program.edit', $program->id) }}" title="Edit" class="element-floating-left">
							<button class="btn btn-xs btn-warning">
								<span class="fa fa-edit"/> Edit
							</button>
						</a>

						<form action="{{ route('program.delete', $program->id) }}" method="post" class="element-floating-left">
							{{csrf_field()}}
							<input name="_method" type="hidden" value="DELETE">
							<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  role {{ $program->id }} - {{$program->name }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
						</form>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<!-- END PROJECT TABLE -->
	</div>

</div>
@endsection
