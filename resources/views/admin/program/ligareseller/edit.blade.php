@extends('admin.layouts.app')

@section('page_title', 'Ubah')
@section('page_desc', 'Ubah Product pada program')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"> Add Product</h3>
				</div>
				<div class="box-body">
					<form class="form-horizontal" method="POST" product="form" action="{{ route('ligareseller.update', $ligaResellerProgram->id) }}">
						{{ csrf_field() }}
						<fieldset>
							<input type="hidden" name="user_id" value="{{ Auth::user()->id }}"/>
							<input type="hidden" name="program_id" value="{{ $programId }}"/>
							<input name="_method" type="hidden" value="PATCH">
							<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
								<label for="product" class="col-sm-3 control-label">Cari dan pilih Product</label>
								<div class="col-sm-9">
									<select id="product" name="product" class="form-control select-role" required>
										@foreach($products as $product)
											<option @if($product->id == $ligaResellerProgram->product_id)
														selected
													@endif
													value="{{$product['id']}}">{{$product['name']}}</option>
										@endforeach
									</select>
									@if ($errors->has('product'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('point') ? ' has-error' : '' }}">
								<label for="point" class="col-sm-3 control-label">Point Product</label>
								<div class="col-sm-9">
									<input type="number" name="point" class="form-control" placeholder="Point Product" required value="{{ $ligaResellerProgram->point }}">
									@if ($errors->has('point'))
										<span class="help-block">
                                        <strong>{{ $errors->first('point') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block">Save</button>
								</div>
							</div>
						</fieldset>

					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
