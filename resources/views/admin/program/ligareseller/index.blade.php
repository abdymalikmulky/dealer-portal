@extends('admin.layouts.app')

@section('page_title', $program->name )
@section('page_desc', $program->desc )

@section('content')

<div class="main-content">
	<div class="bottom-30px">
		<div class="table-action-add">
			<a href="{{ route('ligareseller.program.create', $program->id) }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> Add Product</a>
		</div>
	</div>
	<div class="table-responsive">
		<!-- PROJECT TABLE -->
		<table id="table-data" class="table colored-header datatable project-list">
			<thead>
			<tr>
				<th>SN Product</th>
				<th>Point</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			@foreach($program->ligaResellers as $ligaResellerProduct)
				<tr>
					<td><a href="{{ route('product.show', $ligaResellerProduct->id) }}">{{ $ligaResellerProduct->SN }}</a></td>
					<td>{{ $ligaResellerProduct->pivot->point }}</td>
					<td>
						<a href="{{ route('ligareseller.program.edit', array($ligaResellerProduct->pivot->id, $program->id )) }}" title="Edit" class="element-floating-left">
							<button class="btn btn-xs btn-warning">
								<span class="fa fa-edit"/> Edit
							</button>
						</a>

						<form action="{{ route('ligareseller.program.delete', array($ligaResellerProduct->pivot->id, $program->id )) }}" method="post" class="element-floating-left">
							{{csrf_field()}}
							<input name="_method" type="hidden" value="DELETE">
							<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  Product {{ $ligaResellerProduct->id }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
						</form>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<!-- END PROJECT TABLE -->
	</div>

</div>
@endsection
