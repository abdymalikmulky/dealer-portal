@extends('admin.layouts.app')

@section('page_title', 'Add')
@section('page_desc', 'Add program dan event pada sistem')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"> Add program</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('program.store') }}">
					{{ csrf_field() }}
					<fieldset>
						<input type="hidden" name="user_id" value="{{ Auth::user()->id }}"/>

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">Nama program</label>
							<div class="col-sm-9">
								<input type="text" name="name" class="form-control" id="name" placeholder="Nama program" required>
								@if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('programType') ? ' has-error' : '' }}">
							<label for="programType" class="col-sm-3 control-label">Pilih Jenis Program</label>
							<div class="col-sm-9">
								<select id="programType" name="programType" class="form-control select-role" required>
									@foreach($programTypes as $programType)
										<option value="{{$programType['type']}}">{{$programType['type']}}</option>
									@endforeach
								</select>
								@if ($errors->has('programType'))
									<span class="help-block">
                                        <strong>{{ $errors->first('programType') }}</strong>
                                    </span>
								@endif
							</div>
						</div>


						<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
							<label for="desc" class="col-sm-3 control-label">Description</label>
							<div class="col-sm-9">
								<textarea type="desc" name="desc" class="form-control" id="desc" placeholder="Description product" required></textarea>
								@if ($errors->has('desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
							<label for="time" class="col-sm-3 control-label">Waktu program berjalan</label>
							<div class="col-sm-9">
								<input type="text" name="time" class="form-control daterange-picker" placeholder="Waktu program berjalan" required>
								@if ($errors->has('time'))
									<span class="help-block">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-block">Save</button>
							</div>
						</div>
					</fieldset>
					
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
