@extends('admin.layouts.app')

@section('page_title', $title)
@section('page_desc', 'Data ' . $title)

@section('content')

<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<a href="" class="close">×</a>
			<strong>Terjadi Kesalahan!</strong> {{Session::get('error')}}
		</div>
	@endif
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> Data {{ $title }}</h3> </em>
			</div>
			<div class="box-body">

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>{{ ucwords($title ) }}</th>
							<th>Count</th>

						</tr>
					</thead>
					<tbody>
					@php $no = 1; @endphp
						@foreach($stockProducts as $stockProduct)
						@if(($stockProduct->qty - $stockProduct->sellout_qty) > 0)
						<tr>
							<td></td>
							<td>{{ $stockProduct->product->product_code }} - {{ $stockProduct->product->type }}</td>
							<td> <b>{{ $stockProduct->qty - $stockProduct->sellout_qty }} @if($productType=="unitmono") Unit Mono @elseif($productType=="unitcolor") Unit Color @else {{ ucwords($productType) }} @endif</b></td>
						</tr>
							<?php $no++; ?>
						@endif
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</div>
    </div>
</div>

@endsection
