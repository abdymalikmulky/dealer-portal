@extends('admin.layouts.app')

@section('page_title', 'Ubah')
@section('page_desc', 'Ubah data reseller ' . $reseller->name)

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">{{ $dealer->name }} | Ubah reseller</h3>
				</div>
				<div class="box-body">
					<form class="form-horizontal" method="POST" product="form" action="{{ route('reseller.update', $reseller->id) }}">
						{{ csrf_field() }}
						<fieldset>
							<input name="_method" type="hidden" value="PATCH">
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-sm-3 control-label">Nama dealer</label>
								<div class="col-sm-9">
									<input type="text" name="name" class="form-control" id="name" placeholder="Nama Dealer" required value="{{ $reseller->name }}">
									@if ($errors->has('name'))
										<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
								<label for="address" class="col-sm-3 control-label">Alamat</label>
								<div class="col-sm-9">
									<textarea type="address" name="address" class="form-control" id="review" placeholder="Alamat" required>{{ $reseller->address }}</textarea>
									@if ($errors->has('address'))
										<span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block">Save</button>
								</div>
							</div>
						</fieldset>

					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
