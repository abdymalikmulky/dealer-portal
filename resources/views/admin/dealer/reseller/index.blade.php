@extends('admin.layouts.app')

@section('page_title', 'Reseller Dealer')
@section('page_desc', 'Manage Reseller Dealer ' . $dealer->name)

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> {{ $dealer->name }} | Data Reseller </h3> </em>
			</div>
			<div class="box-body">
				<div class="table-action-add">

					@if($resellers->count() > 0)
					<a href="{{ route('sellout.dealer', $dealer->id) }}" title="Sell Out" >
						<button class="btn btn-success right"><span class="fa fa-money"/> Data Sell Out </button>
					</a>
					@endif
					<a href="{{ route('reseller.create', $dealer->id) }}">
						<button type="button" class="btn btn-primary right"><span class="fa fa-plus-square"/>  Add Reseller</button>
					</a>

				</div>

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Alamat</th>
							<th>Point</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($resellers as $reseller)
							<?php $totalPoint = 0; ?>
							{{--({{ $loop->iteration }} / {{ $loop->count }})--}}
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $reseller->name }}</td>
							<td>{{ $reseller->address }}</td>
							<td>
								<a href="{{ route('sellout.reseller', $reseller->id) }}">
									{{ $reseller->sellOutCount() }} Sell Out
								</a>,
								@foreach($reseller->approvedSellOuts as $sellOut)
                                    <?php $totalPoint += $sellOut->product->ligaResellerProduct->point; ?>
								@endforeach
								<b>{{ $totalPoint }} Point </b>
							</td>
							<td>
								<a href="{{ route('reseller.edit', $reseller->id) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a>

								<form action="{{ route('reseller.delete', $reseller->id) }}" method="post" class="element-floating-left">
								{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  data {{ $reseller->id }} - {{$reseller->name }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</div>
    </div>
</div>
@endsection
