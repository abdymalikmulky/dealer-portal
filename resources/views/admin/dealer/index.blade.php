@extends('admin.layouts.app')

@section('page_title', 'Dealer')
@section('page_desc', 'Manage dealer ' . Auth::user()->name)

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> Data Dealer | BDM {{ Auth::user()->name }}</h3> </em>
			</div>
			<div class="box-body">
				<div class="table-action-add">	

					<a href="{{ route('dealer.create') }}">
						<button type="button" class="btn btn-primary right"><span class="fa fa-plus-square"/>  Add</button>
					</a>
				</div>

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Email</th>
							<th>Nama Owner</th>
							<th>Telepon</th>
							<th>Asal</th>
							<th>Reseller</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($dealers as $dealer)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $dealer->name }}</td>
							<td>{{ $dealer->user->email }}</td>
							<td>{{ $dealer->owner_name }}</td>
							<td>{{ $dealer->phone }}</td>
							<td>{{ $dealer->city }}</td>
							<td><a href="{{ route('reseller', $dealer->id) }}">{{ $dealer->resellerCount() }} Reseller</a></td>
							<td>
								<div class="row">
									<div class="col-md-6 nopadding">
										<a href="{{ route('dealer.edit', $dealer->id) }}" title="Edit" >
											<button class="btn btn-xs btn-warning btn-block">
												<span class="fa fa-edit"/> Edit
											</button>
										</a>
									</div>
									<div class="col-md-6 nopadding">
										<form action="{{ route('dealer.delete', $dealer->id) }}" method="post" >
											{{csrf_field()}}
											<input name="_method" type="hidden" value="DELETE">
											<button class="btn btn-xs btn-danger btn-block" type="submit" onclick="confirm('Are you sure want to delete  data {{ $dealer->id }} - {{$dealer->name }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
										</form>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 nopadding">
										<a href="{{ route('sellout.dealer', $dealer->id) }}" @if($dealer->resellerCount() == 0) onclick="alert('Belum memiliki reseller, Addkan reseller terlebih dahulu'); return false;" @endif title="Sell Out" >
											<button class="btn btn-xs btn-success btn-block">
												<span class="fa fa-money"/> Data Sell Out
											</button>
										</a>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</div>
    </div>
</div>
@endsection
