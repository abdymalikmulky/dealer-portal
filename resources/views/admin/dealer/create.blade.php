@extends('admin.layouts.app')

@section('page_title', 'Add')
@section('page_desc', 'Add dealer pada sistem')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"> Add dealer</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('dealer.store') }}">
					{{ csrf_field() }}
					<fieldset>
						<legend>Login Data Dealer</legend>
						<input type="hidden" name="bdm_id" value="{{ Auth::user()->id }}"/>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">Nama dealer</label>
							<div class="col-sm-9">
								<input type="text" name="name" class="form-control" id="name" placeholder="Nama Dealer" required>
								@if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-sm-3 control-label">Email dealer</label>
							<div class="col-sm-9">
								<input type="text" name="email" class="form-control" id="email" placeholder="Email Dealer" required>
								@if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-sm-3 control-label">Password</label>
							<div class="col-sm-9">
								<input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
								@if ($errors->has('password'))
									<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							<label for="password_confirmation" class="col-sm-3 control-label">Konfirmasi Password</label>
							<div class="col-sm-9">
								<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Konfirmasi Password" required>
								@if ($errors->has('password_confirmation'))
									<span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Informasi Dealer Lainnya</legend>
						<div class="form-group{{ $errors->has('owner') ? ' has-error' : '' }}">
							<label for="owner" class="col-sm-3 control-label">Nama owner</label>
							<div class="col-sm-9">
								<input type="text" name="owner" class="form-control" id="owner" placeholder="Nama owner" required>
								@if ($errors->has('owner'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('owner') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<label for="address" class="col-sm-3 control-label">Alamat</label>
							<div class="col-sm-9">
								<textarea type="address" name="address" class="form-control" id="review" placeholder="Alamat" required></textarea>
								@if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<label for="phone" class="col-sm-3 control-label">Telepon</label>
							<div class="col-sm-9">
								<input type="number" name="phone" class="form-control" id="phone" placeholder="Nomor Telepon" required>
								@if ($errors->has('phone'))
									<span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
							<label for="branch" class="col-sm-3 control-label">Count Cabang</label>
							<div class="col-sm-9">
								<input type="number" name="branch" class="form-control" id="branch" placeholder="Banyak Cabang" required>
								@if ($errors->has('branch'))
									<span class="help-block">
                                        <strong>{{ $errors->first('branch') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
							<label for="city" class="col-sm-3 control-label">Kota</label>
							<div class="col-sm-9">
								<select name="city" class="form-control select-role" id="city" required>
									<option value="">Pilih Kota Dealer</option>
									<option value="aceh">Aceh</option>
									<option value="Sumut">Sumatera Utara</option>
									<option value="sumbar">Sumatera Barat</option>
									<option value="Riau">Riau</option>
									<option value="Jambi">Jambi</option>
									<option value="Sumsel">Sumatera Selatan</option>
									<option value="Bengkulu">Bengkulu</option>
									<option value="Lampung">Lampung</option>
									<option value="BaBel">Kep. Bangka Belitung</option>
									<option value="kepRiau">Kepulauan Riau</option>
									<option value="Jakarta">Jakarta</option>
									<option value="Jabar">Jawa Barat</option>
									<option value="Banten">Banten</option>
									<option value="Jateng">Jawa Tengah</option>
									<option value="Yogyakarta">Yogyakarta</option>
									<option value="Jatim">Jawa Timur</option>
									<option value="Kalbar">Kalimantan Barat</option>
									<option value="Kalteng">Kalimantan Tengah</option>
									<option value="Kalsel">Kalimantan Selatan</option>
									<option value="Kaltim">Kalimantan Timur</option>
									<option value="Kaltra">Kalimantan Utara</option>
									<option value="Bali">Bali</option>
									<option value="NTT">Nusa Tenggara Timur</option>
									<option value="NTB">Nusa Tenggara Barat</option>
									<option value="Sulut">Sulawesi Utara</option>
									<option value="Sulteng">Sulawesi Tengah</option>
									<option value="Sulsel">Sulawesi Selatan</option>
									<option value="Sultengg">Sulawesi Tenggara</option>
									<option value="Sulbar">Sulawesi Barat</option>
									<option value="Gorontalo">Gorontalo</option>
									<option value="Maluku">Maluku</option>
									<option value="Maluku Utara">Maluku Utara</option>
									<option value="Papua">Papua</option>
									<option value="Papua Barat">Papua Barat</option>
								</select>
								@if ($errors->has('city'))
									<span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-block">Save</button>
							</div>
						</div>
					</fieldset>

				</form>
			</div>
		</div>
    </div>
</div>
@endsection
