@extends('admin.layouts.app')

@section('page_title', 'Edit data ' .$title)
@section('page_desc', '')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title">Ubah {{$title}}</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" role="form" action="{{ route('user.update', $user->id) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<fieldset>
						<legend>Login Data {{$title}}</legend>
						<input name="_method" type="hidden" value="PATCH">
						<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
							<label for="username" class="col-sm-3 control-label">Username <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="username" class="form-control" id="username" placeholder="Username" value="{{ $user->username }}" required>
								@if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						{{--Jika bukan dealer, pake email sama name, kalo dealer cukup pake name company sama email pic--}}
						@if(!$isForDealer)
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">Name <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ $user->name }}" required>
								@if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-sm-3 control-label">Email <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{ $user->email }}" required>
								@if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						@endif

						@if($isSuperAdmin)
						<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
							<label for="role" class="col-sm-3 control-label">Pilih Role <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<select id="role" name="role" class="form-control select-role" style="width:100%" required>
									@foreach($roles as $role)
									<option 
									@if(in_array($role->id, array_column($user->roles->toArray(), 'id')))
										selected
									@endif 
									value="{{$role['id']}}">{{$role['name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						@else
							<input name="role" type="hidden" value="{{$roleFor}}">
						@endif
	
						
					</fieldset>

					<fieldset>
						<legend>Another Information</legend>
						{{--Check if user is not dealer, dont use user detail table--}}
						@if(!$isForDealer)
							{{-- Informasi User Selain Dealer--}}

						<div class="form-group">
							<label for="nip" class="col-sm-3 control-label">NIP <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="nip" id="nip" value="{{ $user->user_detail->NIP }}" placeholder="Nomor Induk Pegawai">
								@if ($errors->has('nip'))
									<span class="help-block">
                                        <strong>{{ $errors->first('nip') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="phone" class="col-sm-3 control-label">Mobile Phone <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="texxt" class="form-control" name="phone" id="phone" value="{{ $user->user_detail->phone }}" placeholder="Nomor Telepon/HP">
								@if ($errors->has('phone'))
									<span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="address" class="col-sm-3 control-label">Area <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<textarea class="form-control" name="address" id="address" rows="5" cols="30" placeholder="Alamat">{{ $user->user_detail->address }}</textarea>
								@if ($errors->has('address'))
									<span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="information" class="col-sm-3 control-label">Additional Information <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<textarea class="form-control" name="information" id="information" rows="5" cols="30" placeholder="Keterangan Addan (Kosongkan Jika Tidak Ada)">{{ $user->user_detail->information }}</textarea>
								@if ($errors->has('information'))
									<span class="help-block">
                                        <strong>{{ $errors->first('information') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="photo" class="col-sm-3 control-label">Profile Photo <span class="color-red">*</span></label>
							<div class="col-md-9">
								<img src="{{ storage($user->user_detail->photo) }}" width="250px" style="margin-bottom: 10px;"/>
								<input type="file" id="photo" name="photo">
								<p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB</em></p>
								@if ($errors->has('photo'))
									<span class="help-block">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						@else
							{{-- ini akan tereksekusi kalo usernya bdm, saat ini hanya bdm aja yang bisa set--}}
							{{-- Informasi User Dealer--}}

							{{--Data Perusahaan--}}
							<h4>Informasi Business Partner</h4>
							<div class="form-group">
								<label for="name" class="col-sm-3 control-label">Jenis Badan Usaha <span class="color-red">*</span></label>
								<div class="col-sm-9">
									<select class="form-control" name="company_type" id="company_type" placeholder="Nama business Partner">
										<option value="PT" @if($user->dealer->company_type == "PT") selected @endif>PT </option>
										<option value="CV" @if($user->dealer->company_type == "CV") selected @endif>CV </option>
										<option value="PERORANGAN" @if($user->dealer->company_type == "PERORANGAN") selected @endif>PERORANGAN</option>
									</select>
									@if ($errors->has('name'))
										<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="col-sm-3 control-label">Name <span class="color-red">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="name" id="name" placeholder="Nama business Partner" value="{{ $user->dealer->company_name }}">
									@if ($errors->has('name'))
										<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group">
                                <label for="regencies" class="col-sm-3 control-label">Choose City/Regency <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <select id="regencies" name="regency" class="form-control select-regencies" style="width:100%" required>
                                        @foreach($regencies as $regencie)
                                        <option @if($user->dealer->company_city==$regencie['name']) selected @endif value="{{$regencie['name']}}">{{$regencie['name']}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('regency'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('regency') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
							<div class="form-group">
								<label for="company_address" class="col-sm-3 control-label">Address  <span class="color-red">*</span></label>
								<div class="col-sm-9">
									<textarea class="form-control" name="company_address" id="company_address" rows="5" cols="30" placeholder="Alamat business Partner"> {{ $user->dealer->company_address}}</textarea>
									@if ($errors->has('company_address'))
										<span class="help-block">
                                    <strong>{{ $errors->first('company_address') }}</strong>
                                </span>
									@endif
								</div>
							</div>
							<div class="form-group">
                                <label for="company_websites" class="col-sm-3 control-label">Websites <span class="color-red">*</span></label>
                                <div class="col-sm-9 form-website">
                                	<?php
                                	$splitedWebsiteData = explode(";", $user->dealer->company_websites);
                                	?>
                                	@foreach ($splitedWebsiteData as $value)
                                		
                                		<div id="box-form-website">
                                			<input type="url" required class="form-control company_website" name="company_websites[]" id="company_websites" placeholder="Website" value="{{ $value }}"> 
                                			<a href="{{ $value }}" target="_blank" class="show-website"><i class="fa fa-globe"></i></a>
                                			<a href="#" class="delete-company-website"><i class="fa fa-trash"></i></a>
                                		</div>
                                		<div style="clear: both;"></div>
                                	@endforeach
                                    
                                    @if ($errors->has('company_websites'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_websites') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-9">
                                    <p style="margin:5px 0px 0px 5px; text-align: left">
                                    	<a href="#" class="btn btn-sm btn-primary add-website">Add website</a></p>
                                </div>
                            </div>
							<div class="form-group">
								<label for="company_phone" class="col-sm-3 control-label">Telepon Kantor <span class="color-red">*</span></label>
								<div class="col-sm-9">
									<input type="number" class="form-control" name="company_phone" id="company_phone" placeholder="Nomor Telepon Kantor"  value="{{ $user->dealer->company_phone }}">
									@if ($errors->has('company_phone'))
										<span class="help-block">
                                        <strong>{{ $errors->first('company_phone') }}</strong>
                                    </span>
									@endif
								</div>
							</div>


							{{--Data PIC--}}
							<h4>Informasi PIC</h4>
							<div class="form-group">
								<label for="PIC_name" class="col-sm-3 control-label">Name <span class="color-red">*</span></label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="PIC_name" id="PIC_name" placeholder="Nama PIC"  value="{{ $user->dealer->PIC_name }}">
									@if ($errors->has('PIC_name'))
										<span class="help-block">
                                    <strong>{{ $errors->first('PIC_name') }}</strong>
                                </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="PIC_email" class="col-sm-3 control-label">Email <span class="color-red">*</span></label>
								<div class="col-sm-9">
									<input type="email" class="form-control" name="email" id="email" placeholder="Email PIC"  value="{{ $user->dealer->PIC_email }}">
									@if ($errors->has('email'))
										<span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="PIC_phone" class="col-sm-3 control-label">Mobile Phone <span class="color-red">*</span></label>
								<div class="col-sm-9">
									<input type="number" class="form-control" name="PIC_phone" id="PIC_phone" placeholder="Telepon/HP PIC"  value="{{ $user->dealer->PIC_phone}}">
									@if ($errors->has('PIC_phone'))
										<span class="help-block">
                                    <strong>{{ $errors->first('PIC_phone') }}</strong>
                                </span>
									@endif
								</div>
							</div>
							<div class="form-group">
                                <label for="PIC_jabatan" class="col-sm-3 control-label">Position</label>
                                <div class="col-sm-9">
									<select name="PIC_jabatan" id="PIC_jabatan" class="form-control">
										<option value="">Choose Position</option>
                                        <option value="Owner" @if($user->dealer->PIC_jabatan=="Owner") selected @endif>Owner</option>
                                        <option value="Sales" @if($user->dealer->PIC_jabatan=="Sales") selected @endif>Sales</option>
                                        <option value="Finance" @if($user->dealer->PIC_jabatan=="Finance") selected @endif>Finance</option>
                                        <option value="Adminstration" @if($user->dealer->PIC_jabatan=="Adminstration") selected @endif>Adminstration</option>
                                    </select>
                                    @if ($errors->has('PIC_jabatan'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_jabatan') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
							<h4>Informasi PIC II</h4>
							<p class="input-desciprtion">* Kosongkan jika tidak ada PIC ke 2</p>
							<div class="form-group">
								<label for="PIC_name2" class="col-sm-3 control-label">Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" name="PIC_name2" id="PIC_name" placeholder="Nama PIC ke-2" value="{{ $user->dealer->PIC2_name}}" />
									@if ($errors->has('PIC_name'))
										<span class="help-block">
                                    <strong>{{ $errors->first('PIC_name') }}</strong>
                                </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="PIC_email" class="col-sm-3 control-label">Email</label>
								<div class="col-sm-9">
									<input type="email" class="form-control" name="email2" id="email" placeholder="Email PIC ke-2" value="{{ $user->dealer->PIC2_email}}" >
									@if ($errors->has('email'))
										<span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="PIC_phone" class="col-sm-3 control-label">Mobile Phone</label>
								<div class="col-sm-9">
									<input type="number" class="form-control" name="PIC_phone2" id="PIC_phone" placeholder="Mobile Phone PIC ke-2" value="{{ $user->dealer->PIC2_phone}}">
									@if ($errors->has('PIC_phone'))
										<span class="help-block">
                                    <strong>{{ $errors->first('PIC_phone') }}</strong>
                                </span>
									@endif
								</div>
							</div>
							<div class="form-group">
                                <label for="PIC_jabatan2" class="col-sm-3 control-label">Position </label>
                                <div class="col-sm-9">
                                    <select name="PIC_jabatan2" id="PIC_jabatan2" class="form-control">
                                        <option value="">Choose Position</option>
                                        <option value="Owner" @if($user->dealer->PIC2_jabatan=="Owner") selected @endif>Owner</option>
                                        <option value="Sales" @if($user->dealer->PIC2_jabatan=="Sales") selected @endif>Sales</option>
                                        <option value="Finance" @if($user->dealer->PIC2_jabatan=="Finance") selected @endif>Finance</option>
                                        <option value="Adminstration" @if($user->dealer->PIC2_jabatan=="Adminstration") selected @endif>Adminstration</option>
                                    </select>
                                    @if ($errors->has('PIC_jabatan2'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_jabatan2') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>


						@endif

					</fieldset>


					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary btn-block">Save</button>
						</div>
					</div>

				</form>
			</div>
		</div>
    </div>
</div>
@endsection
