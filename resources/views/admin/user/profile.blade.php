@extends('admin.layouts.app')

@section('page_title', 'Profile')
@section('page_desc', 'edit profile & data user')

@section('content')

    @if(Session::has('error'))
        @if(Session::get('error'))
            <span class="profile_error_input"></span>
            <div class="alert alert-danger alert-dismissable">
                <a href="" class="close">×</a>
                <strong>Terjadi Kesalahan!</strong> {{Session::get('message')}}
            </div>
        @else
            <div class="alert alert-success alert-dismissable">
                <a href="" class="close">×</a>
                <strong>Berhasil!</strong> {{Session::get('message')}}
            </div>
        @endif
    @endif
    @if ($errors->any())

        @foreach ($errors->all() as $error)
                <span class="profile_error_input"></span>
                <div class="alert alert-danger alert-dismissable">
                    <a href="" class="close">×</a>
                    {{$error}}
                </div>
        @endforeach

    @endif
    <!-- NAV TABS -->
    <ul class="nav nav-tabs nav-tabs-custom-colored tabs-iconized">
        <li class="active"><a href="#profile-tab" data-toggle="tab"><i class="fa fa-user"></i> Profile</a></li>
        <li><a href="#otentikasi-tab" data-toggle="tab"><i class="fa fa-gear"></i> Change Password</a></li>
    </ul>
    <!-- END NAV TABS -->
    <div class="tab-content profile-page">
        <!-- PROFILE TAB CONTENT -->
        <div class="tab-pane profile active" id="profile-tab">
            <div class="box">
                <div class="box-body profile-box">

                    <form class="form-horizontal" method="POST" role="form" action="{{ route('user.update', $user->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PATCH">

                        <div class="row">

                            <div class="col-md-3">
                                <div class="user-info-left">
                                    <!-- Profile Image -->
                                    <div class="box box-primary">
                                        <div class="box-body box-profile">
                                            <div class="data-input" style="text-align: center;">
                                                <span>Upload Photo</span>
                                                        <input type="file" id="photo" name="photo" style="font-size:10px; margin: auto;">
                                                        <p class="input-desciprtion">* photo format (png, jpg)</p>
                                                        @if ($errors->has('photo'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('photo') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            @if(Auth::user()->roles[0]->id != Config::get('constants.DEALER'))
                                            <img class="profile-user-img img-responsive img-circle" src="{{ storage($user->user_detail->photo) }}" alt="User profile picture">
                                            @else
                                            <img class="profile-user-img img-responsive img-circle" src="{{ storage($user->dealer->company_photo) }}" alt="User profile picture">
                                            @endif
                                            <h3 class="data-value profile-username text-center">{{$user->name}}</h3>
                                            <p class="data-input">
                                                <input class="form-control" type="text" name="name" id="name" placeholder="Nama" value="{{ $user->name }}" required style="text-size:100px">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </p>

                                            @if(Auth::user()->roles[0]->id != Config::get('constants.DEALER'))
                                            <p class="text-muted text-center">NIK : {{$user->user_detail->NIP}}</p>
                                            @endif
                                            <p class="text-muted text-center">{{strtoupper($user->roles[0]->name)}}</p>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->

                                </div>
                            </div>
                            <div class="col-md-9">


                                <div class="user-info-right">
                                    <div class="basic-info">
                                        <h3 style="margin:5px"><i class="fa fa-square"></i> Login Info</h3>
                                        <div class="form-group data-row">
                                            <label class="data-name">Username</label>
                                            <span class="data-value full-width"> {{$user->username}}</span>
                                            <span class="data-input">
                                                <input class="form-control" type="text" name="username" id="username" placeholder="Username" value="{{ $user->username }}" required>
                                                        @if ($errors->has('username'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                        @endif
                                            </span>
                                        </div>
                                        <p class="data-row form-group">
                                            <label class="data-name">Last Login</label>
                                            <span class="data-value full-width">{{date("d-m-Y | h:i:s", strtotime($user->last_login))}}</span>
                                        </p>
                                        <p class="data-row form-group">
                                            <label class="data-name">Date Joined</label>
                                            <span class="data-value full-width">{{date("d-m-Y | h:i:s", strtotime($user->created_at))}}</span>
                                        </p>
                                    </div>
                                    <div class="contact_info">
                                        @if(!$isDealer)
                                            <h3><i class="fa fa-square"></i> Contact Info</h3>
                                            <p class="form-group data-row">
                                                <label class="data-name">Email</label>
                                                <span class="data-value full-width">{{$user->email}}</span>
                                                <span class="data-input">
                                            <input class="form-control" type="text" name="email" id="email" placeholder="Email" value="{{ $user->email }}" required>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                    @endif
                                        </span>
                                            </p>
                                            <p class="data-row form-group">
                                                <label class="data-name">Mobile Phone</label>
                                                <span class="data-value full-width">{{$user->user_detail->phone}}</span>
                                                <span class="data-input">
                                            <input class="form-control" type="text" name="phone" id="phone" placeholder="Telepon/HP" value="{{ $user->user_detail->phone }}" required>
                                                    @if ($errors->has('phone'))
                                                        <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                                    @endif
                                        </span>
                                            </p>
                                            <p class="data-row form-group">
                                                <label class="data-name">Area/Cabang</label>
                                                <span class="data-value full-width">{{strtoupper($user->user_detail->address)}}</span>
                                                <span class="data-input">
                                                    <select id="address" name="address" class="form-control" style="width:100%" required>
                                                        <option value="timur">Timur</option>
                                                        <option value="barat">Barat</option>
                                                        <option value="tenggara">Tenggara</option>
                                                        <option value="selatan">Selatan</option>
                                                    </select>
                                                    @if ($errors->has('address'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                        </span>
                                                    @endif

                                                </span>
                                            </p>

                                        @else
                                            <h3><i class="fa fa-square"></i> Informasi Perusahaan</h3>

                                            <p class="data-row form-group">
                                                <label class="data-name">Jenis Perusahaan</label>
                                                <span class="data-value full-width">{{$user->dealer->company_type}}</span>
                                                <span class="data-input">
                                                <select class="form-control" name="company_type" id="company_type" placeholder="Nama business Partner">
                                                    <option value="PT">PT (Perseroan Terbatas)</option>
                                                    <option value="CV">CV (Comanditaire Venotschap / Persekutuan Komanditer)</option>
                                                    <option value="PERORANGAN">PERORANGAN</option>
                                                </select>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                                  
                                            </p>
                                            <p class="data-row form-group">
                                                <label class="data-name">City/Regency</label>
                                                <span class="data-value full-width">{{$user->dealer->company_city}}</span>
                                                <div class="data-input">
                                                    <select id="regencies" name="regency" class="form-control" style="width:100%" required>
                                                        <option value="">Choose Company City</option>
                                                        @foreach($regencies as $regencie)
                                                        <option @if($user->dealer->company_city==$regencie['name']) selected @endif value="{{$regencie['name']}}">{{$regencie['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('regency'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('regency') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </p>
                                            <p class="data-row form-group">
                                                <label class="data-name">Alamat</label>
                                                <span class="data-value full-width">{{$user->dealer->company_address}}</span>
                                                <span class="data-input">
                                                <textarea class="form-control" name="company_address" id="company_address" rows="5" cols="30" placeholder="Alamat Perusahaan"> {{ $user->dealer->company_address}} </textarea>
                                                    @if ($errors->has('company_address'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('company_address') }}</strong>
                                                    </span>
                                                            @endif
                                                    </span>
                                            </p>
                                            <p class="data-row form-group ">
                                                <?php
                                                $splitedWebsiteData = explode(";", $user->dealer->company_websites);
                                                ?>
                                                <label class="data-name">Websites</label>
                                                <span class="data-value full-width">
                                                    @if($user->dealer->company_websites != "") 
                                                    @foreach ($splitedWebsiteData as $value)
                                                    <a href="{{ $value }}" target="_blank">{{ $value }}</a>  
                                                    @endforeach
                                                    @endif
                                                </span>
                                                <span class="data-input form-website">
                                                @foreach ($splitedWebsiteData as $value)
                                                @if($value!="")
                                                <span><a href="{{ $value }}" target="_blank">Go to website</a></span>
                                                @endif
                                                <input type="url" class="form-control company_website" name="company_websites[]" id="company_websites" placeholder="Website" value="{{ $value }}"> 
                                                @endforeach
                                                    @if ($errors->has('company_websites'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('company_websites') }}</strong>
                                                    </span>
                                                            @endif
                                                </span>
                                                <p style="display:none; margin:5px 0px 0px 5px; text-align: right" class="form-add-website"><a href="#" class="add-website">Add website</a></p>
                                            </p>

                                            <p class="data-row">
                                                <label class="data-name">Mobile Phone</label>
                                                <span class="data-value full-width">{{$user->dealer->company_phone}}</span>
                                                <span class="data-input">
                                        <input type="number" class="form-control" name="company_phone" id="company_phone" placeholder="Nomor Mobile Phone Perusahaan"  value="{{ $user->dealer->company_phone }}">
                                                    @if ($errors->has('company_phone'))
                                                        <span class="help-block">
                                            <strong>{{ $errors->first('company_phone') }}</strong>
                                        </span>
                                                    @endif
                                    </span>
                                            </p>

                                            {{--PIC--}}
                                            <br/>
                                            <br/>
                                            <h3><i class="fa fa-square"></i> Informasi PIC Perusahaan</h3>
                                            <h4>PIC I</h4>
                                            <p class="data-row">
                                                <label class="data-name">Nama </label>
                                                <span class="data-value full-width">{{$user->dealer->PIC_name}}</span>
                                                <span class="data-input">
                                        <input type="text" name="PIC_name" id="PIC_name" placeholder="Nama PIC" class="form-control" value="{{ $user->dealer->PIC_name }}" required>
                                                    @if ($errors->has('PIC_name'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('PIC_name') }}</strong>
                                                </span>
                                                    @endif
                                        </span>
                                            </p>
                                            <p class="data-row">
                                                <label class="data-name">Email</label>
                                                <span class="data-value full-width">{{$user->dealer->PIC_email}}</span>
                                                <span class="data-input">
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Email PIC" required value="{{ $user->dealer->PIC_email }}">
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                    @endif
                                        </span>
                                            </p>

                                            <p class="data-row">
                                                <label class="data-name">Mobile Phone</label>
                                                <span class="data-value full-width">{{$user->dealer->PIC_phone}}</span>
                                                <span class="data-input">
                                                <input type="number" name="PIC_phone" id="PIC_phone" placeholder="Mobile Phone PIC" required class="form-control" value="{{ $user->dealer->PIC_phone}}">
                                                    @if ($errors->has('PIC_phone'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('PIC_phone') }}</strong>
                                                            </span>
                                                        @endif
                                                </span>
                                            </p>

                                            <p class="data-row">
                                                <label class="data-name">Position</label>
                                                <span class="data-value full-width">
                                                    @if($user->dealer->PIC_jabatan=="")
                                                    - 
                                                    @endif
                                                    {{$user->dealer->PIC_jabatan}}</span>
                                                <span class="data-input">
                                                <select name="PIC_jabatan" id="PIC_jabatan" class="form-control">
                                                    <option value="">Choose Position</option>
                                                    <option value="Owner" @if($user->dealer->PIC_jabatan=="Owner") selected @endif>Owner</option>
                                                    <option value="Sales" @if($user->dealer->PIC_jabatan=="Sales") selected @endif>Sales</option>
                                                    <option value="Finance" @if($user->dealer->PIC_jabatan=="Finance") selected @endif>Finance</option>
                                                    <option value="Adminstration" @if($user->dealer->PIC_jabatan=="Adminstration") selected @endif>Adminstration</option>
                                                </select>
                                                    @if ($errors->has('PIC_jabatan'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('PIC_jabatan') }}</strong>
                                                            </span>
                                                        @endif
                                                </span>
                                            </p>




                                           <h4>PIC II</h4>
                                            <p class="data-row">
                                                <label class="data-name">Name</label>
                                                <span class="data-value full-width">
                                                    @if($user->dealer->PIC2_name=="") 
                                                    -
                                                    @endif
                                                    {{$user->dealer->PIC2_name}}</span>
                                                <span class="data-input">
                                        <input type="text" name="PIC_name2" id="PIC2_name" placeholder="Nama PIC Ke-2" class="form-control" value="{{ $user->dealer->PIC2_name }}">
                                                    @if ($errors->has('PIC2_name'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('PIC2_name') }}</strong>
                                                </span>
                                                    @endif
                                        </span>
                                            </p>
                                            <p class="data-row">
                                                <label class="data-name">Email</label>
                                                <span class="data-value full-width">
                                                    @if($user->dealer->PIC2_email=="") 
                                                    -
                                                    @endif
                                                    {{$user->dealer->PIC2_email}}</span>
                                                <span class="data-input">
                                        <input type="email" name="email2" class="form-control" id="PIC2_email" placeholder="Email PIC Ke-2" value="{{ $user->dealer->PIC2_email }}">
                                                    @if ($errors->has('PIC2_email'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('PIC2_email') }}</strong>
                                                </span>
                                                    @endif
                                        </span>
                                            </p>

                                            <p class="data-row">
                                                <label class="data-name">Mobile Phone</label>
                                                <span class="data-value full-width">
                                                    @if($user->dealer->PIC2_phone=="") 
                                                    -
                                                    @endif
                                                    {{$user->dealer->PIC2_phone}}</span>
                                                <span class="data-input">
                                        <input type="number" name="PIC_phone2" id="PIC2_phone" placeholder="Mobile Phone PIC Ke-2" class="form-control" value="{{ $user->dealer->PIC2_phone}}">
                                                    @if ($errors->has('PIC2_phone'))
                                                        <span class="help-block">
                                                    <strong>{{ $errors->first('PIC2_phone') }}</strong>
                                                </span>
                                                    @endif
                                    </span>
                                            </p>

                                        <p class="data-row">
                                                <label class="data-name">Position</label>
                                                <span class="data-value full-width">
                                                    @if($user->dealer->PIC2_jabatan=="")
                                                    - 
                                                    @endif
    
                                                    {{$user->dealer->PIC2_jabatan}}</span>
                                                <span class="data-input">
                                                <select name="PIC_jabatan2" id="PIC_jabatan2" class="form-control">
                                                    <option value="">Choose Position</option>
                                                    <option value="Owner" @if($user->dealer->PIC2_jabatan=="Owner") selected @endif>Owner</option>
                                                    <option value="Sales" @if($user->dealer->PIC2_jabatan=="Sales") selected @endif>Sales</option>
                                                    <option value="Finance" @if($user->dealer->PIC2_jabatan=="Finance") selected @endif>Finance</option>
                                                    <option value="Adminstration" @if($user->dealer->PIC2_jabatan=="Adminstration") selected @endif>Adminstration</option>
                                                </select>
                                                    @if ($errors->has('PIC_jabatan2'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('PIC_jabatan2') }}</strong>
                                                            </span>
                                                        @endif
                                                </span>
                                            </p>






                                        @endif





                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-right">
                                    <div class="profile-box-edit">
                                        <a href="#" class="btn btn-primary profile-edit"><i class="fa fa-pencil"></i> Edit Profile</a>
                                    </div>
                                    <div class="profile-box-save">
                                        <p>
                                            <a class="btn btn-danger profile-save-cancel"><i class="fa fa-close"></i> Batal</a>
                                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- END PROFILE TAB CONTENT -->

        <!-- OTENTIKASI TAB CONTENT -->
        <div class="tab-pane otentikasi" id="otentikasi-tab">
            <div class="box">
                <div class="box-body profile-box">
                    <form class="form-horizontal" method="POST" role="form" action="{{ route('user.update.password', $user->id) }}">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PATCH">
                <fieldset>
                    <h4><i class="fa fa-square"></i> Change Password</h4>
                    <div class="form-group">
                        <label for="old-password" class="col-sm-3 control-label">Old Password</label>
                        <div class="col-sm-4">
                            <input type="password" id="old-password" name="old-password" class="form-control" placeholder="Current Password / Old Password" required>
                            @if ($errors->has('old-password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('old-password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr />
                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">New Password</label>
                        <div class="col-sm-4">
                            <input type="password" id="password" name="password" class="form-control" required placeholder="New Password">
                            <p class="input-desciprtion">* include at least min 6 character, 1 letter and 1 number</p>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="col-sm-3 control-label">Repeat Password</label>
                        <div class="col-sm-4">
                            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" required placeholder="Repeat New Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </fieldset>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            </form>
                </div>
            </div>
        </div>


    <!-- END SETTINGS TAB CONTENT -->
    </div>

@endsection
