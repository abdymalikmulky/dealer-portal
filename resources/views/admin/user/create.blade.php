@extends('admin.layouts.app')

@section('page_title', 'Create  ' . $title)
@section('page_desc', '')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">

			<div class="box-body">
				<form class="form-horizontal" method="POST" role="form" action="{{ route('user.store') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<fieldset>
						<legend>Login Data {{$title}}</legend>
						<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
							<label for="username" class="col-sm-3 control-label">Username <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="username" class="form-control" id="username" placeholder="Username" required>
								@if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
                        {{--Jika bukan dealer, pake email sama name, kalo dealer cukup pake name company sama email pic--}}
                        @if(!$isForDealer)
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-sm-3 control-label">Email <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
								@if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">Name <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
								@if ($errors->has('name'))
									<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
                        @endif

						@if($isSuperAdmin)
						<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
							<label for="role" class="col-sm-3 control-label">Pilih Role <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<select id="role" name="role" class="form-control select-role" style="width:100%" required>
									@foreach($roles as $role)
									<option value="{{$role['id']}}">{{$role['name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						@else
							<input name="role" type="hidden" value="{{$roleFor}}">
						@endif
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-sm-3 control-label">Password <span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                <span class="input-message"><i>* minimum 6 characters</i></span>
								@if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							<label for="password_confirmation" class="col-sm-3 control-label">Password Confirmation<span class="color-red">*</span></label>
							<div class="col-sm-9">
								<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Konfirmasi Password" required>
								@if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
	
						
					</fieldset>

					
					<fieldset>
						<legend>Other Information of {{$title}}</legend>

						{{--Check if user is not dealer, dont use user detail table--}}
						@if(!$isForDealer)
                            {{-- Informasi User Selain Dealer--}}

                            <div class="form-group">
                                <label for="nip" class="col-sm-3 control-label">NPK <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="nip" id="nip" placeholder="NPK">
                                    @if ($errors->has('nip'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('nip') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-sm-3 control-label">Mobile Phone <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Nomor Telepon/HP" >
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Area <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="address" id="address" rows="5" cols="30" placeholder="Area (Timur, barat, selatan dan lainny)" required></textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="information" class="col-sm-3 control-label">Additional Information </label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="information" id="information" rows="5" cols="30" placeholder="Additional Information"></textarea>
                                    @if ($errors->has('information'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('information') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="photo" class="col-sm-3 control-label">Profile Photo <span class="color-red">*</span></label>
                                <div class="col-md-9">
                                    <input type="file" id="photo" name="photo" class="form-control" required>
                                    <p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB</em></p>
                                    @if ($errors->has('photo'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

						@else
                            {{-- ini akan tereksekusi kalo usernya bdm, saat ini hanya bdm aja yang bisa set--}}
                            {{-- Informasi User Dealer--}}

                            {{--Data Perusahaan--}}
                            <h4>Informasi Business Partner</h4>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Business Entity <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="company_type" id="company_type">
                                        <option value="PT"  @if(Request::old('company_type')=="PT") selected @endif>PT</option>
                                        <option value="CV"  @if(Request::old('company_type')=="CV") selected @endif>CV</option>
                                        <option value="PERORANGAN">PERORANGAN</option>
                                    </select>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Name <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nama business Partner" required value="{{Request::old('name')}}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regencies" class="col-sm-3 control-label">Choose City/Regency <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <select id="regencies" name="regency" class="form-control select-regencies" style="width:100%" required>
                                        @foreach($regencies as $regencie)
                                        <option value="{{$regencie['name']}}" @if(Request::old('regency')==$regencie['name']) selected @endif>{{$regencie['name']}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('regency'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('regency') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company_address" class="col-sm-3 control-label">Address <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="company_address" id="company_address" rows="5" cols="30" placeholder="Alamat business Partner" required>{{Request::old('company_address')}}</textarea>
                                    @if ($errors->has('company_address'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('company_address') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company_websites" class="col-sm-3 control-label">Websites <span class="color-red">*</span></label>
                                <div class="col-sm-9 form-website">
                                    <div id="box-form-website">
                                        <input type="url" required class="form-control company_website" name="company_websites[]" id="company_websites" placeholder="Website" onblur="checkURL(this)" value="{{Request::old('company_websites')[0]}}">  
                                        <a href="" target="_blank" class="show-website"><i class="fa fa-globe"></i></a>
                                        <a href="#" class="delete-company-website"><i class="fa fa-trash"></i></a>
                                    
                                    </div>
                                    
                                    @if ($errors->has('company_websites'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_websites') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-9">
                                    <p style="margin:5px 0px 0px 5px"><a href="#" class="btn btn-sm btn-primary add-website">Add website</a></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company_phone" class="col-sm-3 control-label">Office Phone Number <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="company_phone" id="company_phone" placeholder="Nomor Telepon/HP business Partner" required value="{{Request::old('company_phone')}}"> 
                                    @if ($errors->has('company_phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company_phone" class="col-sm-3 control-label">Upload Photo </label>
                                <div class="col-sm-9">
                                    
                                        <input type="file" id="photo" name="photo" class="form-control">
                                        <p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB</em></p>
                                        @if ($errors->has('photo'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
                                        @endif
                                    @if ($errors->has('company_phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{--Data PIC--}}
                            <h4>Informasi PIC I</h4>
                            <div class="form-group">
                                <label for="PIC_name" class="col-sm-3 control-label">Name <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="PIC_name" id="PIC_name" placeholder="Nama PIC" required value="{{Request::old('PIC_name')}}">
                                    @if ($errors->has('PIC_name'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_name') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="PIC_email" class="col-sm-3 control-label">Email <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email PIC" required value="{{Request::old('email')}}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="PIC_phone" class="col-sm-3 control-label">Mobile Phone <span class="color-red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="PIC_phone" id="PIC_phone" placeholder="Mobile Phone PIC" required  value="{{Request::old('PIC_phone')}}">
                                    @if ($errors->has('PIC_phone'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_phone') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="PIC_jabatan" class="col-sm-3 control-label">Position</label>
                                <div class="col-sm-9">
                                    <select name="PIC_jabatan" id="PIC_jabatan" class="form-control">
                                        <option value="Owner" @if(Request::old('PIC_jabatan')=="Owner") selected @endif>Owner</option>
                                        <option value="Sales" @if(Request::old('PIC_jabatan')=="Sales") selected @endif>Sales</option>
                                        <option value="Finance" @if(Request::old('PIC_jabatan')=="Finance") selected @endif>Finance</option>
                                        <option value="Adminstration" @if(Request::old('PIC_jabatan')=="Adminstration") selected @endif>Adminstration</option>
                                    </select>
                                    @if ($errors->has('PIC_jabatan'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_jabatan') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <h4>Informasi PIC II</h4>
                            <p class="input-desciprtion">* Kosongkan jika tidak ada PIC ke 2</p>
                            <div class="form-group">
                                <label for="PIC_name2" class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="PIC_name2" id="PIC_name2" placeholder="Nama PIC ke-2" value="{{Request::old('PIC_name2')}}">
                                    @if ($errors->has('PIC_name2'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_name2') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="PIC_email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email2" id="email2" placeholder="Email PIC ke-2" value="{{Request::old('email2')}}">
                                    @if ($errors->has('email2'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('email2') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="PIC_phone2" class="col-sm-3 control-label">Mobile Phone</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="PIC_phone2" id="PIC_phone2" placeholder="Mobile Phone PIC ke-2" value="{{Request::old('PIC_phone2')}}">
                                    @if ($errors->has('PIC_phone2'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_phone2') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="PIC_jabatan2" class="col-sm-3 control-label">Position </label>
                                <div class="col-sm-9">
                                    <select name="PIC_jabatan2" id="PIC_jabatan2" class="form-control">
                                       <option value="Owner" @if(Request::old('PIC_jabatan2')=="Owner") selected @endif>Owner</option>
                                        <option value="Sales" @if(Request::old('PIC_jabatan2')=="Sales") selected @endif>Sales</option>
                                        <option value="Finance" @if(Request::old('PIC_jabatan2')=="Finance") selected @endif>Finance</option>
                                        <option value="Adminstration" @if(Request::old('PIC_jabatan2')=="Adminstration") selected @endif>Adminstration</option>
                                    </select>
                                    @if ($errors->has('PIC_jabatan2'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('PIC_jabatan2') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>


                        @endif
					</fieldset>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
