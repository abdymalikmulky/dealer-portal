@extends('admin.layouts.app')

@section('page_title', $title)
@section('page_desc', 'All Data '.$title)

@section('content')
	<div class="table-action-add">
		<a href="{{ route('user.create') }}">
			<button type="button" class="btn btn-primary right"><span class="fa fa-plus"/>  Add</button>
		</a>
	</div>

	<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title">Data {{$title}}</h3> </em>
			</div>
			<div class="box-body">
				<div class="box-body">
					<table id="table-data" class="table table-striped table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>@if($isForDealer) Business Partner @else Name @endif</th>
								@if($isForDealer)
								<th>City/Regency</th>
								@endif
								<th>Username</th>
								<th>Email</th>
								{{--Cek kalo superadmin, kalo superadmin, munculin rolenya--}}
								@if($isSuperAdmin)
								<th>Role</th>
								@endif
								{{--<th>Status</th>--}}
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
							<tr>
								<td></td>
								<td>{{$user->name}}</td>
								@if($isForDealer)
								<th>{{$user->dealer->company_city}}</th>
								@endif
								<td>{{$user->username}}</td>
								<td>{{$user->email}}</td>
								@if($isSuperAdmin)
								<td>
									{{-- Data Roels --}}
									@foreach($user->roles as $role)
										<span class="label label-info">{{$role->name}}</span>
									@endforeach
								</td>
								@endif
								{{--<td>{{$user->status->name}}</td>--}}
								<td>
									<a href="{{ route('user.edit', $user->id) }}" title="Edit" class="element-floating-left">
										<button class="btn btn-xs btn-warning">
											<span class="fa fa-edit"/> Edit
										</button>
									</a>

									<form action="{{ route('user.delete', $user->id) }}" method="post" class="element-floating-left">
									{{csrf_field()}}
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-xs btn-danger" type="submit" onclick="if(!confirm('Are you sure want to delete  user {{ $user->id }} - {{$user->name }} ? ')){ return false; }" title="Delete"><span class="fa fa-ban"/> Delete</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
    </div>
</div>
@endsection
