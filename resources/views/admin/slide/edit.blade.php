@extends('admin.layouts.app')

@section('page_title', 'Edit Marketing Program')
@section('page_desc', 'Ubah Marketing Program pada sistem')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				{{--<div class="box-header">--}}
					{{--<h3 class="box-title"> Ubah Marketing Program</h3>--}}
				{{--</div>--}}
				<div class="box-body">
					<form class="form-horizontal" method="POST" slide="form" action="{{ route('slide.update', $slide->id) }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<fieldset>
							<legend>Data Marketing Program</legend>
							<input name="_method" type="hidden" value="PATCH">

							<div class="form-group{{ $errors->has('headline') ? ' has-error' : '' }}">
								<label for="headline" class="col-sm-3 control-label">Headline</label>
								<div class="col-sm-9">
									<input type="headline" name="headline" class="form-control" id="headline" placeholder="Headline Marketing Promo" value="{{ $slide->headline }}">
									@if ($errors->has('headline'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('headline') }}</strong>
	                                    </span>
	                                @endif
								</div>
							</div>
	


							<div class="form-group">
								<label for="photo" class="col-sm-3 control-label">Picture</label>
								<div class="col-md-9">
									<img src="{{ storage($slide->picture) }}" width="450px"/>
									<br/>
									<br/>
									<input type="file" id="photo" name="photo">
									<p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB | ratio 2 : 1 ex. image size: 1200px X 500px </em></p>
									@if ($errors->has('photo'))
										<span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
								<label for="url" class="col-sm-3 control-label">URL slide</label>
								<div class="col-sm-9">
									<input type="url" name="url" class="form-control" id="url" placeholder="Link Url Slide" value="{{ $slide->url }}">
									@if ($errors->has('url'))
										<span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
								<label for="desc" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-9">
									<textarea type="desc" name="desc" class="form-control textarea" id="desc" placeholder="Description slide">{{ $slide->description}}</textarea>
									@if ($errors->has('desc'))
										<span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block">Save</button>
								</div>
							</div>
						</fieldset>

					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
