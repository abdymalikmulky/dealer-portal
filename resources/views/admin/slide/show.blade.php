@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box" style="margin-top:10px;">
			<div class="news-detail row">
				<div class="col-md-12 news-detail-image">
					<img src="{{ storage($slide->picture) }}" alt="Picture"/>
				</div>
				<div class="col-md-12 news-detail-cont">
					<div class="news-detail-content">
						{!! $slide->description !!}
					</div>

				</div>
			</div>
		</div>

    </div>
</div>
@endsection
