@extends('admin.layouts.app')

@section('page_title', 'Marketing Program')
@section('page_desc', 'Data Marketing Program')

@section('content')
@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
<div class="table-action-add">

	<a href="{{ route('slide.create') }}">
		<button type="button" class="btn btn-primary right"><span class="fa fa-plus"/>  Add</button>
	</a>
</div>
@endif

@if(Auth::user()->roles[0]->id > Config::get('constants.PM'))
<div class="row">
    <div class="col-md-12 ">
		<div class="box" style="margin-top:10px;">

			<ul class="news">
				@foreach($slides as $slide)
				<li>
					<div class="row">
						<div class="col-md-3 tt-image">
							<img src="{{ storage($slide->picture) }}" alt="Picture"/>
						</div>
						<div class="col-md-9 ">
							<p class="tt-title"><a href="{{$slide->url}}" target="_blank">{{$slide->headline}}</a> </p>
							<p class="tt-date">
								{{ date("d M Y H:m:s", strtotime($slide->created_at))  }}
							</p>
							<div class="tt-content">
								{{ strip_tags(str_limit($slide->description, 200, "..."))  }} <a href='@if($slide->url!="") {{ $slide->url }} @else slide/{{ $slide->id }} @endif' style="color:skyblue">Read more</a>
							</div>

						</div>
						<!-- I got these buttons from simplesharebuttons.com -->
						<div id="share-buttons">
						    
						 	{{----}}
						    {{--<!-- Facebook -->--}}
						    {{--<a href="http://www.facebook.com/sharer.php?u={{$slide->url}}" target="_blank">--}}
						        {{--<img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />--}}
						    {{--</a>--}}
						    {{----}}
						    {{--<!-- Google+ -->--}}
						    {{--<a href="https://plus.google.com/share?url={{$slide->url}}" target="_blank">--}}
						        {{--<img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />--}}
						    {{--</a>--}}
						    {{----}}
						    {{--<!-- LinkedIn -->--}}
						    {{--<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{$slide->url}}" target="_blank">--}}
						        {{--<img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />--}}
						    {{--</a>--}}
						    {{----}}
						    {{--<!-- Twitter -->--}}
						    {{--<a href="https://twitter.com/share?url={{$slide->url}}&amp;text=Dealer%20Portal%20%20{{$slide->url}}&amp;hashtags=dealerportal" target="_blank">--}}
						        {{--<img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />--}}
						    {{--</a>--}}

							<!-- WA -->
							<a href="https://api.whatsapp.com/send?text=Check out our new promotion! @if($slide->url!="") {{ $slide->url }} @else {{URL::to('slide')}}/{{ $slide->id }} @endif" target="_blank">
								<img src="http://icons.iconarchive.com/icons/dtafalonso/android-l/256/WhatsApp-icon.png" alt="Twitter" />
							</a>
								{{--EMAIL--}}
							{{-- @if($slide->url!="") {{ $slide->url }} @else {{URL::to('slide')}}/{{ $slide->id }} @endif --}}
							<a href="mailto:?Subject={{$slide->headline}} &body={{strip_tags($slide->description)}} " target="_blank">
								<img src="https://simplesharebuttons.com/images/somacro/email.png" alt="Twitter" />
							</a>


						</div>
					
					</div>
					<div style="clear: both"></div>
				</li>
					<hr/>
				@endforeach
			</ul>

    </div>
    </div>
</div>
@endif

@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> Data Marketing Program</h3> </em>
			</div>
			<div class="box-body">

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Headline</th>
							<th>Picture</th>
							<th>Description</th>
							@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
							<th>Action</th>
							@endif
						</tr>
					</thead>
					<tbody>
						@foreach($slides as $slide)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>
								<a href="{{$slide->url}}" target="_blank">
									{{ $slide->headline }}
								</a>
							</td>
							<td><a href="{{$slide->url}}" target="_blank"><img src="{{ storage($slide->picture) }}" alt="Profile Picture" width="200px"/></a></td>
							<td>{{ strip_tags(str_limit($slide->description, 500, "..."))  }} </td>
							{{-- @if(Auth::user()->roles[0]->id <= Config::get('constants.PM')) --}}
							<td>
								<a href="{{ route('slide.edit', $slide->id) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a> 

								<form action="{{ route('slide.delete', $slide->id) }}" method="post" class="element-floating-left">
								{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" onclick="if(!confirm('Are you sure want to delete  slide {{ $slide->id }} {{$slide->url }}')) { return false; }" title="Delete"><span class="fa fa-ban"/> Delete</button>
								</form>
							</td>
							{{-- @endif --}}
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</div>
    </div>
</div>
@endif
@endsection
