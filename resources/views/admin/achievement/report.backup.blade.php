@extends('admin.layouts.app')

@section('page_title', 'Achievement')
@section('page_desc', 'Data Achievement')

@section('content')

<div class="row">
	<div class="col-md-12">

	<!-- WIDGET MAIN CHART WITH TABBED CONTENT -->
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><i class="fa fa-bar-chart-o"></i> Achievement</h3> <em>- Report Achievement COS, BDM & Dealer</em>
		</div>
		<div class="box-body">
			<!-- chart tab nav -->
			<div class="chart-nav row ">
				<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<div class="col-md-4">
							<strong>Category : </strong>
							<ul id="cat-tab">
								<li class="active"><a href="#bdm">BDM</a></li>
								<li><a href="#dealer">Dealer</a></li>
							</ul>
							<select name="year" id="year" >
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
							</select>
							<input type="hidden" value="#year" class="filter-tren"/>
						</div>
						<div class="col-md-4">
							<strong>Tren : </strong>
							<ul id="tren-tab">
								<li class="active"><a href="#year">Anually</a></li>
								<li><a href="#quarter">Quarter</a></li>
								<li><a href="#month">Monthly</a></li>
							</ul>
							<input type="hidden" value="#year" class="filter-tren"/>
						</div>

						<div class="col-md-4">
							<strong>Value : </strong>
							<ul id="value-tab">
								<li class="active"><a href="#revenue">Revenue</a></li>
								<li><a href="#count">Count Unit</a></li>
							</ul>
							<input type="hidden" value="#revenue" class="filter-value"/>
						</div>

						<div class="col-md-4">
							<strong>Tahun</strong>
							<select name="year" id="year" >
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
							</select>
							<input type="hidden" value="#revenue" class="filter-value"/>
						</div>
						<div class="col-md-4">
							<strong>Bulan</strong>
							<select name="bulan" id="year" >
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
							</select>
							<input type="hidden" value="#revenue" class="filter-value"/>
						</div>
						<div class="row" style="margin-right: 10px">
							<div class="col-md-11">
							</div>
							<div class="col-md-1">
								<button class="btn btn-success btn-sm chart-filter right"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
					</div>
				</div>


			</div>
			</div>


			<hr/>
			<!-- end chart tab nav -->
			<!-- chart placeholder-->
			<div class="chart-content">
				<div class="demo-flot-chart sales-chart"></div>
			</div>
			<!-- end chart placeholder-->
			<hr class="separator">
			<!-- secondary stat -->
			<div class="secondary-stat">
				<div class="row">
					<div class="col-lg-4">
						<div id="secondary-stat-item1" class="secondary-stat-item big-number-stat clearfix">
							<div class="data">
								<span class="col-left big-number">260</span>
								<span class="col-right"><em>New Orders</em><em>3% <i class="fa fa-caret-down"></i></em></span>
							</div>
							<div id="spark-stat1" class="inlinesparkline">Loading...</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div id="secondary-stat-item2" class="secondary-stat-item big-number-stat clearfix">
							<p class="data">
								<span class="col-left big-number">$23,000</span>
								<span class="col-right"><em>Revenue</em><em>5% <i class="fa fa-caret-up"></i></em></span>
							</p>
							<div id="spark-stat2" class="inlinesparkline">Loading...</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div id="secondary-stat-item3" class="secondary-stat-item big-number-stat clearfix">
							<p class="data">
								<span class="col-left big-number">$47,000</span>
								<span class="col-right"><em>Total Sales</em><em>7% <i class="fa fa-caret-up"></i></em></span>
							</p>
							<div id="spark-stat3" class="inlinesparkline">Loading...</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end secondary stat -->
		</div>
		</div>
	</div>
	<!-- END WIDGET MAIN CHART WITH TABBED CONTENT -->
</div>

@endsection
