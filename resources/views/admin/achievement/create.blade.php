@extends('admin.layouts.app')

@section('page_title', 'Add Achievement')
@section('page_desc', 'berdasarkan tahun')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Add Achievement</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" achievement="form" action="{{ route('achievement.store') }}">
					{{ csrf_field() }}
					<fieldset>

						<legend>Plan Setting</legend>

						<div class="form-group{{ $errors->has('dealer') ? ' has-error' : '' }}">
							<label for="role" class="col-sm-3 control-label">Business Partner</label>
							<div class="col-sm-9">
								<select id="dealer" name="dealer" class="form-control" style="width:100%" required>
									@foreach($dealers as $dealer)
										<option value="{{$dealer->user->id}}">{{$dealer['company_name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('dealer'))
									<span class="help-block">
                                        <strong>{{ $errors->first('dealer') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
							<label for="sn" class="col-sm-3 control-label">Year</label>
							<div class="col-sm-9">
								<input type="text" name="year" value="{{ Date("Y") }}" class="form-control year_input" id="year" placeholder="Achievement Year" required>
								@if ($errors->has('year'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						<div class="achievement-table">
							<table class="table table-bordered">
							
								@foreach($months as $month)
									<?php $loopMonth = $loop; ?>
									<tr>
										@if($loopMonth->first)
										<th style="background: #eee">Month/Unit</th>
										@else
										<td class="active">
											{{ $month['name'] }}
										</td>
										@endif
										
										@foreach($productCategories as $productCategory)
										@if($productCategory->description!="Optional")
											@if($loopMonth->first)	
												<td class="active">{{ $productCategory->description }} Revenue</td>
												
												@if($productCategory->description!="Consumable")
													<td class="active">Number of {{ $productCategory->description }}</td>
												@endif
											@else
												<td>
													<input value="2000" type="number" maxlength="20" name="target_{{ $productCategory->category }}_revenue_{{$month['id']}}" class="form-control" id="target_{{ $productCategory->category }}_revenue" placeholder="Target Achievement Revenue {{ $productCategory->description }}" required>
												</td>
												
												@if($productCategory->description!="Consumable")
													<td>
														<input value="20" type="text" name="target_{{ $productCategory->category }}_count_{{$month['id']}}" class="form-control" id="target_{{ $productCategory->category }}_count" placeholder="Target Achievement Count {{ $productCategory->description }}" required>
													</td>
												@endif
											@endif
										@endif
										@endforeach
									</tr>
								@endforeach
							</table>
						</div>

						{{-- 
						@foreach($months as $month)
							<legend>{{ $month->name }}</legend>
							@foreach($productCategories as $productCategory)
								<p class="achieve_category">{{ $productCategory->description }}</p>

								<div class="form-group{{ $errors->has('target_'.$productCategory->category.'_revenue') ? ' has-error' : '' }}">
									<label for="sn" class="col-sm-3 control-label">Target Revenue </label>
									<div class="col-sm-9">
										<input value="1000" type="text" name="target_{{ $productCategory->category }}_revenue_{{$month->id}}" class="form-control" id="target_{{ $productCategory->category }}_revenue" placeholder="Target Achievement Revenue {{ $productCategory->description }}" required>
										@if ($errors->has('target_'.$productCategory->category.'_revenue'))
											<span class="help-block">
                                        <strong>{{ $errors->first('target_'.$productCategory->category.'_revenue') }}</strong>
                                    </span>
										@endif
									</div>
								</div>
								<div class="form-group{{ $errors->has('target_'.$productCategory->category.'_count') ? ' has-error' : '' }}">
									<label for="sn" class="col-sm-3 control-label">Target Number of </label>
									<div class="col-sm-9">
										<input value="10" type="text" name="target_{{ $productCategory->category }}_count_{{$month->id}}" class="form-control" id="target_{{ $productCategory->category }}_count" placeholder="Target Achievement Count {{ $productCategory->description }}" required>
										@if ($errors->has('target_'.$productCategory->category.'_unit'))
											<span class="help-block">
                                        <strong>{{ $errors->first('target_'.$productCategory->category.'_count') }}</strong>
                                    </span>
										@endif
									</div>
								</div>


							@endforeach
						@endforeach --}}


						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-block">Save</button>
							</div>
						</div>
					</fieldset>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
