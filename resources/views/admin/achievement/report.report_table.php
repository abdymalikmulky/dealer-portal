<table id="table-achievement-report" class="table table-sorting table-striped table-hover">
	<thead>
		<tr>
			<th colspan="3" class="text-center">Indicator</th>
			<th colspan="4" class="text-center">Evaluation</th>
		</tr>
		<tr>
			{{-- INDICATOR --}}
			<td>
				Actual
			</td>
			<td>
				Plan
			</td>
			<td>
				LY
			</td>

			{{-- EVAL --}}
			<td>
				AR/PLAN
			</td>
			<td>
				GR/LY
			</td>

		</tr>
	</thead>
	<tbody>
		<tr>
			{{-- INDICATOR --}}
			<td>
				999
			</td>
			<td>
				999
			</td>
			<td>
				999
			</td>

			{{-- EVAL --}}
			<td>
				4599
			</td>
			<td>
				4599
			</td>
		</tr>
	</tbody>
</table>