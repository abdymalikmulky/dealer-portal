@extends('admin.layouts.app')

@section('page_title', 'Plan Setting')
@section('page_desc', '')

@section('content')

<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<a href="" class="close">×</a>
			<strong>Terjadi Kesalahan!</strong> {{Session::get('message')}}
		</div>
	@endif
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> </h3> </em>
			</div>
			<div class="box-body">
				<div class="table-action-add">
					<a href="{{ route('achievement.create') }}">
						<button type="button" class="btn btn-primary right"><span class="fa fa-plus"></span>  ADD</button>
					</a>
				</div>

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Business Partner</th>
							<th>Year</th>
							<th>Total Plan Revenue</th>
							<th>Total Plan Unit</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($achievements as $achievement)
							<?php
                            $totalAchievement = $achievement->totalAchievement($achievement->detail);
                            ?>
							<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $achievement->user->dealer->company_name }}</td>
							<td>{{ $achievement->year }}</td>
							<td>Rp. {{ number_format($totalAchievement['totalRevenue'], 0, 0, '.') }}</td>
							<td>{{ $totalAchievement['totalCount'] }}</td>
							<td>
								{{--<a href="#" title="Edit" class="element-floating-left">--}}
									{{--<button class="btn btn-xs btn-success">--}}
										{{--<span class="fa fa-eye"/> Detail--}}
									{{--</button>--}}
								{{--</a>--}}
								<a href="{{ route('achievement.edit', $achievement->id) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a>

								<form action="{{ route('achievement.delete', $achievement->id) }}" method="post" class="element-floating-left">
								{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" onclick="if(!confirm('Are you sure want to delete  achievement Tahun {{$achievement->year }}')){ return false; }" title="Delete"><span class="fa fa-ban"/> Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</div>
    </div>
</div>

@endsection
