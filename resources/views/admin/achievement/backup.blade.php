@extends('admin.layouts.app')

@section('page_title', 'Achievement')
@section('page_desc', 'Achievement Report')

@section('content')

<div class="row">
	<div class="col-md-12">

	<!-- WIDGET MAIN CHART WITH TABBED CONTENT -->
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><i class="fa fa-bar-chart-o"></i> Achievement</h3> <em>- Achievement Report for COS, BDM & Dealer</em>
		</div>
		<div class="box-body">
			<!-- chart tab nav -->
			<div class="chart-nav row ">
				<div class="col-md-12">
					<div class="box">
						<div class="box-body">
							<form action="" method="get">
								@if($roleId == Config::get('constants.PM'))
								<div class="form-group col-md-{{$bootstrapColSize}}" >
									<label>COS : </label>
									<select name="cos" class="form-control achievement-filter-cos" required role="{{Config::get('constants.COS')}}">
										<option value="">- Choose COS -	</option>
										<option value="all" @if(app('request')->input('cos')=='all') selected @endif>All COS</option>
										@if(!empty($coses))
											@foreach($coses as $cos)
											<option value="{{ $cos->user->id }}" @if(app('request')->input('cos')==$cos->user->id) selected @endif>{{ $cos->NIP }} - {{ $cos->user->name }}</option>
											@endforeach
										@endif
									</select>
								</div>
								@endif

								@if($roleId <= Config::get('constants.COS'))
								<div class="form-group col-md-{{$bootstrapColSize}}" >
									<label>BDM : </label>
									<select class="form-control display-gone achievement-filter-default-bdm">
										<option value="">- Choose BDM -</option>
										<option value="all"> ALL BDM </option>
									</select>
									<select name="bdm" class="form-control achievement-filter-bdm" required role="{{Config::get('constants.BDM')}}">
										<option value="">- Choose BDM -</option>
										<option value="all" @if(app('request')->input('bdm')=='all') selected @endif>All BDM</option>
										@if(!empty($bdms))
											@foreach($bdms as $bdm)
											<option value="{{ $bdm->user->id }}" @if(app('request')->input('bdm')==$bdm->user->id) selected @endif>{{ $bdm->NIP }} - {{ $bdm->user->name }}</option>
											@endforeach
										@endif
									</select>
								</div>
								@endif
	
								@if($roleId <= Config::get('constants.BDM'))
								<div class="form-group col-md-{{$bootstrapColSize}}">
									<label>Business Partner : </label>
									<select class="form-control display-gone achievement-filter-default-bp">
										<option value="">- Choose Business Partner -</option>
										<option value="all">All Business Partner</option>
									</select>
									<select name="dealer" class="form-control achievement-filter-bp" required role="{{Config::get('constants.DEALER')}}">
										<option value="">- Choose Business Partner -</option>
										<option value="all" @if(app('request')->input('dealer')=='all') selected @endif>All Business Partner</option>
										@if(!empty($dealers))
											@foreach($dealers as $dealer)
											<option value="{{ $dealer->user->id }}" @if(app('request')->input('dealer')==$dealer->user->id) selected @endif>{{ $dealer->company_name }}</option>
											@endforeach
										@endif
									</select>
								</div>
								@endif

								<div class="form-group col-md-{{$bootstrapColSize}}" >
									<label>Target : </label>
									<select name="target" id="filter-target" class="form-control" required>
										<option value="">- Choose Target -</option>
										<option value="revenue" @if(app('request')->input('target')=="revenue") selected @endif>Revenue</option>
										<option value="number_of_item" @if(app('request')->input('target')=="number_of_item") selected @endif>Number Of Item</option>
									</select>
								</div>
								<div class="form-group col-md-4" >
									<label>Tren : </label>
									<select name="tren" id="filter-tren" class="form-control" required>
										<option value="">- Choose Tren -</option>
										<option value="mtd" @if(app('request')->input('tren')=="mtd") selected @endif>Month to Date</option>
										<option value="ytd" @if(app('request')->input('tren')=="ytd") selected @endif>Year to Date</option>
									</select>
								</div>
								<div class="form-group col-md-4" >
									<label>Year : </label>
									<select name="year" id="filter-year" class="form-control" required>
										<option value="">- Choose Year -</option>
										@foreach($years as $year)
										<option value="{{ $year->year }}" @if(app('request')->input('year')==$year->year) selected @endif>{{ $year->year }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-4" >
									<label>Month : </label>
									<select name="month" class="form-control" required>
										<option value="">- Choose Month -</option>
										@foreach($months as $month)
											<option value="{{ $month->id }}" @if(app('request')->input('month')==$month->id) selected @endif>{{ $month->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="row">
									
									<div class="col-md-10">
									</div>
									<div class="col-md-2">
										<button class="btn btn-success btn-sm right form-control"><i class="fa fa-filter"></i> Filter</button>
									</div>
								</div>
							</form>
						</div>
					</div>


				</div>
			</div>
			@if(!empty($dataAchievementReport))
			{{-- BOX --}}
			<div class="row box-achievement-category">
				{{-- {{print_r($dataAchievementReport)}} --}}
		        @foreach($dataAchievementReport as $dataAchievement)
		        	@php
		        		$actualValue = $dataAchievement['actual_' . app('request')->input('target')];
		        		$actualLYValue = $dataAchievement['actual_' . app('request')->input('target') . '_last_year'];
		        		$planValue = $dataAchievement['plan_' . app('request')->input('target')];

		        		$actualValueFormated = number_format($actualValue, 0, '.', '.');
		        		if(app('request')->input('target') == "revenue") { 
		        			$actualValueFormated = "Rp. " . $actualValueFormated;
		        		} else {
		        			$actualValueFormated = $actualValueFormated . " Item ";
		        		}


		        		$grPlan = $dataAchievement['gr_plan_' . app('request')->input('target')];
		        		$grLastYear = $dataAchievement['gr_last_year_' . app('request')->input('target')];
		        		$arPlan = $dataAchievement['ar_plan_' . app('request')->input('target')];
		        		$arLastYear = $dataAchievement['ar_last_year_' . app('request')->input('target')];
		        	@endphp
					@if($dataAchievement['category']!="optional")
				        <div class="col-lg-4 col-xs-12 box-achievement-category-item @if(app('request')->input('target')=="number_of_item" AND $dataAchievement['category']=="consumable") display-gone @endif">
				          <div class="small-box @if($dataAchievement['category']=="unitmono") bg-aqua @elseif($dataAchievement['category']=="unitcolor") bg-green @else bg-red @endif">
				            <div class="inner">
				              <h3>{{ $dataAchievement['category'] }}</h3>
				              <p>{{ $actualValueFormated }}</p>
				            </div>
				            <div class="icon box-achievement-icon">
				              <i class="fa @if($dataAchievement['category']=="unitmono") fa-archive @elseif($dataAchievement['category']=="unitcolor") fa-archive @else fa-cubes @endif"></i>
				            </div>
				            <div class="box-achievement-category-ratio">
				            	<div class="row">
				            		{{-- <div class="col-md-6 box-achievement-category-ratio-item">
				            			GR/Plan <br><b><span class="item-grplan">{{ $grPlan }}</span></b> <i class="fa @if($grPlan < 0) fa-chevron-down @else fa-chevron-up @endif"></i>
				            		</div> --}}
				            		<div class="col-md-6 box-achievement-category-ratio-item">
				            			AR/Plan <br><b><span class="item-arplan">{{ $arPlan }}%</span></b> <i class="fa @if($arPlan < 0) fa-chevron-down @else fa-chevron-up @endif"></i>
				            		</div>
				            		<div class="col-md-6 box-achievement-category-ratio-item">
				            			GR/LY <br><b><span class="item-grly">{{ $grLastYear }}</span></b> <i class="fa @if($grLastYear < 0) fa-chevron-down @else fa-chevron-up @endif"></i>
				            		</div>
				            		{{-- <div class="col-md-6 box-achievement-category-ratio-item">
				            			AR/LY <br><b><span class="item-arplan">{{ $arLastYear }}%</span></b> <i class="fa @if($arLastYear < 0) fa-chevron-down @else fa-chevron-up @endif"></i>
				            		</div> --}}

				            	</div>
				            </div>
				            <a href="#" class="small-box-footer box-achievement-detail" category="{{ $dataAchievement['category'] }}" actual_value="{{ $actualValue }}" plan_value="{{ $planValue }}" last_year_value="{{ $actualLYValue }}" data-value="{{ json_encode($dataAchievement) }}" target="{{ app('request')->input('target') }}">View Detail <i class="fa fa-arrow-circle-right"></i></a>
				          </div>
				        </div>
			        @endif
		        @endforeach
		    </div>	
		    {{-- DETAIL --}}
	        <div class="row">
				<div class="col-md-6">
					<div class="box box-primary box-achievement-charttable">
						<div class="box-header with-border">
						  <i class="fa fa-bar-chart-o"></i>

						  <h3 class="box-title">Chart Report</h3>

						  {{-- <div class="box-tools pull-right">
						    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						    </button>
						    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						  </div> --}}
						</div>
			            <div class="box-body">
							<p class="box-achievement-warning-charttable">Click <b>View Detail <i class="fa fa-arrow-circle-right"></i></b> above, to show the Chart</p>
			            	<div id="bar-chart" class="box-achievement-report-data" style="height: 300px;"></div>
			            </div>
			            <!-- /.box-body-->
			        </div>
				</div>
				<div class="col-md-6">
					<div class="box box-primary box-achievement-charttable">
						<div class="box-header with-border">
						  <i class="fa fa-table"></i>

						  <h3 class="box-title">Table Report</h3>

						  {{-- <div class="box-tools pull-right">
						    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						    </button>
						    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						  </div> --}}
						</div>
			            <div class="box-body">
			            	<p class="box-achievement-warning-charttable">Click <b>View Detail <i class="fa fa-arrow-circle-right"></i></b> above, to show the Table</p>
			            	<div style="height:300px" class="box-achievement-report-data box-achievement-report-table">
			            	</div>
			            </div>
			            <!-- /.box-body-->
			        </div>

				</div>
			</div>
			@endif

			{{--<hr class="separator">--}}
			<!-- secondary stat -->
			{{--<div class="secondary-stat">--}}
				{{--<div class="row">--}}
					{{--<div class="col-lg-4">--}}
						{{--<div id="secondary-stat-item1" class="secondary-stat-item big-number-stat clearfix">--}}
							{{--<div class="data">--}}
								{{--<span class="col-left big-number">260</span>--}}
								{{--<span class="col-right"><em>New Orders</em><em>3% <i class="fa fa-caret-down"></i></em></span>--}}
							{{--</div>--}}
							{{--<div id="spark-stat1" class="inlinesparkline">Loading...</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<div class="col-lg-4">--}}
						{{--<div id="secondary-stat-item2" class="secondary-stat-item big-number-stat clearfix">--}}
							{{--<p class="data">--}}
								{{--<span class="col-left big-number">$23,000</span>--}}
								{{--<span class="col-right"><em>Revenue</em><em>5% <i class="fa fa-caret-up"></i></em></span>--}}
							{{--</p>--}}
							{{--<div id="spark-stat2" class="inlinesparkline">Loading...</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<div class="col-lg-4">--}}
						{{--<div id="secondary-stat-item3" class="secondary-stat-item big-number-stat clearfix">--}}
							{{--<p class="data">--}}
								{{--<span class="col-left big-number">$47,000</span>--}}
								{{--<span class="col-right"><em>Total Sales</em><em>7% <i class="fa fa-caret-up"></i></em></span>--}}
							{{--</p>--}}
							{{--<div id="spark-stat3" class="inlinesparkline">Loading...</div>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
			<!-- end secondary stat -->
		</div>
		</div>
	</div>
	<!-- END WIDGET MAIN CHART WITH TABBED CONTENT -->
</div>

@endsection
