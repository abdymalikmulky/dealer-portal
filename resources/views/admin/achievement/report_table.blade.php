<table id="table-achievement-report" class="table table-striped table-hover">
	<thead>
		<tr>
			<th colspan="3" class="text-center">Indicator</th>
			<th colspan="4" class="text-center">Evaluation</th>
		</tr>
		<tr>
			{{-- INDICATOR --}}
			<th>
				Actual
			</th>
			<th>
				Plan
			</th>
			<th>
				LY
			</th>

			{{-- EVAL --}}
			<th>
				AR/PLAN
			</th>
			<th>
				GR/LY
			</th>

		</tr>
	</thead>
	<tbody>
		@php
			$actualValueFormated = number_format($dataAchievement['actual_' . $target], 0, '.', '.');
			$actualPlanValueFormated = number_format($dataAchievement['plan_' . $target], 0, '.', '.');
			$actualLYValueFormated = number_format($dataAchievement['actual_' . $target . '_last_year'], 0, '.', '.');
    		
    		if($target == "revenue") { 
    			$actualValueFormated = "Rp. " . $actualValueFormated;
    			$actualPlanValueFormated = "Rp. " . $actualPlanValueFormated;
    			$actualLYValueFormated = "Rp. " . $actualLYValueFormated;
    		} else {
    			$actualValueFormated = $actualValueFormated . " Item ";
    			$actualPlanValueFormated = $actualPlanValueFormated . " Item ";
    			$actualLYValueFormated = $actualLYValueFormated . " Item ";
    		}
		@endphp
		<tr>
			{{-- INDICATOR --}}
			<td>
				{{ $actualValueFormated }}
			</td>
			<td>
				{{ $actualPlanValueFormated }}
			</td>
			<td>
				{{ $actualLYValueFormated }}
			</td>

			{{-- EVAL --}}
			<td>
				{{ $dataAchievement['ar_plan_' . $target] . "%" }}
			</td>
			<td>
				{{ $dataAchievement['gr_last_year_' . $target] }}
			</td>
		</tr>
		
	</tbody>
</table>