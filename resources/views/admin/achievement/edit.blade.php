@extends('admin.layouts.app')

@section('page_title', 'Edit Achievement')
@section('page_desc', '')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				{{--<div class="box-header">--}}
					{{--<h3 class="box-title"> Ubah Achievement</h3>--}}
				{{--</div>--}}
				<div class="box-body">
					<form class="form-horizontal" method="POST" achievement="form" action="{{ route('achievement.update', $achievement->id) }}">
						{{ csrf_field() }}
						<fieldset>
							<input name="_method" type="hidden" value="PATCH">
							<legend>Dealer & Tahun Target</legend>

							<div class="form-group{{ $errors->has('dealer') ? ' has-error' : '' }}">
								<label for="role" class="col-sm-3 control-label">Business Partner</label>
								<div class="col-sm-9">
									{{ $achievement->user->dealer->company_name }}
									<input type="hidden" name="dealer" value="{{ $achievement->user->dealer->user_id }}">
									@if ($errors->has('dealer'))
										<span class="help-block">
                                        <strong>{{ $errors->first('dealer') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
								<label for="sn" class="col-sm-3 control-label">Year</label>
								<div class="col-sm-9">
									<input type="text" name="year" value="{{ $achievement->year }}" class="form-control year_input" id="year" placeholder="Achievement Year" required>
									@if ($errors->has('year'))
										<span class="help-block">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="achievement-table">
								<table class="table table-bordered">
								
									@foreach($months as $month)
										<?php $loopMonth = $loop; ?>
										<tr>
											@if($loopMonth->first)
											<th style="background: #eee">Month/Unit</th>
											@else
											<td class="active">
												{{ $month['name'] }}
											</td>
											@endif
											
											@foreach($productCategories as $productCategory)
											@if($productCategory->description!="Optional")
												@if($loopMonth->first)	
													<td class="active">{{ $productCategory->description }} Revenue</td>
													
													@if($productCategory->description!="Consumable")
														<td class="active">Number of {{ $productCategory->description }}</td>
													@endif
												@else
													<td>
														<input value="{{ $achievementDetail[$month['id']-1]['target_'.$productCategory->category.'_revenue'] }}" type="number" maxlength="20" name="target_{{ $productCategory->category }}_revenue_{{$month['id']}}" class="form-control" id="target_{{ $productCategory->category }}_revenue" placeholder="Target Achievement Revenue {{ $productCategory->description }}" required>
													</td>
													
													@if($productCategory->description!="Consumable")
														<td>
															<input value="{{ $achievementDetail[$month['id']-1]['target_'.$productCategory->category.'_count'] }}" type="number" name="target_{{ $productCategory->category }}_count_{{$month['id']}}" class="form-control" id="target_{{ $productCategory->category }}_count" placeholder="Target Achievement Count {{ $productCategory->description }}" required>
														</td>
													@endif
												@endif
											@endif
											@endforeach
										</tr>
									@endforeach
								</table>
							</div>
							


							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block" 
									@if($roleId != Config::get('constants.PM')) onclick="if(!confirm('This change will notified to PM, are you sure want to edit this plan?')) { return false; } " @endif>Save</button>
								</div>
							</div>
						</fieldset>
						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
