@extends('admin.layouts.app')

@section('page_title', 'Add '.  $title)
@section('page_desc', 'Add '.  $title .' pada sistem')

@section('content')

<div class="row text-editor">
    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Add {{ $title }}</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" slide="form" action="{{ route('salestool.store') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<fieldset>
						<legend>Data {{ $title }}</legend>
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
							<label for="title" class="col-sm-3 control-label">Tools Name <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="title" class="form-control" id="title" placeholder="Nama {{ $title }}" required>
								@if ($errors->has('title'))
									<span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="col-sm-3 control-label">Description <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<textarea type="description" name="description" class="form-control" id="description" placeholder="Description {{ $title }}" required></textarea>
								@if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group">
							<label for="photo" class="col-sm-3 control-label">File Support <span class="color-red">*</span> </label>
							<div class="col-md-9">
								<input type="file" id="photo" name="photo" required>
								<p class="help-block"><em>tipe file : .pdf, .doc, .xls & Maximum file size: 2 MB</em></p>
								@if ($errors->has('photo'))
									<span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-block">Save</button>
							</div>
						</div>
					</fieldset>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
