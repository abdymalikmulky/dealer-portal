@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">

		<div class="news-detail row">
			<div class="col-md-12">
				<p class="news-detail-title">{{ $tipsTrick->title }}</p>

				<p class="news-detail-date">
					<i class="fa fa-user"></i> {{ $tipsTrick->user->name }} | <i class="fa fa-watch"></i> {{ date("d M Y H:m:s", strtotime($tipsTrick->created_at))  }}
				</p>
			</div>
			<div class="col-md-12 news-detail-image">
				<img src="{{ storage($tipsTrick->picture) }}" alt="Picture"/>
			</div>
			<div class="col-md-12 news-detail-cont">
				<div class="news-detail-content">
					{!! $tipsTrick->content !!}
				</div>
				@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
				<div class="news-detail-action">
					<a href="{{ route('tipstrick.edit', $tipsTrick->id) }}" title="Edit" class="element-floating-left">
						<button class="btn btn-xs btn-warning">
							<span class="fa fa-edit"/> Edit
						</button>
					</a>

					<form action="{{ route('tipstrick.delete', $tipsTrick->id) }}" method="post" class="element-floating-left">
						{{csrf_field()}}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  {{ $title }} {{ $tipsTrick->id }} - {{$tipsTrick->title }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
					</form>
				</div>
				@endif
			</div>
		</div>

    </div>
</div>
@endsection
