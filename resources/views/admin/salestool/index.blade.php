@extends('admin.layouts.app')

@section('page_title', $title)
@section('page_desc', 'Data ' . strtolower($title))

@section('content')

@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
	<div class="table-action-add">
		<a href="{{ route('salestool.create') }}">
			<button type="button" class="btn btn-primary right"><span class="fa fa-plus"/>  Add</button>
		</a>
	</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> Data {{ $title }}</h3> </em>
			</div>
			<div class="box-body">
				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
					<tr>
						<th>No</th>
						<th>Title</th>
						<th>Description</th>
						<th>Created Date</th>
						<th>File</th>
						@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
						<th>Action</th>
						@endif
					</tr>
					</thead>
					<tbody>
					@foreach($salesTools as $salesTool)
						<tr>
							<td style="width:30px;"></td>
							<td>{{ $salesTool->title }}</td>
							<td>{{ $salesTool->description }} </td>
							<td>{{ date('d-m-Y h:i', strtotime($salesTool->created_at)) }} </td>
							<td><a href="{{ storage($salesTool->file) }}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Download</a></td>
							@if(Auth::user()->roles[0]->id <= Config::get('constants.PM'))
							<td>
								<a href="{{ route('salestool.edit', $salesTool->id) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a>

								<form action="{{ route('salestool.delete', $salesTool->id) }}" method="post" class="element-floating-left">
									{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  {{ $title }} {{ $salesTool->id }} - {{$salesTool->title }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
								</form>
							</td>
							@endif
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
    </div>
    </div>
</div>
@endsection
