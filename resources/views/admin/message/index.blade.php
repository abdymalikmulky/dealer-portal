@extends('admin.layouts.app')

@section('page_title', $title)
@section('page_desc', 'Data ' . strtolower($title))

@section('content')
<div class="row">
	<div class="col-md-3">
		<a href="{{ route('message.compose') }}" class="btn btn-primary btn-block margin-bottom">Compose</a>
		@include('admin.message.menu')

	</div>
	<!-- /.col -->
	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">
					@if(Route::currentRouteName() == "message.sent")
						Sent
					@else
						Inbox
					@endif
				</h3>

				<!-- /.box-tools -->
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div class="mailbox-controls">
					<!-- Check all button -->
				</div>
						<div class="mailbox-messages">
							<div class="row">
								<div class="col-md-12 box-table-msg">
								<table id="table-data-message" class="table-responsive table table-sorting table-striped table-hover datatable">
								<thead style="display: none;">
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								@foreach($messages as $msg)
									<tr class='clickable-row' data-href='{{ route('message.show', $msg->id) }}'>
										<td class="mailbox-icon">
											@if($msg->read_status == 0)
												<i class="fa fa-envelope-o"></i>
											@else
												<i class="fa fa-envelope-open-o"></i>
											@endif
										</td>
										<td class="mailbox-name"><i>
												@if(Route::currentRouteName() == "message.sent")
													{{ $msg->receiver->name }}
												@else
													{{ $msg->sender->name }}
												@endif
											</i></td>
										<td class="mailbox-subject"><b>{{ $msg->subject }}</b></td>
										<td class="mailbox-attachment">{{ strip_tags(str_limit($msg->message, 60, "..."))  }}</td>
										<td class="mailbox-date">{{ $msg->timeago($msg->created_at) }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
							<!-- /.table -->
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer no-padding">

			</div>
		</div>
		<!-- /. box -->
	</div>
	<!-- /.col -->
</div>
@endsection
