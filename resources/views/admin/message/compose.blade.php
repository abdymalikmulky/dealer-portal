@extends('admin.layouts.app')

@section('page_title', 'Add')
@section('page_desc', 'Add '.  $title .' pada sistem')

@section('content')

	<div class="row">
		<div class="col-md-3">
			<a href="{{ route('message.compose') }}" class="btn btn-primary btn-block margin-bottom">Compose</a>
			@include('admin.message.menu')

		</div>
		<!-- /.col -->
		<div class="col-md-9">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Compose New Message</h3>
				</div>
				<!-- /.box-header -->


				<form  method="POST" role="form" action="{{ route('message.store') }}">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="form-group">
							<label>To: </label>
							<select id="receiver" name="receiver[]" class="form-control select2" @if(Auth::user()->roles[0]->id < Config::get('constants.DEALER')) multiple="multiple" @endif data-placeholder="To : " style="width:100%" required>
								@foreach($receivers as $receiver)
									<option @if(!empty($messageBefore)) @if($messageBefore->sender->id == $receiver->user_id) selected @endif @endif value="{{ $receiver->user_id }}"> [{{ $receiver->role_name }}] {{ $receiver->user_name }} </option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Subject: </label>
							<input name="subject" class="form-control" placeholder="Subject" value="@if(!empty($messageBefore)) {{ $messageBefore->subject }} @endif" required>
						</div>
						<div class="form-group">
						<textarea name="message" id="compose-textarea" class="form-control textarea" style="height: 300px" required>@if(!empty($messageBefore)) <i> {{$messageBefore->sender->name}} say</i> : {{ $messageBefore->message }} <br/>@endif</textarea>
						</div>

					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<div class="pull-right">
							<button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
						</div>
						<button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
					</div>
				</form>
				<!-- /.box-footer -->
			</div>
			<!-- /. box -->
		</div>
		<!-- /.col -->
	</div>
@endsection
