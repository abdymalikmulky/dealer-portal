

<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Folders</h3>

		<div class="box-tools">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			</button>
		</div>
	</div>



	<div class="box-body no-padding">
		<ul class="nav nav-pills nav-stacked">
			<li @if (strpos(request()->path(), 'sent') === false) class="active" @endif><a href="{{ route('message') }}"><i class="fa fa-inbox"></i> Inbox
					@if($messageCount > 0) <span class="label label-primary pull-right">{{ $messageCount }}</span>@endif</a></li>

			<li @if (strpos(request()->path(), 'sent') !== false) class="active" @endif><a href="{{ route('message.sent') }}"><i class="fa fa-envelope-o"></i> Sent</a></li>
		</ul>
	</div>
	<!-- /.box-body -->
</div>