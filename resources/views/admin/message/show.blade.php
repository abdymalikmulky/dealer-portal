@extends('admin.layouts.app')

@section('content')

<div class="row">
	<div class="col-md-3">
		<a href="{{ route('message.compose', array('message_before' => $message)) }}" class="btn btn-primary btn-block margin-bottom">Reply Message</a>
		@include('admin.message.menu')
	</div>
	<!-- /.col -->
	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Read Message</h3>

				<div class="box-tools pull-right">
					<a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
					<a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div class="mailbox-read-info">
					<h3>{{ $message->subject }}</h3>
					<h5>Dari: {{ $message->sender->name }}
						<span class="mailbox-read-time pull-right">{{ date('d M Y H:i:s', strtotime($message->created_at)) }}</span></h5>
				</div>
				{{--<!-- /.mailbox-read-info -->--}}
				{{--<div class="mailbox-controls with-border text-center">--}}
					{{--<div class="btn-group">--}}
						{{--<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">--}}
							{{--<i class="fa fa-trash-o"></i></button>--}}
						{{--<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">--}}
							{{--<i class="fa fa-reply"></i></button>--}}
						{{--<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">--}}
							{{--<i class="fa fa-share"></i></button>--}}
					{{--</div>--}}
					{{--<!-- /.btn-group -->--}}
					{{--<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">--}}
						{{--<i class="fa fa-print"></i></button>--}}
				{{--</div>--}}
				<!-- /.mailbox-controls -->
				<div class="mailbox-read-message">
					{!! $message->message !!}
				</div>
				<!-- /.mailbox-read-message -->
			</div>
			<!-- /.box-body -->
			{{--<div class="box-footer">--}}
				{{--<ul class="mailbox-attachments clearfix">--}}
					{{--<li>--}}
						{{--<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>--}}

						{{--<div class="mailbox-attachment-info">--}}
							{{--<a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Sep2014-report.pdf</a>--}}
							{{--<span class="mailbox-attachment-size">--}}
					  {{--1,245 KB--}}
					  {{--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>--}}
					{{--</span>--}}
						{{--</div>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<span class="mailbox-attachment-icon"><i class="fa fa-file-word-o"></i></span>--}}

						{{--<div class="mailbox-attachment-info">--}}
							{{--<a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> App Description.docx</a>--}}
							{{--<span class="mailbox-attachment-size">--}}
					  {{--1,245 KB--}}
					  {{--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>--}}
					{{--</span>--}}
						{{--</div>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo1.png" alt="Attachment"></span>--}}

						{{--<div class="mailbox-attachment-info">--}}
							{{--<a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> photo1.png</a>--}}
							{{--<span class="mailbox-attachment-size">--}}
					  {{--2.67 MB--}}
					  {{--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>--}}
					{{--</span>--}}
						{{--</div>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo2.png" alt="Attachment"></span>--}}

						{{--<div class="mailbox-attachment-info">--}}
							{{--<a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> photo2.png</a>--}}
							{{--<span class="mailbox-attachment-size">--}}
					  {{--1.9 MB--}}
					  {{--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>--}}
					{{--</span>--}}
						{{--</div>--}}
					{{--</li>--}}
				{{--</ul>--}}
			{{--</div>--}}

		</div>
		<!-- /. box -->
	</div>
	<!-- /.col -->
</div>

@endsection
