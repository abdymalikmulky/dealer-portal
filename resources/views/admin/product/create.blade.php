@extends('admin.layouts.app')

@section('page_title', 'Add Product')
@section('page_desc', 'Add product pada sistem')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Add product</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('product.store') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<fieldset>

						<div class="form-group{{ $errors->has('product_code') ? ' has-error' : '' }}">
							<label for="sn" class="col-sm-3 control-label">TL <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="product_code" class="form-control" id="product_code" placeholder="Product Code" required>
								@if ($errors->has('product_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('product_code') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
							<label for="sn" class="col-sm-3 control-label">Description <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="type" class="form-control" id="type" placeholder="Tipe product" required>
								@if ($errors->has('type'))
									<span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
							<label for="sn" class="col-sm-3 control-label">Category <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<select id="category" name="category" class="form-control" style="width:100%" required>
									<option value="">Pilih Kategori Product</option>
									@foreach($productCategories as $productCategory)
										<option value="{{ $productCategory->category }}">{{ ucwords($productCategory->category) }}</option>
									@endforeach
								</select>
								@if ($errors->has('category'))
									<span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="photo" class="col-sm-3 control-label">Picture <span class="color-red">*</span> </label>
							<div class="col-md-9">
								<input type="file" id="photo" name="photo">
								<p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB</em></p>
								@if ($errors->has('photo'))
									<span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-block">Save</button>
							</div>
						</div>
					</fieldset>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
