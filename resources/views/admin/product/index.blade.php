@extends('admin.layouts.app')

@section('page_title', 'Product')
@section('page_desc', 'Data product')

@section('content')
<div class="table-action-add">
	{{-- <button type="button" class="btn btn-success right upload-via-excel" data-toggle="modal" data-target="#import_product_excel"><span class="fa fa-upload"> </span>  Import via Excel</button> --}}
	<a href="{{ route('product.create') }}">
		<button type="button" class="btn btn-primary right"><span class="fa fa-plus"></span>  ADD</button>
	</a>
</div>
<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<a href="" class="close">×</a>
			<strong>Terjadi Kesalahan!</strong> {{Session::get('message')}}
		</div>
	@endif
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> Data Product</h3> </em>
			</div>
			<div class="box-body">
				<div class="box">
					<div  class="box-header text-left">
						<h3 class="box-title"> Filter</h3>
					</div>
					<div class="box-body text-left">
						<form action="#">
							<div class="form-group col-md-12">
								<label>Product Category : </label>
								<select name="category" id="filter-category-product" class="form-control">
									<option value="">- All Category - </option>
									@foreach($productCategories as $productCategory)
									<option value="{{ $productCategory->description }}">{{ strtoupper($productCategory->description) }}</option>
									@endforeach
								</select>
							</div>
						</form>
					</div>
				</div>


				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>TL</th>
							<th>Description</th>
							<th>Category</th>
							<th>Photo</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($products as $product)
						<tr>
							<td></td>
							<td>{{$product->product_code}}</td>
							<td>{{$product->type}}</td>
							<td>{{strtoupper($product->category)}}</td>
							<td>@if($product->photo == "")
									<img src="{{ storage('static_image/default.jpeg') }}" width="50px"/>
								@else
									<img src="{{ storage($product->photo) }}" width="75px"/>
								@endif
							</td>
							<td>
								<a href="{{ route('product.edit', $product->id) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a> 

								<form action="{{ route('product.delete', $product->id) }}" method="post" class="element-floating-left">
								{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  product {{ $product->id }} - {{$product->name }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</div>
    </div>
</div>

<div class="modal fade" id="import_product_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" method="POST" product="form" action="{{ route('product.import') }}" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="myModalLabel">Import Data Product</h3>
				</div>
				<div class="modal-body">
					<div>
						<img src="{{ storage("template/template-product.png") }}" />
						<h5 class="modal-download"><a href="{{ storage("document/template-product.xlsx") }}"><i class="fa fa-download"></i> Download Template</a></h5>
						<hr/>
						<div class="modal-upload-box">
							<h4>Upload File Data Product</h4>
							<br/>
							<input type="file" id="file" name="file" class="modal-upload-file">
							<p class="help-block"><em>Only  file : .xlxs</em></p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Import</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
