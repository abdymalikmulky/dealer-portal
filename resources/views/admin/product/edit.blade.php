@extends('admin.layouts.app')

@section('page_title', 'Edit Product')
@section('page_desc', 'UbahProduct pada sistem')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				{{--<div class="box-header">--}}
					{{--<h3 class="box-title"> Ubah Product</h3>--}}
				{{--</div>--}}
				<div class="box-body">
					<form class="form-horizontal" method="POST" role="form" action="{{ route('product.update', $product->id) }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<fieldset>
							<legend>Data Product</legend>
							<input name="_method" type="hidden" value="PATCH">

							<div class="form-group{{ $errors->has('product_code') ? ' has-error' : '' }}">
								<label for="sn" class="col-sm-3 control-label">Product Code <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="text" name="product_code" class="form-control" id="product_code" placeholder="Product Code" required value="{{ $product->product_code }}">
									@if ($errors->has('product_code'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product_code') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
								<label for="sn" class="col-sm-3 control-label">Tipe Product <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="text" name="type" class="form-control" id="type" placeholder="Tipe Product" required value="{{ $product->type }}">
									@if ($errors->has('type'))
										<span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
								<label for="sn" class="col-sm-3 control-label">Category Product <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<select id="category" name="category" class="form-control" style="width:100%" required>
										<option value="">Pilih Kategori Product</option>
										@foreach($productCategories as $productCategory)
											<option @if($product->category==$productCategory->category) selected @endif value="{{ $productCategory->category }}">{{ ucwords($productCategory->category) }}</option>
										@endforeach
									</select>
									@if ($errors->has('category'))
										<span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<label for="photo" class="col-sm-3 control-label">Picture <span class="color-red">*</span> </label>
								<div class="col-md-9">
									@if($product->photo != "")
										<img src="{{ storage($product->photo) }}" width="150px"/>
										<br/>
										<br/>
									@endif
									<input type="file" id="photo" name="photo">
									<p class="help-block"><em>Only  file : .jpg, .png & Maximum file size: 1 MB</em></p>
									@if ($errors->has('photo'))
										<span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block">Save</button>
								</div>
							</div>
						</fieldset>

					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
