@extends('admin.layouts.app')

@section('page_title', 'Add ' . $title )
@section('page_desc', '')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Add {{ $title }}</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellthrough.detail.store', $sellThroughId) }}">
					{{ csrf_field() }}
					<fieldset>
						<legend>Product List {{ $title }}</legend>
						<div>
							@if(app('request')->input('isInitStock'))
							<div class="form-group{{ $errors->has('product_type') ? ' has-error' : '' }}">
								<label for="product_type" class="col-sm-3 control-label">Product Category <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<select id="product_type" name="product_type" class="form-control list-product-type" style="width:100%" required>
										@foreach($productCategories as $productCategory)
											<option value="{{ $productCategory->category }}">{{ $productCategory->description }}</option>
										@endforeach
									</select>
									@if ($errors->has('product_type'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							@else
							<select id="product_type" name="product_type" class="form-control list-product-type" required style="display: none">
								<option value="{{ app('request')->input('product_type') }}">{{ app('request')->input('product_type') }}</option>
							</select>
							@endif
							<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
								<label for="product" class="col-sm-3 control-label">Product Code <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<select id="product" name="product" class="form-control list-product" style="width:100%" required>
										
									</select>
									@if ($errors->has('product'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							
							<div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
								<label for="qty" class="col-sm-3 control-label">Quantity <i class="fa fa-close st-multiplic-icon"></i> Revenue <span class="color-red">*</span> </label>
								<div class="col-sm-2">
									<input type="number" name="qty" class="form-control  @if(strpos(app('request')->input('product_type'), "unit") !== false && app('request')->input('isInitStock') == "0") sellthrough-qty @else sellthrough-qty-nonunit @endif" placeholder="Quantity" value=0" required>
									@if ($errors->has('qty'))
										<span class="help-block">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
									@endif
								</div>
								<div class="col-sm-1 text-center st-multiplic"><i class="fa fa-close st-multiplic-icon"></i></div>
								<div class="col-sm-2">
									<input type="number" name="revenue" class="form-control sellthrough-revenue" id="revenue" placeholder="Revenue per Product" required>
									@if ($errors->has('revenue'))
										<span class="help-block">
			                            <strong>{{ $errors->first('revenue') }}</strong>
			                        </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }} st-box-totalrevenue">
								<label for="product" class="col-sm-3 control-label">Total Revenue <span class="color-red"></span> </label>
								<div class="col-sm-9">
									<p class="control-label sellthrough-totalrevenue">Rp. 0</p>
								</div>
							</div>

							<div class="form-product-sn">
										
							</div>
						</div>

						
					</fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				</form>
			</div>
		</div>
    </div>
</div>
<div class="form-sn" style="display: none">
	<div class="form-group{{ $errors->has('sn') ? ' has-error' : '' }}">
		<label for="sn" class="col-sm-3 control-label">Serial Number <span class="color-red">*</span> </label>
		<div class="col-sm-9">
			<input type="text" name="sn[]" class="form-control serial-number" id="sellthrough-sn" placeholder="SN" required>
			@if ($errors->has('sn'))
				<span class="help-block">
                <strong>{{ $errors->first('sn') }}</strong>
            </span>
			@endif
		</div>
	</div>
</div>
@endsection

