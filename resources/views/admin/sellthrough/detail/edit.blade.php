@extends('admin.layouts.app')

@section('page_title', 'Edit ' . $title)
@section('page_desc', '')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Add {{ $title }}</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellthrough.detail.update', array('sellThroughId' => $sellThroughDetail->sell_through_id, 'sellThroughDetailId' => $sellThroughDetail->id)) }}">
					{{ csrf_field() }}
					<fieldset>
						<legend>Product List {{ $title }}</legend>
						<input name="_method" type="hidden" value="PATCH">
						<div>
							@if(app('request')->input('isInitStock'))
							<div class="form-group{{ $errors->has('product_type') ? ' has-error' : '' }}">

								<label for="product_type" class="col-sm-3 control-label">Product Category <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<select id="product_type" name="product_type" class="form-control list-product-type" style="width:100%" required>
										@foreach($productCategories as $productCategory)
											<option  value="{{ $productCategory->category }}" @if($productCategory->category==$sellThroughDetail->sellThrough->category) selected @endif>{{ $productCategory->description }}</option>
										@endforeach
									</select>
									@if ($errors->has('product_type'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							@else
							<select id="product_type" name="product_type" class="form-control list-product-type" required style="display: none" >
								<option value="{{ app('request')->input('product_type') }}">{{ app('request')->input('product_type') }}</option>
							</select>
							@endif
							<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
								<label for="product" class="col-sm-3 control-label">Product Code <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<select id="product" name="product" class="form-control list-product" style="width:100%" required data-product-id="{{$sellThroughDetail->product_id}}">
									</select>
									@if ($errors->has('product'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
								<label for="qty" class="col-sm-3 control-label">Quantity <i class="fa fa-close st-multiplic-icon"></i> Revenue <span class="color-red">*</span> </label>
								<div class="@if(strpos($productType, 'unit') !== false) col-sm-1 @else col-sm-2 @endif">
									@if(strpos($productType, 'unit') !== false  && app('request')->input('isInitStock') == "0")
										<label class="control-label sellthrough-qty product-qty">{{ $sellThroughDetail->qty }}</label> 
										@if(strpos($productType, 'unit') !== false) Unit @else pcs @endif</a></td>
									@else
										<input type="number" value="{{ $sellThroughDetail->qty }}" name="qty" class="form-control  @if(strpos(app('request')->input('product_type'), "unit"  && app('request')->input('isInitStock') == "0") !== false) sellthrough-qty @else sellthrough-qty-nonunit @endif" placeholder="Quantity" value=0" required>
									@endif
								</div>
								<div class="col-sm-1 text-center st-multiplic"><i class="fa fa-close st-multiplic-icon"></i></div>
								<div class="col-sm-2">
									<input type="number" name="revenue" class="form-control sellthrough-revenue" id="revenue" placeholder="Revenue per Product" required value="{{ $sellThroughDetail->revenue / $sellThroughDetail->qty }}">
									@if ($errors->has('revenue'))
										<span class="help-block">
			                            <strong>{{ $errors->first('revenue') }}</strong>
			                        </span>
									@endif
								</div>
							</div>

						<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }} st-box-totalrevenue">
							<label for="product" class="col-sm-3 control-label">Total Revenue <span class="color-red"></span> </label>
							<div class="col-sm-9">
								<p class="control-label sellthrough-totalrevenue">Rp. {{number_format($sellThroughDetail->revenue, 0, '.', '.') }}</p>
							</div>
						</div>

							<div class="form-product-sn">
								@foreach($sellThroughDetailProducts as $product)
								<div class="form-group{{ $errors->has('sn') ? ' has-error' : '' }}">
									<label for="sn" class="col-sm-3 control-label">Serial Number <span class="color-red">*</span> </label>
									<div class="col-sm-9">
										<input type="text" name="sn[]" class="form-control serial-number" id="sellthrough-sn" placeholder="SN" required value="{{ $product->serial_number }}">
										<p class="text-right" style="margin: 3px 2px 0px 0px;"><a href="#" class="product-item-delete">Delete</a></p>
										@if ($errors->has('sn'))
											<span class="help-block">
							                <strong>{{ $errors->first('sn') }}</strong>
							            </span>
										@endif
									</div>
								</div>
								@endforeach
							</div>
						</div>

					</fieldset>
					
					@if(strpos($productType, 'unit') !== false  && app('request')->input('isInitStock') == "0")
					<div class="text-right form-add-product-item">
						<a href="#" class="btn btn-success add-product-item">Add Product Item</a>
					</div>
					@endif

					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				</form>
			</div>
		</div>
    </div>
</div>
<div class="form-sn" style="display: none">
	<div class="form-group{{ $errors->has('sn') ? ' has-error' : '' }}">
		<label for="sn" class="col-sm-3 control-label">Serial Number <span class="color-red">*</span> </label>
		<div class="col-sm-9">
			<input type="text" name="sn[]" class="form-control serial-number" id="sellthrough-sn" placeholder="SN" required>
			<p class="text-right" style="margin: 3px 2px 0px 0px;"><a href="#" class="product-item-delete">Delete</a></p>
			@if ($errors->has('sn'))
				<span class="help-block">
                <strong>{{ $errors->first('sn') }}</strong>
            </span>
			@endif
		</div>
	</div>
</div>
@endsection

