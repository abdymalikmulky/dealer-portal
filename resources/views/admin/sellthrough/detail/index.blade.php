@extends('admin.layouts.app')

@section('page_title', $title . ' #' . $sellThrough->id)
@section('page_desc', 'Data ' . $title . ' dengan id #' . $sellThrough->id)



@section('content')

<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<a href="" class="close">×</a>
			<strong>Terjadi Kesalahan!</strong> {{Session::get('error')}}
		</div>
	@endif
	<div class="col-md-12">
		<div class="box">
			<div class="row" style="padding:10px">
				<div class="sell-box-date box-body col-md-6">
					<table>
						<tr>
							<th>
								Transaction Date
							</th>
							<td>
								:
							</td>
							<td>
								{{date("d M Y", strtotime($sellThrough->transaction_date))}}
							</td>
						</tr>
						<tr>
							<th>
								QTY 
							</th>
							<td>
								:
							</td>
							<td>
								{{$sellThrough->sellthroughQTY()}}
								@if(strpos($productType, 'unit') !== false) Unit @else pcs @endif</a></td>
							</td>
						</tr>
						<tr>
							<th>
								Revenue Total
							</th>
							<td>
								:
							</td>
							<td>
								Rp. {{number_format($sellThrough->sellthroughRevenue(), 0, '.', '.')}}
							</td>
						</tr>

					</table>
				</div>
				<div class="sellthrough-bp-box col-md-6" style="margin-top:5px;font-size: 18px">
					<b>Business Partner</b><br/>{{ $sellThrough->dealer->company_name }}
					<div>
						<img src="{{ storage($sellThrough->dealer->company_photo) }}" class="img-square" alt="User Avatar" width="100px"/>
					</div>
				</div>

			</div>
		</div>
	</div>
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title"> Data {{ $title }} #{{ $sellThrough->id }}</h3> </em>




			</div>


			<div class="box-body">

				<div class="table-action-add">
					
					<a href="{{ route('sellthrough.detail.create', array('id' => $sellThrough->id, 'isInitStock' => $isInitStock, 'product_type' => $sellThrough->category)) }}">
						<button type="button" class="btn btn-primary right"><span class="fa fa-plus"></span>  ADD</button>
					</a>

				</div>

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Product</th>
							<th>QTY</th>
							<th>Revenue Amount</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($sellThroughProducts as $sellThroughProduct)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>
								{{$sellThroughProduct->product->product_code}} - {{$sellThroughProduct->product->type}}</td>
							<td>
								{{$sellThroughProduct->qty}}
								@if(strpos($productType, 'unit') !== false) Unit @else pcs @endif</a></td>
							</td>
							<td>
								Rp. {{number_format($sellThroughProduct->revenue, 0, '.', '.')}}
							</td>

							<td>	
								
                                <a href="{{ route('sellthrough.detail.edit', array('id' => $sellThroughProduct->id, 'isInitStock' => $isInitStock, 'product_type' => $sellThrough->category)) }}" title="Edit" class="element-floating-left" >
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a>
								<form action="{{ route('sellthrough.detail.delete', array('sellThroughId' => $sellThroughProduct->sell_through_id, 'sellThroughDetailId' => $sellThroughProduct->id)) }}" method="post" class="element-floating-left">
								{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" title="Delete" onclick="if(!confirm('Are you sure want to delete  item ini?')){ return false; }"><span class="fa fa-ban"/> Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				
			</div>
		</div>
		</div>
    </div>
</div>
@endsection
