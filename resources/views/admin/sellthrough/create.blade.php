@extends('admin.layouts.app')

@section('page_title', 'Add '. $title)
@section('page_desc', '')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"> Data {{ $title }}</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellthrough.store', array('isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}">
					{{ csrf_field() }}
					<fieldset>

						<div class="form-group{{ $errors->has('dealer') ? ' has-error' : '' }}">
							<label for="role" class="col-sm-3 control-label">Business Partner <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<select id="dealer" name="dealer" class="form-control" style="width:100%" required>
									@foreach($dealers as $dealer)
										<option value="{{$dealer['id']}}">{{$dealer['company_name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('dealer'))
									<span class="help-block">
                                        <strong>{{ $errors->first('dealer') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('transaction_date') ? ' has-error' : '' }}">
							<label for="transaction_date" class="col-sm-3 control-label">Transaction Date <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="transaction_date" class="form-control date_input" id="transaction_date" placeholder="Transaction Date" required>
								@if ($errors->has('transaction_date'))
									<span class="help-block">
                                        <strong>{{ $errors->first('transaction_date') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						@if(!$isInitStock)
							<input type="hidden" id="product_type" name="product_type" value="{{ app('request')->input('product_type') }}"/>
						@endif
						

					</fieldset>
					<fieldset class="field-list-product">
						<legend>Product List {{ $title }} ( <span class="form-total-product">1</span> Product )</legend>

						<div class="form-list-product">
							<div class="form-item-product">
								@if($isInitStock)
								<div class="form-group{{ $errors->has('product_type') ? ' has-error' : '' }}">
									<label for="product_type" class="col-sm-3 control-label">Product Category <span class="color-red">*</span> </label>
									<div class="col-sm-9">
										<select id="product_type" name="product_type" class="form-control list-product-type" style="width:100%" required>
											@foreach($productCategories as $productCategory)
												<option value="{{ $productCategory->category }}">{{ $productCategory->description }}</option>
											@endforeach
										</select>
										@if ($errors->has('product_type'))
											<span class="help-block">
	                                        <strong>{{ $errors->first('product_type') }}</strong>
	                                    </span>
										@endif
									</div>
								</div>
								@endif
								
								<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
									<label for="product" class="col-sm-3 control-label">Product Code <span class="color-red">*</span> </label>
									<div class="col-sm-9">
										<select id="product" name="product[]" class="form-control list-product" style="width:100%" required>
										</select>
										@if ($errors->has('product'))
											<span class="help-block">
	                                        <strong>{{ $errors->first('product') }}</strong>
	                                    </span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
									<label for="qty" class="col-sm-3 control-label">Quantity <i class="fa fa-close st-multiplic-icon"></i> Revenue <span class="color-red">*</span> </label>
									<div class="col-sm-2">
										<input type="number" name="qty[]" class="form-control @if(strpos(app('request')->input('product_type'), "unit") !== false) sellthrough-qty @else sellthrough-qty-nonunit @endif" placeholder="Quantity" value=0" required>
										@if ($errors->has('qty'))
											<span class="help-block">
	                                        <strong>{{ $errors->first('qty') }}</strong>
	                                    </span>
										@endif
									</div>
									<div class="col-sm-1 text-center st-multiplic"><i class="fa fa-close st-multiplic-icon"></i></div>
									<div class="col-sm-2">
										<input type="number" name="revenue[]" class="form-control sellthrough-revenue" id="revenue" placeholder="Revenue per Product" required>
										@if ($errors->has('revenue'))
											<span class="help-block">
				                            <strong>{{ $errors->first('revenue') }}</strong>
				                        </span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }} st-box-totalrevenue">
									<label for="product" class="col-sm-3 control-label">Total Revenue <span class="color-red"></span> </label>
									<div class="col-sm-9">
										<p class="control-label sellthrough-totalrevenue">Rp. 0</p>
									</div>
								</div>
								
								<div class="form-product-sn">
									
								</div>

								

								<div class="form-group-remove text-right">
									<a href="#" class="remove-product"><i class="fa fa-big fa-trash" style="color:maroon;font-size:20px;"></i></a>
								</div>

								<hr/>
							</div>
						</div>
						<div class="text-right form-add-product">
							<a href="#" class="btn btn-success add-product">Add Product</a>
						</div>

						
					</fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				</form>
			</div>
		</div>
    </div>
</div>
<div class="form-sn" style="display: none">
	<div class="form-group{{ $errors->has('sn') ? ' has-error' : '' }}">
		<label for="sn" class="col-sm-3 control-label">Serial Number <span class="color-red">*</span> </label>
		<div class="col-sm-9">
			<input type="text" name="sn[]" class="form-control serial-number" id="sellthrough-sn" placeholder="SN" required>
			@if ($errors->has('sn'))
				<span class="help-block">
                <strong>{{ $errors->first('sn') }}</strong>
            </span>
			@endif
		</div>
	</div>
</div>
@endsection

