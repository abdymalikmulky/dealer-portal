@extends('admin.layouts.app')

@section('page_title', 'Ubah ' . $title . ' #' . $sellThrough->id)
@section('page_desc', '')

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"> Data {{ $title }}</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellthrough.update', array('id' => $sellThrough->id, 'isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}">
					{{ csrf_field() }}
					<fieldset>
						<input name="_method" type="hidden" value="PATCH">
						<div class="form-group{{ $errors->has('dealer') ? ' has-error' : '' }}">
							<label for="role" class="col-sm-3 control-label">Business Partner <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<select id="dealer" name="dealer" class="form-control" style="width:100%" required>
									@foreach($dealers as $dealer)
										<option value="{{$dealer['id']}}" @if($dealer['id'] == $sellThrough->dealer_id) selected @endif>{{$dealer['company_name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('dealer'))
									<span class="help-block">
                                        <strong>{{ $errors->first('dealer') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('transaction_date') ? ' has-error' : '' }}">
							<label for="transaction_date" class="col-sm-3 control-label">Transaction Date <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="transaction_date" class="form-control date_input" id="transaction_date" placeholder="Tanggal Sellout" required value="{{ date("d-m-Y", strtotime($sellThrough->transaction_date)) }}">
								@if ($errors->has('transaction_date'))
									<span class="help-block">
                                        <strong>{{ $errors->first('transaction_date') }}</strong>
                                    </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('revenue') ? ' has-error' : '' }}">
							<label for="revenue" class="col-sm-3 control-label">QTY </label>
							<div class="col-sm-9">
								<label class="control-label">
									{{$sellThrough->sellthroughQTY()}} 
									&nbsp;
									<a href="{{ route('sellthrough.show', array('id' => $sellThrough->id, 'isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}" title="Edit" class="element-floating-left">
											<span class="fa fa-pencil"/> Edit
									</a>
								</label>
							</div>
						</div>
						<div class="form-group{{ $errors->has('revenue') ? ' has-error' : '' }}">
							<label for="revenue" class="col-sm-3 control-label">Revenue Total </label>
							<div class="col-sm-9">
								<label class="control-label">
									Rp. {{number_format($sellThrough->sellthroughRevenue(),0,'.','.')}}
									&nbsp;
									<a href="{{ route('sellthrough.show', array('id' => $sellThrough->id, 'isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}" title="Edit" class="element-floating-left">
											<span class="fa fa-pencil"/>  Edit
									</a>
								</label>
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				</form>
			</div>
		</div>
    </div>
</div>
@endsection

