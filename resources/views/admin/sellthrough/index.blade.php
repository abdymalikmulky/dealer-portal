@extends('admin.layouts.app')

@section('page_title', $title)
@section('page_desc', 'Data ' . $title)

@section('content')
<input type="hidden" value="{{ Route::currentRouteName() }}" class="current_route">
<input type="hidden" value="{{ app('request')->input('isInitStock') }}" class="is_init_stock">
<input type="hidden" value="{{ app('request')->input('product_type') }}" class="product_type">
	<div class="table-action-add">
		{{-- <button type="button" class="btn btn-success right upload-via-excel" data-toggle="modal" data-target="#import_sellThrough_excel"><span class="fa fa-upload"> </span>  Import via Excel</button> --}}
		@if(Auth::user()->roles[0]->id == Config::get('constants.BDM'))
		<a href="{{ route('sellthrough.create', array('isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}">
			<button type="button" class="btn btn-primary right"><span class="fa fa-plus"></span>  ADD</button>
		</a>
		@endif

	</div>
<div class="row">
	@if(Session::has('error'))
		<div class="alert alert-danger alert-dismissable">
			<a href="" class="close">×</a>
			<strong>Terjadi Kesalahan!</strong> {{Session::get('error')}}
		</div>
	@endif

    <div class="col-md-12">
		<div class="box">	
			<div class="box-body">
				<div class="box">
					<div  class="box-header text-left">
						<h3 class="box-title"> Filter</h3>
					</div>
					<div class="box-body text-left">
						<form action="#">
							@if(Auth::user()->roles[0]->id == Config::get('constants.COS'))
							<div class="form-group col-md-6" >
								<label>BDM : </label>
								<select name="user" id="filter-by-bdm" class="form-control">
									<option value="">- All BDM - </option>
									@foreach($bdms as $bdm) 
									<option value="{{ $bdm->user_id }}" @if($bdm->user_id == app('request')->input('bdm')) selected @endif>{{ $bdm->user->name }}</option>
									@endforeach
								</select>
							</div>
							@endif

							<div class="form-group col-md-6" >
								<label>Business Partner : </label>
								<select name="user" id="filter-by-user" class="form-control">
									<option value="">- All Business Partner - </option>
									@foreach($dealers as $dealer) 
									<option value="{{ $dealer->company_name }}">{{ $dealer->company_name }}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group col-md-6" style="display: none">
								<label>Product Category : </label>
								<select name="category" id="filter-category" class="form-control">
									<option value="">- All Category - </option>
									@foreach($productCategories as $productCategory)
									<option value="{{ $productCategory->description }}">{{ strtoupper($productCategory->description) }}</option>
									@endforeach
								</select>
							</div>
						</form>
					</div>
				</div>

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Business Partner</th>
							<th>Transaction Date</th>
							<th>QTY (@if(strpos($productType, 'unit') !== false) Unit @else pcs @endif)</th>
							<th>Revenue (Rp.)</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th></th>
							<th>Business Partner</th>
							<th>Transaction Date</th>
							<th>QTY (@if(strpos($productType, 'unit') !== false) Unit @else pcs @endif)</th>
							<th>Revenue (Rp.)</th>
							{{-- <th>Action</th> --}}
						</tr>
					</tfoot>
					<tbody>
						@foreach($sellThroughs as $sellThrough)
						<tr>
							<td></td>
							<td>{{ $sellThrough->dealer->company_name }}</td>
							<td>{{date("d M Y", strtotime($sellThrough->transaction_date))}}</td>
							<td class="center">
								{{ $sellThrough->sellthroughQTY() }}
								@if(strpos($productType, 'unit') !== false) Unit @else pcs @endif
							</td>
							</td>
							<td>
								{{number_format($sellThrough->sellthroughRevenue(),0,',',',')}}						
							</td>
								{{-- {{count($sellThrough->sellthrough_products)}} tipe Product</td> --}}
							<td>
								<a href="{{ route('sellthrough.show', array('id' => $sellThrough->id, 'isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-success">
										<span class="fa fa-eye"/> Detail
									</button>
								</a>
								{{-- Only Form BDM --}}
								@if(Auth::user()->roles[0]->id == Config::get('constants.BDM'))
                                <a href="{{ route('sellthrough.edit', array('id' => $sellThrough->id, 'isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}" title="Edit" class="element-floating-left">
									<button class="btn btn-xs btn-warning">
										<span class="fa fa-edit"/> Edit
									</button>
								</a>

								<form action="{{ route('sellthrough.delete', array('id' => $sellThrough->id, 'isInitStock' => app('request')->input('isInitStock'),'product_type' => app('request')->input('product_type'))) }}" method="post" class="element-floating-left">
								{{csrf_field()}}
									<input name="_method" type="hidden" value="DELETE">
									<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  sellThrough {{ $sellThrough->id }} - {{$sellThrough->name }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
								</form>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
    </div>
</div>

<div class="modal fade" id="import_sellThrough_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" method="POST" sellThrough="form" action="{{ route('sellthrough.import') }}" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="myModalLabel">Import Data {{ $title }}</h3>
				</div>
				<div class="modal-body">
					<div style="text-align: center">
						<img src="{{ storage("template/template-sellthrough.png") }}" />
						<h5 class="modal-download"><a href="{{ storage("document/template-sellthrough.xlsx") }}"><i class="fa fa-download"></i> Download Template</a></h5>
						<hr/>
						<input type="hidden" name="is_init_stock" value="{{ app('request')->input('isInitStock') }}"/>

						@if($isInitStock)
							<div class="form-group{{ $errors->has('product_type') ? ' has-error' : '' }}">
								<label for="product_type" class="col-sm-3 control-label">Pilih Jenis Sellout Product</label>
								<div class="col-sm-9">
									<select id="product_type" name="product_type" class="form-control" style="width:100%" required>
										@foreach($productCategories as $productCategory)
											<option value="{{ $productCategory->category }}">{{ $productCategory->description }}</option>
										@endforeach
									</select>
									@if ($errors->has('product_type'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product_type') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
						@else
							<input type="hidden" name="product_type" value="{{ app('request')->input('product_type') }}"/>
						@endif

						<div class="form-group{{ $errors->has('dealer') ? ' has-error' : '' }}">
							<label for="role" class="col-sm-3 control-label">Business Partner</label>
							<div class="col-sm-9">
								<select id="dealer" name="dealer" class="form-control" style="width:100%" required>
									@foreach($dealers as $dealer)
										<option value="{{$dealer['id']}}">{{$dealer['company_name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('dealer'))
									<span class="help-block">
                                        <strong>{{ $errors->first('dealer') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="modal-upload-box">
							<h4>Upload File Data {{ $title }}</h4>
							<br/>
							<input type="file" id="file" name="file" class="modal-upload-file">
							<p class="help-block"><em>Only  file : .xlxs</em></p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Import</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
