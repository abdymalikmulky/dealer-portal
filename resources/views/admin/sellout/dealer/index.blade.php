@extends('admin.layouts.app')

@if( ! empty($reseller))
	@section('page_title', 'Sell Out ' . $reseller->name)
	@section('page_desc', 'Data sell out Reseller ' . $reseller->name . ' dari dealer ' . $dealer->name)
@else
	@section('page_title', 'Sell Out ')
	@section('page_desc', 'Data sell out dealer ' . $dealer->name)
@endif

@section('content')
<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div  class="box-header">
				<h3 class="box-title">  Data Sell Out | @if( ! empty($reseller)) Reseller : {{ $reseller->name }} @endif Dealer : {{ $dealer->name }}</h3> </em>
			</div>
			<div class="box-body">
				<div class="table-action-add">	

					<a href="{{ route('sellout.dealer.create', $dealer->id) }}">
						<button type="button" class="btn btn-primary right"><span class="fa fa-plus-square"/>  Input Sell Out</button>
					</a>
				</div>

				<table id="table-data" class="table table-sorting table-striped table-hover datatable">
					<thead>
						<tr>
							<th>No</th>
							<th>Reseller</th>
							<th>Product SN</th>
							<th>Nama Customer</th>
							<th>Invoice</th>
							<th>Waktu Transaksi</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php $totalPoint = 0; ?>
						@foreach($sellOuts as $sellOut)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>
									<a href="#">
										{{ $sellOut->reseller->name }}
									</a>
								</td>
								<td>
									<a href="#">
										{{ $sellOut->product->SN }}
									</a>
									<br/>
									<i>Point : {{ $sellOut->product->ligaResellerProduct->point }}</i>
								</td>
								<td>{{ $sellOut->customer_name }}</td>
								<td>@if($sellOut->invoice_photo == "")
										<i style="color:red">Tidak Ada Bukti Pembayaran</i>
									@else
										<a href="{{ asset('images/'.$sellOut->invoice_photo) }}" target="_blank">
											<img src="{{ asset('images/'.$sellOut->invoice_photo) }}" width="100px"/>
										</a>
									@endif

								</td>
								<td>{{ $sellOut->created_at }}</td>
								<td>

										<span class="@if($sellOut->status_id == config('constants.APPROVED')) text-success @elseif($sellOut->status_id == config('constants.REJECTED')) text-danger @else text-muted @endif">
											{{ $sellOut->status->name }}
										</span>

								</td>

								<td>
									<form action="{{ route('sellout.delete', $sellOut->id) }}" method="post" class="element-floating-left">
									{{csrf_field()}}
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-xs btn-danger" type="submit" onclick="confirm('Are you sure want to delete  product {{ $sellOut->id }} - {{$sellOut->customerName }}')" title="Delete"><span class="fa fa-ban"/> Delete</button>
									</form>
								</td>
						</tr>
                            <?php $totalPoint += $sellOut->product->ligaResellerProduct->point; ?>
						@endforeach
					</tbody>
				</table>
				<h2>Count Sell Out : {{ $sellOuts->count() }} Product</h2>
				<h2>Total Point : {{ $totalPoint }}</h2>
			</div>
		</div>
		</div>
    </div>
</div>
@endsection
