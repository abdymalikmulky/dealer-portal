@extends('admin.layouts.app')

@section('page_title', 'Add')
@section('page_desc', 'Add Product pada sistem')

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"> Add Product</h3>
				</div>
				<div class="box-body">
					<form class="form-horizontal" method="POST" role="form" action="{{ route('product.update', $product->id) }}">
						{{ csrf_field() }}
						<fieldset>
							<legend>Data Product</legend>
							<input name="_method" type="hidden" value="PATCH">
							<div class="form-group{{ $errors->has('sn') ? ' has-error' : '' }}">
								<label for="sn" class="col-sm-3 control-label">Serial Number <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="text" name="sn" class="form-control" id="sn" placeholder="Serial Number (SN)" required value="{{ $product->SN }}">
									@if ($errors->has('sn'))
										<span class="help-block">
                                        <strong>{{ $errors->first('sn') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-sm-3 control-label">Nama product <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="text" name="name" class="form-control" id="name" placeholder="Nama product" required value="{{ $product->name }}">
									@if ($errors->has('name'))
										<span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('review') ? ' has-error' : '' }}">
								<label for="review" class="col-sm-3 control-label">Description <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<textarea type="review" name="review" class="form-control" id="review" placeholder="Review product" required>{{ $product->review}}</textarea>
									@if ($errors->has('review'))
										<span class="help-block">
                                        <strong>{{ $errors->first('review') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-block">Save</button>
								</div>
							</div>
						</fieldset>

					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
