@extends('admin.layouts.app')

@section('page_title', 'Sell Out')
@section('page_desc', 'Sell Out Dealer ' . $dealer->name)

@section('content')

<div class="row">
    <div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Sell Out | {{ $dealer->name }} </h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellout.dealer.store', $dealer->id) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<fieldset>
						<input type="hidden" name="user_id" value="{{ Auth::user()->id }}"/>
						<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
							<label for="product" class="col-sm-3 control-label">Pilih Product <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<select id="product" name="product" class="form-control select-role" required>
									@foreach($products as $product)
										<option value="{{$product['id']}}">{{$product['name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('product'))
									<span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('reseller') ? ' has-error' : '' }}">
							<label for="product" class="col-sm-3 control-label">Pilih reseller <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<select id="reseller" name="reseller" class="form-control select-role" required>
									@foreach($resellers as $reseller)
										<option value="{{$reseller['id']}}">{{$reseller['name']}}</option>
									@endforeach
								</select>
								@if ($errors->has('reseller'))
									<span class="help-block">
                                        <strong>{{ $errors->first('reseller') }}</strong>
                                    </span>
								@endif
							</div>
						</div>


						<div class="form-group{{ $errors->has('customer_name') ? ' has-error' : '' }}">
							<label for="customer_name" class="col-sm-3 control-label">Nama customer <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="customer_name" class="form-control" id="customer_name" placeholder="Nama customer" required>
								@if ($errors->has('customer_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_name') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('invoice') ? ' has-error' : '' }}">
							<label for="invoice" class="col-sm-3 control-label">Bukti Transaksi (Invoice) <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="file" name="invoice" class="form-control" required/>
								@if ($errors->has('invoice'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('invoice') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>




						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-block">Save</button>
							</div>
						</div>
					</fieldset>
					
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
