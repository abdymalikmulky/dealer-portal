@extends('admin.layouts.app')

@section('page_title', 'Edit Customer Info')
@section('page_desc', 'Edit ' . $title . ' pada sistem')

@section('content')

<div class="row">
    <div class="col-md-12">

		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Edit {{ $title }}</h3>--}}
			{{--</div>--}}
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellout.update', array('id' => $sellOut->id,'product_type' => app('request')->input('product_type'))) }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input name="_method" type="hidden" value="PATCH">


					<fieldset class="field-list-product">
						<legend>Product List {{ $title }}</legend>
                        <?php $inc = 1 ?>

						<!--edit-->
						@foreach($sellOutDetail as $key => $sellOutProduct)

							@if(Auth::user()->roles[0]->id < Config::get('constants.DEALER'))
							<p class="sellout-product-title">
								@if(Auth::user()->roles[0]->id == Config::get('constants.COS'))

								<a style="float:right;margin-right: 10px;" title="Hapus Product" href="{{ route('sellout.delete.product', array('index' => $key, 'id' => $sellOutProduct->id, 'sell_out_id' => $sellOutProduct->sell_out_id, 'product_type' => app('request')->input('product_type'))) }}" onclick="if(!confirm('Are you sure want to delete  this product?')){ return false; }"><i class="fa fa-trash" style="color:maroon"></i></a>
								Product  {{$inc++}}

								@endif
							</p>
							@endif
							<div>
								@if(strpos(app('request')->input('product_type'), "unit") !== false)
								<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
									<label for="sn" class="col-sm-3 control-label sellout-product-label">Serial Number </label>
									<div class="col-sm-9 sellout-product-value">
										:
										<b>
											{{ $sellOutProduct->sellThroughDetailProduct->serial_number }}
										</b>
									</div>
								</div>
								@endif

								<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
									<label for="product" class="col-sm-3 control-label sellout-product-label">Product Code </label>
									<div class="col-sm-9 sellout-product-value">
										:
										<b>{{ $sellOutProduct->product->product_code }}
											-
											{{ $sellOutProduct->product->type }}
										</b>
									</div>
								</div>

								<div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
									<label for="qty" class="col-sm-3 control-label sellout-product-label">Quantity  </label>
									<div class="col-sm-9 sellout-product-value">
										: <b>{{ $sellOutProduct->qty }}</b>
									</div>
								</div>
							</div>
							<hr/>
						@endforeach
						@if(Auth::user()->roles[0]->id == Config::get('constants.COS'))
						<a href="{{ route('sellout.create.addproduct', array('product_type' => app('request')->input('product_type'), 'sell_out_id' => $sellOutProduct->sell_out_id)) }}">
							<button type="button" class="btn btn-success form-control"><span class="fa fa-plus"></span>  Add</button>
						</a>


						@endif
						@if(count(Session::get('products')) <= 0 && count($sellOutDetail) <= 0)
							<p align="center"><i></i></p>
						@endif


					</fieldset>
					<fieldset>



						<legend>Customer Data </legend>
						<div class="form-group{{ $errors->has('transaction_date') ? ' has-error' : '' }}">
							<label for="transaction_date" class="col-sm-3 control-label">Transaction Date <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" value="{{ $sellOut->transaction_date }}" name="transaction_date" class="form-control date_input" id="transaction_date" placeholder="Transaction Date" required>
								@if ($errors->has('transaction_date'))
									<span class="help-block">
                                        <strong>{{ $errors->first('transaction_date') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('documents') ? ' has-error' : '' }}">
							<label for="documents" class="col-sm-3 control-label">Sell out document <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								@foreach($sellOut->sellOutDocuments as $document)
								<div class="box-document">
									
                                	<a href="{{ route('sellout.delete.document', $document->id) }}" onclick="if(!confirm('Are you sure want to delete Sellout Document {{ $document->filename }}')){ return false; }" class="delete"><i class="fa fa-trash"></i></a>

									<a href="{{ storage('sellout/' . $document->filename) }}">
										{{ $document->filename }}
									</a>
								</div>
								@endforeach
    							<input type="file" name="documents[]" multiple />
								@if ($errors->has('documents'))
									<span class="help-block">
                                        <strong>{{ $errors->first('documents') }}</strong>
                                    </span>
								@endif
							</div>
						</div>

						<input type="hidden" id="product_type" name="product_type" value="{{ app('request')->input('product_type') }}"/>

						<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
							<label for="company_name" class="col-sm-3 control-label">Customer Name<span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" value="{{ $sellOut->cust_company_name }}" name="company_name" class="form-control" id="company_name" placeholder="Customer Name" required>
								@if ($errors->has('company_name'))
									<span class="help-block">
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }}">
							<label for="company_address" class="col-sm-3 control-label">Address <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<textarea name="company_address" class="form-control" id="company_address" placeholder="Customer Address" required>{{ $sellOut->cust_company_address }}</textarea>
								@if ($errors->has('company_address'))
									<span class="help-block">
                                    <strong>{{ $errors->first('company_address') }}</strong>
                                </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<label for="phone" class="col-sm-3 control-label">Phone <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="phone" value="{{ $sellOut->cust_phone }}"class="form-control" id="phone" placeholder="Customer Phone Number" required>
								@if ($errors->has('phone'))
									<span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-3 control-label">Contact Person <span class="color-red">*</span> </label>
							<div class="col-sm-9">
								<input type="text" name="name" value="{{ $sellOut->cust_name }}"class="form-control" id="name" placeholder="Contact Person" required>
								@if ($errors->has('name'))
									<span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<input type="email" name="email" value="{{ $sellOut->cust_email }}" class="form-control" id="email" placeholder="Email">
								@if ($errors->has('email'))
									<span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
								@endif
							</div>
						</div>

						{{--PAKE UX yang Total Product--}}
						{{--<div class="form-group{{ $errors->has('total_product') ? ' has-error' : '' }}">--}}
						{{--<label for="total_product" class="col-sm-3 control-label">Total Product {{ $title }} <span class="color-red">*</span> </label>--}}
						{{--<div class="col-sm-9">--}}
						{{--<input type="number" name="total_product" class="form-control" id="total_product_sell_through" placeholder="Total Product" min="1" max="20" required>--}}
						{{--@if ($errors->has('total_product'))--}}
						{{--<span class="help-block">--}}
						{{--<strong>{{ $errors->first('total_product') }}</strong>--}}
						{{--</span>--}}
						{{--@endif--}}
						{{--</div>--}}
						{{--</div>--}}

					</fieldset>
					<hr/>
					<div class="form-group">
						<div class="col-sm-12">
							@if(Auth::user()->roles[0]->id == Config::get('constants.COS') || Auth::user()->roles[0]->id == Config::get('constants.DEALER'))
							<button type="submit" class="btn btn-primary btn-block">Save</button>
							@endif
						</div>
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				</form>
			</div>
		</div>
    </div>
</div>
@endsection
