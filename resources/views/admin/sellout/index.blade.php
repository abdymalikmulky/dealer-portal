@extends('admin.layouts.app')

@section('page_title', $title)
@section('page_desc', 'Data ' . $title)

@section('content')
	<div class="table-action-add">
		{{--<button type="button" class="btn btn-success right upload-via-excel" data-toggle="modal" data-target="#import_sellThrough_excel"><span class="fa fa-upload"> </span>  Import via Excel</button>--}}
		@if(Auth::user()->roles[0]->id == Config::get('constants.DEALER'))
		<a href="{{ route('sellout.create', array('product_type' => app('request')->input('product_type'), 'init_product' => true)) }}">
			<button type="button" class="btn btn-primary right"><span class="fa fa-plus"></span>  ADD</button>
		</a>
		@endif

	</div>
	<div class="row">
		@if(Session::has('error'))
			<div class="alert alert-danger alert-dismissable">
				<a href="" class="close">×</a>
				<strong>Terjadi Kesalahan!</strong> {{Session::get('error')}}
			</div>
		@endif
		<div class="col-md-12">
			<div class="box">
				<div  class="box-header">
					<h3 class="box-title"> Data {{ $title }}</h3> </em>
				</div>
				<div class="box-body">
					
					<div class="box">
						<div  class="box-header text-left">
							<h3 class="box-title"> Filter by Month & Year</h3>
						</div>
						<div class="box-body text-left">
							<form action="#">
								
								<div class="form-group col-md-6" >
									<label>Month : </label>
									<select name="month" id="filter-by-month" class="form-control">
										<option value="">All Month</option>
										@foreach($month as $monthData) 
										<option value="{{ substr($monthData->name, 0, 3) }}">{{ $monthData->name }}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group col-md-6" >
									<label>Year : </label>
									<select name="month" id="filter-by-year" class="form-control">
										<option value="">All Year</option>
										@foreach($years as $year) 
										<option value="{{ $year->year }}">{{ $year->year }}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group col-md-6" style="display: none">
									<label>Product Category : </label>
									<select name="category" id="filter-category" class="form-control">
										<option value="">- All Category - </option>
										@foreach($productCategories as $productCategory)
										<option value="{{ $productCategory->description }}">{{ strtoupper($productCategory->description) }}</option>
										@endforeach
									</select>
								</div>
							</form>
						</div>
					</div>

					<table id="table-data" class="table table-sorting table-striped table-hover datatable">
						<thead>
						<tr>
							<td>No</td>
							@if(Auth::user()->roles[0]->id < Config::get('constants.DEALER'))
							<th>Business Partner</th>
							@endif
							<th>Transaction Date</th>
							<th>Customer Name</th>
							@if(Auth::user()->roles[0]->id != Config::get('constants.PM'))
							<th>Contact Person</th>
							@endif
							<th>Phone</th>
							<th>Email</th>
							<th>Product</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>
						@foreach($sellOuts as $sellOut)
							<tr>
								<td></td>
								@if(Auth::user()->roles[0]->id < Config::get('constants.DEALER'))
								<td>{{ $sellOut->dealer->company_name }}</td>
								@endif
								<td>{{ date("d M Y", strtotime($sellOut->transaction_date)) }}</td>
								<td>{{ $sellOut->cust_company_name }}</td>
								@if(Auth::user()->roles[0]->id != Config::get('constants.PM'))
								<td>{{ $sellOut->cust_name }}</td>
								@endif
								<td>{{ $sellOut->cust_phone }}</td>
								<td>{{ $sellOut->cust_email }}</td>
								<td>

									<a href="{{ route('sellout.edit', array('id' => $sellOut->id, 'product_type' => app('request')->input('product_type'), 'init_product' => true)) }}">{{ $sellOut->sellOutQty($sellOut->id)->qty }}
										@if(strpos($productType, 'unit') !== false) Unit @else pcs @endif</a>
								</td>
								
								<td>
									<a href="{{ route('sellout.edit', array('id' => $sellOut->id, 'product_type' => app('request')->input('product_type'), 'init_product' => true)) }}" title="Edit" class="element-floating-left">
										<button class="btn btn-xs btn-warning">
											<span class="fa fa-edit"/> Edit
										</button>
									</a>

									@if(Auth::user()->roles[0]->id == Config::get('constants.DEALER'))
										@php
										
										//Customer Data
										$dateConvertion = date("d-m-Y", strtotime($sellOut->transaction_date));
										$bodyString = 
										"Customer Data\n---------------------------------\n Transaction Date : ".$dateConvertion." \n Customer Name : ".$sellOut->cust_company_name." \nAddress : ".$sellOut->cust_company_address." \n Phone : ".$sellOut->cust_phone." \n Contact Person : ".$sellOut->cust_name." \n Email : ".$sellOut->cust_email. "\n\n";

										//Unit data
										$bodyString .= 
										"List of Unit\n---------------------------------\n";
										foreach ($sellOut->sellOutDetail as $key => $selloutDetail){
											$serialNumber = $selloutDetail->sellThroughDetailProduct['serial_number'];
											$productCode = $selloutDetail->product->product_code . "-" . $selloutDetail->product->type;

											$bodyString .= ($key+1) . ". [SN : " . $serialNumber . "]  " .$productCode."\n";
										}
										


										$revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
	    								$bodyCustomer = strtr(rawurlencode($bodyString), $revert);
										@endphp

										@if(strpos($productType, 'unit') !== false) 
											<a href="mailto:ccc@astragraphia.co.id?Subject=Request Installation&body={{$bodyCustomer}}&cc={{Auth::user()->email}};{{$cos->email}};{{$bdm->email}}" title="Edit" class="element-floating-left">
												<button class="btn btn-xs btn-success">
														<span class="fa fa-folder"/> Request installation
												</button>
											</a>
										@endif

									@endif
									@if(Auth::user()->roles[0]->id < Config::get('constants.DEALER'))

									<form action="{{ route('sellout.delete', array('id' => $sellOut->id, 'product_type' => app('request')->input('product_type'))) }}" method="post" class="element-floating-left">
										{{csrf_field()}}
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-xs btn-danger" type="submit" onclick="if(!confirm('Are you sure want to delete  Sellout dengan id #{{ $sellOut->id }}')){ return false; }" title="Delete"><span class="fa fa-ban"/> Delete</button>
									</form>

									@endif

								</td>
								
							</tr>
						@endforeach
						</tbody>
							<tfoot>
				            <tr>
				                <th colspan="7" style="text-align:right">Total:</th>
				                
				            </tr>
				        </tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>

	<div class="modal fade" id="import_sellThrough_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" method="POST" sellThrough="form" action="{{ route('sellthrough.import') }}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" id="myModalLabel">Import Data {{ $title }}</h3>
					</div>
					<div class="modal-body">
						<div style="text-align: center">
							<img src="{{ storage("template/template-sellthrough.png") }}" />
							<h5 class="modal-download"><a href="{{ storage("document/template-sellthrough.xlsx") }}"><i class="fa fa-download"></i> Download Template</a></h5>
							<hr/>
							<input type="hidden" name="product_type" value="{{ app('request')->input('product_type') }}"/>
							<div class="form-group{{ $errors->has('dealer') ? ' has-error' : '' }}">
								<label for="role" class="col-sm-3 control-label">Business Partner</label>
								<div class="col-sm-9">
									<select id="dealer" name="dealer" class="form-control" style="width:100%" required>
										@foreach($dealers as $dealer)
											<option value="{{$dealer['id']}}">{{$dealer['company_name']}}</option>
										@endforeach
									</select>
									@if ($errors->has('dealer'))
										<span class="help-block">
                                        <strong>{{ $errors->first('dealer') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="modal-upload-box">
								<h4>Upload File Data {{ $title }}</h4>
								<br/>
								<input type="file" id="file" name="file" class="modal-upload-file">
								<p class="help-block"><em>Only  file : .xlxs</em></p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
						<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Import</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
