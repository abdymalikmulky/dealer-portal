@extends('admin.layouts.app')

@section('page_title', 'Add '. $title )
@section('page_desc', 'Add ' . $title )

@section('content')
<div class="row">

    <div class="col-md-12">
		<div class="box">
			{{--<div class="box-header">--}}
				{{--<h3 class="box-title"> Add {{ ucwords($category) }}</h3>--}}
			{{--</div>--}}


			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellout.store.product', array('product_type' => app('request')->input('product_type'), 'sell_out_id' => app('request')->input('sell_out_id'))) }}">
					{{ csrf_field() }}
					<fieldset>
						<div class="form-list-addproduct">
							@if(strpos(app('request')->input('product_type'), "unit") !== false)
							<div class="form-group{{ $errors->has('sn') ? ' has-error' : '' }}" >
								<label for="sn" class="col-sm-3 control-label">Serial Number <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<select id="sn" name="sn" class="form-control select-filter select-multi sellout-sn" style="width:100%" required>
										
										@foreach($dealerProductSerialNumbers as $key => $productSN)
											@php
												$arraySN = array();
												if(count(Session::get('products')) > 0) {
													$arraySN = array_column(Session::get('products'), 'sn');
												}
											@endphp
											@if(!in_array($productSN->serial_number, $arraySN)) {
												<option value="{{ $productSN->serial_number }}" product_id="{{ $productSN->product_id }}" product_id="{{ $productSN->product_id }}" product_code="{{ $productSN->product_code }}" product_type="{{ $productSN->type }}">SN : <b>{{ $productSN->serial_number }}</b> ( {{ $productSN->type }} - {{ $productSN->product_code }} )</option>
											@endif
										@endforeach
									</select>

									@if ($errors->has('sn'))
										<span class="help-block">
                                        <strong>{{ $errors->first('sn') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							@endif
							<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
								<label for="product" class="col-sm-3 control-label">Product Code <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									@if(strpos(app('request')->input('product_type'), "unit") === false)
									<select id="product_stock" name="product" class="form-control list-product-stock select-filter" style="width:100%" required>
										<option value="">Choose Product & Type</option>
										@foreach($productsStock as $key => $productStock)
                                            <?php
                                            	$currentQty = \App\Http\Controllers\admin\SellOutController::getQtyProductById($productStock->product_id);
                                            	$stockRemaining = ($productStock->qty - $productStock->sellout_qty) - $currentQty;
                                            if($stockRemaining > 0) {
											?>
											<option value="{{ $productStock->product_id }}" product_code="{{ $productStock->product_code }}" stock_remaining="{{ $stockRemaining }}">{{ $productStock->product_code }} - {{ $productStock->type }}</option>
											@php  } @endphp
										@endforeach
									</select>
									@else
									<label class="control-label product-by-sn">
										{{ $dealerProductSerialNumbers[0]->type }}
										- {{ $dealerProductSerialNumbers[0]->product_code }}
									</label>
									<input type="hidden" id="product_code" name="product" value="{{ $dealerProductSerialNumbers[0]->id }}"/>
									@endif
									@if ($errors->has('product'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<p id="desc_product">Remaining stock for {{ ucwords($category) }} <span id="desc_product_code">HASD123JF</span>
								is <span id="desc_product_remaining">0 {{ ucwords($category) }}</span></p>

							@if(strpos(app('request')->input('product_type'), "unit") === false)
							<div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}" >
								<label for="qty" class="col-sm-3 control-label">Quantity <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="number" min="1" name="qty" value="" class="form-control has-error input-qty" id="qty" placeholder="Quantity" required>

									@if ($errors->has('qty'))
										<span class="help-block">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							@else
							<input type="hidden" min="0" name="qty" value="1" class="form-control has-error" id="qty" placeholder="Quantity" required>
							@endif
						</div>
						<input type="hidden" id="product_type" name="product_type" value="{{ app('request')->input('product_type') }}"/>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" id="submit_add_product" @if(strpos(app('request')->input('product_type'), "unit") === false) disabled @endif class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				</form>
			</div>
		</div>
    </div>
</div>
@endsection

