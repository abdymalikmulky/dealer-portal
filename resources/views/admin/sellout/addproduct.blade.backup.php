@extends('admin.layouts.app')

@section('page_title', 'Add')
@section('page_desc', 'Add ' . $title )

@section('content')

<div class="row">
	{{--{{ dd(Session::get('products')) }}--}}
    <div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"> Add {{ ucwords($category) }}</h3>
			</div>
			<div class="box-body">
				<form class="form-horizontal" method="POST" product="form" action="{{ route('sellout.store.product', array('product_type' => app('request')->input('product_type'), 'sell_out_id' => app('request')->input('sell_out_id'))) }}">
					{{ csrf_field() }}
					<fieldset>
						<div class="form-list-addproduct">
							<div class="form-group{{ $errors->has('sn') ? ' has-error' : '' }}" >
								<label for="sn" class="col-sm-3 control-label">Serial Number <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="text" name="sn" class="form-control has-error" id="sn" placeholder="Masukkan Serial Number Product" required>

									@if ($errors->has('sn'))
										<span class="help-block">
                                        <strong>{{ $errors->first('sn') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
								<label for="product" class="col-sm-3 control-label">Product Code <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<select id="product_stock" name="product" class="form-control list-product select-filter" style="width:100%" required>
										<option value="">Choose Product & Type</option>
										@foreach($productsStock as $key => $productStock)
                                            <?php
                                            	$currentQty = \App\Http\Controllers\admin\SellOutController::getQtyProductById($productStock->product_id);
											?>
											<option value="{{ $productStock->product_id }}" product_code="{{ $productStock->product_code }}" stock_remaining="{{ ($productStock->qty - $productStock->sellout_qty) - $currentQty}}">{{ $productStock->product_code }} - {{ $productStock->type }}</option>
										@endforeach
									</select>
									@if ($errors->has('product'))
										<span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
							<p id="desc_product">Sisa stock anda untuk {{ ucwords($category) }} <span id="desc_product_code">HASD123JF</span>
								adalah <span id="desc_product_remaining">0 {{ ucwords($category) }}</span></p>

							<div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}" >
								<label for="qty" class="col-sm-3 control-label">Quantity <span class="color-red">*</span> </label>
								<div class="col-sm-9">
									<input type="number" min="0" name="qty" value="0" class="form-control has-error" id="qty" placeholder="Kuantitas" required>

									@if ($errors->has('qty'))
										<span class="help-block">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
									@endif
								</div>
							</div>
						</div>
						<input type="hidden" id="product_type" name="product_type" value="{{ app('request')->input('product_type') }}"/>
					</fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" id="submit_add_product" disabled class="btn btn-primary btn-block">Save</button>
						</div>
					</div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				</form>
			</div>
		</div>
    </div>
</div>
@endsection

