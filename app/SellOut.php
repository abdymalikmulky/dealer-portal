<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\SellOutDetail;

class SellOut extends Model
{
    protected $table = 'sell_out';
    protected $fillable = ['id', 'dealer_id', 'category', 'transaction_date', 'cust_company_name', 'cust_company_address', 'cust_phone', 'cust_name', 'cust_email', 'created_by'];

    public function sellOutDetail() {
        return $this->hasMany('App\SellOutDetail', 'sell_out_id');
    }

    public function sellOutDocuments() {
        return $this->hasMany('App\SellOutDocument', 'sell_out_id');
    }

    public function dealer() {
    	return $this->belongsTo('App\Dealer', 'dealer_id');
    }

    public function sellOutQty($sellOutId) {
        
        return SellOutDetail::select(DB::raw('SUM(qty) as qty'))
        ->where('sell_out_id', $sellOutId)
        ->groupBy('sell_out_detail.sell_out_id')
        ->first();
    }
}
