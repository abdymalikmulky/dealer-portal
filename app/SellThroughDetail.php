<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellThroughDetail extends Model
{
    protected $table = 'sell_through_detail';
    protected $fillable = ['id', 'dealer_id', 'sell_through_id', 'product_id', 'qty', 'revenue'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function sellThrough()
    {
        return $this->belongsTo('App\SellThrough', 'sell_through_id');
    }



}

?>
