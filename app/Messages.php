<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = [
        'sender_id', 'receiver_id', 'subject', 'message', 'read_status'
    ];
    public function sender() {
        return $this->belongsTo('App\User', 'sender_id');
    }
    public function receiver() {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    /*
     * receiverId = userId
     * */
    public function getUnreadMessage($receiverId) {
        $data = $this->where('receiver_id', $receiverId)->get();
        print_r($data);
        die();
    }



    function timeago($date) {
        $timestamp = strtotime($date);

        $strTime = array("second", "minute", "hour", "day", "month", "year");
        $length = array("60","60","24","30","12","10");

        $currentTime = time();
        if($currentTime >= $timestamp) {
            $diff     = time()- $timestamp;
            for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
                $diff = $diff / $length[$i];
            }

            $diff = round($diff);
            return $diff . " " . $strTime[$i] . "s ago ";
        }
    }
}
