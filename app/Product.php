<?php
/**
 * Created by PhpStorm.
 * User: abdymalikmulky
 * Date: 10/7/17
 * Time: 9:37 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Product extends Model
{
    protected $fillable = ['user_id', 'product_code', 'category', 'photo', 'type'];

    function dealerProductStock($category) {
        $sellThroughDetail = new SellThroughDetail;
        $sellThrough = new SellThrough;
        $product = new Product;
        $stTable = $sellThrough->getTable();
        $stdTable = $sellThroughDetail->getTable();
        $productTable = $product->getTable();
        $productTableId = 'product_id';


        $stockProducts = SellThroughDetail::select('product_id', 'product_code', 'type')
            ->selectRaw('(select sum(qty) from sell_out_detail where `product_id` = '.$stdTable.'.product_id group by product_id) as sellout_qty')
            ->selectRaw('sum(qty) as qty')
            ->join($stTable, $stdTable . '.' . $stTable . '_id', $stTable . '.' . 'id')
            ->join($productTable, $stdTable . '.' . $productTableId, $productTable . '.' . 'id')
            ->where($stTable . '.category', $category)
            ->where($stTable . '.dealer_id', Auth::user()->dealer->id)
            ->groupBy('product_id', 'product_code', 'type')->get();


        return $stockProducts;
    }
}