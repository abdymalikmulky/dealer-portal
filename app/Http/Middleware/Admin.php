<?php

namespace App\Http\Middleware;

use App\Dealer;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Config;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            
            if (Route::current()->getName() != "profile" and Route::current()->getName() != "user.update" and Route::current()->getName() != "notification.lists") {
                $isTheresEmptyValues = false;
                $emptyValuesAttribute = "";


                if (Auth::user()->roles[0]->id == Config::get('constants.DEALER')) {
                    $dealer = Dealer::where('user_id', Auth::user()->id)->first();
                    foreach ($dealer->toArray() as $key => $value) {
                        if($key != "company_websites" && $key != "PIC_jabatan" && $key != "PIC2_jabatan" && $key != "PIC2_name" && $key != "PIC2_email" && $key != "PIC2_phone") {
                            if ($value == "") {
                                $isTheresEmptyValues = true;
                                $emptyValuesAttribute .= $key . ", ";
                            }   
                        }
                    }
                    $emptyValuesAttribute = substr($emptyValuesAttribute, 0, -2). " ";
                } else {
                    $user = User::find(Auth::user()->id)->user_detail;
                    foreach ($user->toArray() as $key => $value) {
                        if ($key != "information") {
                            if ($value == "") {
                                $isTheresEmptyValues = true;
                                if ($key == "address") {
                                    $key = "area";
                                }
                                $emptyValuesAttribute .= ucfirst($key) . ", ";
                            }
                        }
                    }

                }


                if (Auth::user()->roles[0]->id == Config::get('constants.SUPERADMIN')) {
                    $isTheresEmptyValues = false;
                }
                if ($isTheresEmptyValues) {
                    $request->session()->flash('error', true);
                    $emptyValuesAttribute .= "WAJIB DIISI !";
                    $request->session()->flash('message', $emptyValuesAttribute);

                    return redirect()->route('profile');
                }
            }
            return $next($request);
        } else {
            $request->session()->flash('error', "Anda Harus Login Terlebih Dahulu");

            return redirect()->route('home');
        }

    }
}
