<?php
/**
 * Created by PhpStorm.
 * User: abdymalikmulky
 * Date: 10/8/17
 * Time: 5:41 PM
 */

namespace App\Http\ViewComposers;
use App\ProductCategories;
use App\Program;
use App\SellOut;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;


class ProductViewComposer
{
    public function compose(View $view) {
        //active programs/event

        $productCategories = ProductCategories::all();
        $view->with('productCategories', $productCategories);
    }
}