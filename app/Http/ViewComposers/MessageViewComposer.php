<?php
/**
 * Created by PhpStorm.
 * User: abdymalikmulky
 * Date: 10/8/17
 * Time: 5:41 PM
 */

namespace App\Http\ViewComposers;
use App\Messages;
use App\ProductCategories;
use App\Program;
use App\SellOut;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class MessageViewComposer
{
    public function compose(View $view) {
        //active programs/event
        if(Auth::check()) {
            $myMessages = Messages::where('receiver_id', Auth::user()->id)
                ->where('read_status', 0)
                ->orderBy('id', 'desc')->get();
            $view->with('myMessages', $myMessages);
        }
    }
}