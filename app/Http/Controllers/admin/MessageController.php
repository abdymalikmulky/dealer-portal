<?php

namespace App\Http\Controllers\admin;

use Config;
use App\UserDetail;
use App\Messages;
use App\TipsTrick;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'message';
    public $title = "Messages";
    public $messages;
    public $messageUnReadCount;
    public $messagesQuery;

    public function __construct()
    {


    }

    public function setData() {
        $messagesQuery = Messages::where('receiver_id', Auth::user()->id)->orderBy('id', 'desc');
        $this->messages = $messagesQuery->get();

        $messageUnread = $messagesQuery->where('read_status', 0)->get();
        $this->messageUnReadCount = count($messageUnread);
    }


    public function index()
    {
        $this->setData();
        $messages = $this->messages;
        return view('admin.message.index', compact('messages'))
            ->with('messageCount', $this->messageUnReadCount)
            ->with('title', $this->title);
    }

    public function sent()
    {
        $this->setData();
        $messagesQuery = Messages::where('sender_id', Auth::user()->id)->orderBy('id', 'desc');
        $messages = $messagesQuery->get();

        return view('admin.message.index', compact('messages'))
            ->with('sent', true)
            ->with('messageCount', $this->messageUnReadCount)
            ->with('title', $this->title);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isReplyMessage = false;

        $this->setData();
        $roleId = Auth::user()->roles[0]->id;
        $userId = Auth::user()->id;

        $messageBefore = null;
        $idMessageBefore = request()->get('message_before');
        if(request()->has('message_before')) {
            $messageBefore = Messages::find($idMessageBefore);
            $isReplyMessage = true;             
        }

        if($roleId==Config::get('constants.DEALER')) {
            $userCreatorId = Auth::user()->dealer->created_by;
        } else {
            $userCreatorId = Auth::user()->user_detail->created_by;
        }
        
        if($roleId==Config::get('constants.PM'))  {
            $receivers = User::select('users.id as user_id', 'users.name as user_name', 'roles.name as role_name')
                ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'user_roles.role_id')
                ->where('users.id', '!=', $userCreatorId)
                ->get();

        } else if($roleId==Config::get('constants.DEALER')) {
            $receivers = User::select('users.id as user_id', 'users.name as user_name', 'roles.name as role_name')
                ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'user_roles.role_id')
                ->where('users.id', $userCreatorId)
                ->get();
        } else  { 
            $receiverParent = User::select('users.id as user_id', 'users.name as user_name', 'roles.name as role_name')
                ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'user_roles.role_id')
                ->where('users.id', $userCreatorId);

            $receiverJoin = User::select('users.id as user_id', 'users.name as user_name', 'roles.name as role_name')
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id');
            
            if($roleId==Config::get('constants.BDM')) {
                $receiverJoin = $receiverJoin->join('dealers', 'dealers.user_id', '=', 'users.id')
                ->where('dealers.created_by', '=', $userId);
            } else {
                $receiverJoin = $receiverJoin->join('user_details', 'user_details.user_id', '=', 'users.id')
                ->where('user_details.created_by', '=', $userId);
            }
            $receivers = $receiverJoin
            ->orderBy('users.id', 'asc')
            ->union($receiverParent)
            ->get();
        }
        
        return view('admin.message.compose')
            ->with('messageCount', $this->messageUnReadCount)
            ->with('receivers', $receivers)
            ->with('messageBefore', $messageBefore)
            ->with('title', $this->title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'receiver' => 'required',
            'subject' => 'required|string',
            'message' => 'required|string'
        ]);

        $receivers = $request->get('receiver');
        $subject = $request->get('subject');
        $message = $request->get('message');

        foreach ($receivers as $receiver) {
            $dataMessage = [
                'sender_id' => Auth::user()->id,
                'receiver_id' => $receiver,
                'subject' => $subject,
                'message' => $message,
                'read_status' => 0
            ];
            $createdMessage = Messages::create($dataMessage);

            $userReceiver = User::find($receiver);
            $emailData = [
                'email' => $userReceiver->email,
                'subject' => $subject,
                'bodyMessage' => $createdMessage
            ];

            //Turn off email
            /*$sendMail = Mail::send(['html' => 'admin.message.resultmail'], $emailData, function($message) use ($emailData){
                $message->from('abdymmmi@gmail.com', 'Dealer Portal');
                $message->to($emailData['email']);
                $message->subject($emailData['subject']);
            });*/

        }
        return redirect($this->redirectUrl);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->setData();
        $message = Messages::find($id);
        $message->read_status = 1;
        $message->save();
        return view('admin.message.show', compact('message'))
            ->with('messageCount', $this->messageUnReadCount)
            ->with('title', $this->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
