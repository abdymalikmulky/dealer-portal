<?php

namespace App\Http\Controllers\admin;

use App\Product;
use App\SellThrough;
use App\SellThroughDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StockOfProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'stockproduct';
    public $title = "Stock ";
    public $productType = "unit";

    public function __construct()
    {
        $this->productType = app('request')->input('product_type');
        if ($this->productType == "unitmono") {
            $this->title .= "Unit Mono";
        } else if ($this->productType == "unitcolor") {
            $this->title .= "Unit Color";
        } else {
            $this->title .= ucwords($this->productType);
        }
    }

    public function index()
    {
        $product = new Product;

        $stockProducts = $product->dealerProductStock($this->productType);

        return view('admin.stockproduct.index', compact('stockProducts'))
            ->with('title', $this->title)
            ->with('productType', $this->productType);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
