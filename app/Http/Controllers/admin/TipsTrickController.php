<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\UploadPhotoRequest;
use App\TipsTrick;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class TipsTrickController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'tipstrick';
    public $title = "Tips & Trick";
    public function index()
    {
        $tipsTricks = TipsTrick::orderBy('id', 'desc')->get();
        return view('admin.tipstrick.index', compact('tipsTricks'))->with('title', $this->title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tipstrick.create')->with('title', $this->title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UploadPhotoRequest $uploadPhotoRequest)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string'
        ]);

        $photo = "static_image/default.png";
        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('tipsandtrick');
            $photo = $photoFileName;
        }


        //Create
        $create = TipsTrick::create([
            'created_by' => Auth::user()->id,
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'picture' => $photo
        ]);

        return redirect($this->redirectUrl);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipsTrick = TipsTrick::find($id);
        return view('admin.tipstrick.show', compact('tipsTrick'))->with('title', $this->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipsTrick = TipsTrick::find($id);
        return view('admin.tipstrick.edit', compact('tipsTrick'))->with('title', $this->title);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, UploadPhotoRequest $uploadPhotoRequest)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string'
        ]);

        $tipsTrick = TipsTrick::find($id);

        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('tipstrick');
            $tipsTrick->picture = $photoFileName;
        }

        $tipsTrick->title = $request->get('title');
        $tipsTrick->content = $request->get('content');
        $tipsTrick->save();


        return redirect($this->redirectUrl);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipsTrick = TipsTrick::find($id);
        $tipsTrick->delete();
        return redirect($this->redirectUrl);
    }
}
