<?php

namespace App\Http\Controllers\admin;

use App\Dealer;
use App\Regencies;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadExcelRequest;
use App\Http\Requests\UploadPhotoRequest;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\UserRoles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'user';

    protected $userRole = 0; //user role active
    protected $userRoleFor = 0; //manage user for COS, BDM, Dealer or AllUser(user)
    protected $manageUserFor = "User"; //manage user for COS, BDM, Dealer or AllUser(user)
    protected $isSuperAdmin = false;
    protected $isForDealer = false;
    protected $isMyProfile = false;
    protected $isDealer = false;


    public function __construct()
    {

        $this->middleware(function ($request, $next) {
            
            $this->userRole = Auth::user()->roles[0]->id;


            if($this->userRole == Config::get('constants.SUPERADMIN')) {
                $this->isSuperAdmin = true;
            }

            if($this->userRole == Config::get('constants.PM')) {
                $this->manageUserFor = "COS";
                $this->userRoleFor = Config::get('constants.COS');
            } else if($this->userRole == Config::get('constants.COS')) {
                $this->manageUserFor = "BDM";
                $this->userRoleFor = Config::get('constants.BDM');
            } else if($this->userRole == Config::get('constants.BDM')) {
                $this->manageUserFor = "Dealer";
                $this->userRoleFor = Config::get('constants.DEALER');
                $this->isForDealer = true;
            } else {
                $this->isDealer = true;
            }

            return $next($request);
        });

    }

    public function index()
    {
        if($this->isSuperAdmin) {
            $users = User::all();
        } else {
            $userDetailTable = "user_details";

            //kalo dealer joinnnya ke table dealer, kalo slain itu ke user_deatil
            if($this->isForDealer) {
                $userDetailTable = "dealers";
            }

            $userSelectAndJoin = User::select('users.id as id', 'username', 'users.name as name', 'email', 'users.status_id')
                ->join('user_roles', 'users.id', 'user_roles.user_id')
                ->join($userDetailTable, 'users.id', $userDetailTable.'.user_id');

            $usersCondition = $userSelectAndJoin->where('user_roles.role_id', Config::get('constants.'.strtoupper($this->manageUserFor)));

            //cek kalo cos kebawah harus sesuai  yang buatnya
            if($this->userRole != Config::get('constants.PM')) {
                $usersCondition = $usersCondition->where($userDetailTable.'.created_by', Auth::user()->id);
            }

            $users = $usersCondition->with('roles')->get();

        }

        //Dealer TITLE
        if($this->manageUserFor == "Dealer") {
            $this->manageUserFor = "Business Partner";
        }

        return view('admin.user.index', compact('users'))
            ->with('title', "Channel Management " . $this->manageUserFor)
            ->with('isSuperAdmin', $this->isSuperAdmin)
            ->with('isForDealer', $this->isForDealer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Dealer TITLE
        if($this->manageUserFor == "Dealer") {
            $this->manageUserFor = "Business Partner";
        }

        $regencies = Regencies::all();
        $roles = Role::all()->toArray();
        return view('admin.user.create', compact('roles','regencies'))
            ->with('title', $this->manageUserFor)
            ->with('isSuperAdmin', $this->isSuperAdmin)
            ->with('roleFor', $this->userRoleFor)
            ->with('isForDealer', $this->isForDealer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UploadPhotoRequest $uploadPhotoRequest)
    {

        $formValidate = $this->validateForm();

        $request->validate($formValidate);

        $roles = $request->get('role');

        //Create User
        $createUser = User::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'name' => $request->get('name'),
            'password' => bcrypt($request->get('password')),
            'status_id' => 1 //ACTIVE
        ]);
        $createdUserId = $createUser->id;



        $photoFileName = "photo/default.png";
        
        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('photo');
        }

        if($this->isForDealer) {
            $dataWebsite = "";
            foreach ($request->get('company_websites') as $key => $value) {
                $dataWebsite .= $value.";";
            }
            $dataWebsite = substr($dataWebsite, 0, -1);
            $userDetailData = array(
                'company_name' => $request->get('name'),
                'company_city' => $request->get('regency'),
                'company_address' => $request->get('company_address'),
                'company_phone' => $request->get('company_phone'),
                'company_type' => $request->get('company_type'),
                'company_websites' => $dataWebsite,
                'PIC_name' => $request->get('PIC_name'),
                'PIC_email' => $request->get('email'),
                'PIC_phone' => $request->get('PIC_phone'),
                'PIC_jabatan' => $request->get('PIC_jabatan'),
                'PIC2_name' => $request->get('PIC_name2'),
                'PIC2_email' => $request->get('email2'),
                'PIC2_phone' => $request->get('PIC_phone2'),
                'PIC2_jabatan' => $request->get('PIC_jabatan2')
            );
            $userDetailData['company_photo'] = $photoFileName;

        } else {
            $userDetailData = array(
                'NIP' => $request->get('nip'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'information' => $request->get('information'),
            );
            $userDetailData['photo'] = $photoFileName;
        }
        
        //Create/insert user detail
        $this->createUserDetail($createdUserId, $userDetailData);


        //Create User Roles
        $this->createUserRoles($createdUserId, $roles);

    
        return redirect($this->redirectUrl);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "Profile";
        echo $id;
    }

    public function profile() {
        $isChangePasswordSuccess = false;
        $oldPasswordDiff = false;
        if(!empty(app('request')->input('change_password'))) {
            $isChangePasswordSuccess = true;
        }
        if(!empty(app('request')->input('oldpassword_diff'))) {
            $oldPasswordDiff = true;
        }

        $regencies = Regencies::all();
        $user = Auth::user();
        return view('admin.user.profile', compact('user', 'regencies'))
            ->with('isSuperAdmin', $this->isSuperAdmin)
            ->with('isDealer', $this->isDealer)
            ->with('isChangePasswordSuccess', $isChangePasswordSuccess)
            ->with('isOldPasswordDiff', $oldPasswordDiff);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $user = User::find($id);
        $regencies = Regencies::all();

        if($this->manageUserFor == "Dealer") {
            $this->manageUserFor = "Business Partner";
        }

        return view('admin.user.edit', compact('roles','user','id','regencies'))
            ->with('title', $this->manageUserFor)
            ->with('isSuperAdmin', $this->isSuperAdmin)
            ->with('roleFor', $this->userRoleFor)
            ->with('isForDealer', $this->isForDealer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $userId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userId, UploadPhotoRequest $uploadPhotoRequest)
    {
        $isUpdateDataDealer = false;

        //cek update profile sendrii atau user lain
        if($userId == Auth::user()->id){
            $this->isMyProfile = true;
        }
        //whos gonna be updated, dealer or user detail
        // kalau ini bukan profile saya, dan saya adalah BDM (isForDealer=true) makan update untuk dealer atau kalau ini profile saya dan saya adalah dealer makan update untuk dealer juga true
        if((!$this->isMyProfile && $this->isForDealer) || $this->isDealer) {
            $isUpdateDataDealer = true;
        }


        //TODO: buatin validate khusus

        $roles = $request->get('role');

        //Update User
        $user = User::find($userId);
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->save();


        //TODO: ini harusnya ganti langsung update aja
        if(!$this->isMyProfile) {
            $this->deleteRole($userId);
        }

        if($isUpdateDataDealer) {
            $userDetailData = $this->getDataDealer($request);
        } else {
            $userDetailData = $this->getDataUser($request);
        }

        //upload photo
        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('photo');
            if($isUpdateDataDealer) {
                $userDetailData['company_photo'] = $photoFileName;
            } else {
                $userDetailData['photo'] = $photoFileName;
            }
        }
        //update user detail
        $this->updateUserDetail($userId, $userDetailData, $isUpdateDataDealer);

        //Create User Roles
        if(!$this->isMyProfile) { //jika ini profile saya sendiri saya ngga bisa ganti roles
            $this->createUserRoles($userId, $roles);
        }


        $redirectUrl = $this->redirectUrl;
        if($this->isMyProfile) {
            $redirectUrl = ADMIN_PREFIX_URL . 'profile';
        }


        return redirect($redirectUrl);
    }

    function updatePassword(Request $request, $userId) {
        $error = false;
        $formValidate = array(
            'old-password' => 'required|string|min:6',
            'password' => 'required|string|min:6|confirmed'
        );
        $validator = Validator::make($request->all(), $formValidate);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            $user = User::find($userId);

            $oldPassword = $request->get('old-password');
            $newPassword = $request->get('password');

            $newPasswordBcrypted = bcrypt($newPassword);


            if (Hash::check($oldPassword, $user->password)) {
                $user->password = $newPasswordBcrypted;
                $user->save();

                $message = "Password anda berhasil diubah.";
                $error = false;
            } else {
                $message = "Password lama anda tidak sesuai dengan password sebelumnya.";
                $error = true;
            }

        }


        $request->session()->flash('error', $error);
        $request->session()->flash('message', $message);

        return redirect(ADMIN_PREFIX_URL . 'profile');
    }


    private function getDataDealer($request) {
        $dataWebsite = "";
        foreach ($request->get('company_websites') as $key => $value) {
            $dataWebsite .= $value.";";
        }
        $dataWebsite = substr($dataWebsite, 0, -1);

        return array(
            'company_name' => $request->get('name'),
            'company_city' => $request->get('regency'),
            'company_address' => $request->get('company_address'),
            'company_phone' => $request->get('company_phone'),
            'company_type' => $request->get('company_type'),
            'company_websites' => $dataWebsite,
            'PIC_name' => $request->get('PIC_name'),
            'PIC_email' => $request->get('email'),
            'PIC_phone' => $request->get('PIC_phone'),
            'PIC_jabatan' => $request->get('PIC_jabatan'),
            'PIC2_name' => $request->get('PIC_name2'),
            'PIC2_email' => $request->get('email2'),
            'PIC2_phone' => $request->get('PIC_phone2'),
            'PIC2_jabatan' => $request->get('PIC_jabatan2')
        );
    }

    private function getDataUser($request) {
        $dataUser = array();
        $nip = $request->get('nip');
        if(!empty($nip)) {
            $dataUser['NIP'] = $nip;
        }
        $dataUser['phone'] = $request->get('phone');
        $dataUser['address'] = $request->get('address');
        $dataUser['information'] = $request->get('information');

        return $dataUser;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->deleteRole($id);
        $this->deleteUserDetail($id);
        $user = User::find($id);
        $user->delete();

        return redirect($this->redirectUrl);
    }

    private function createUserRoles($userId, $roles) {

        if(is_array($roles)) {
            foreach ($roles as $roleId) {
                UserRoles::create([
                    'user_id' => $userId,
                    'role_id' => $roleId
                ]);
            }
        } else {
            $roleId = $roles;
            UserRoles::create([
                'user_id' => $userId,
                'role_id' => $roleId
            ]);
        }

    }

    private function createUserDetail($userId, $userDetailData) {
        //Create User detail
        $userDetailData['user_id'] = $userId;
        $userDetailData['created_by'] = Auth::user()->id;
        if($this->isForDealer) {
            Dealer::create($userDetailData);
        } else {
            UserDetail::create($userDetailData);
        }
    }

    private function updateUserDetail($userId, $userDetailData, $isUpdateDataDealer) {
        //Update User detail
        if($isUpdateDataDealer) {
            $dealer = Dealer::where('user_id', $userId)
            ->update($userDetailData);
        } else {

            $userdetail = UserDetail::where('user_id', $userId)
            ->update($userDetailData);
        }
    }

    private function validateForm() {

        $formValidate = array(
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'nip' => 'required|numeric',
            'phone' => 'required|numeric',
            'photo' => 'image|mimes:jpeg,bmp,png',
            'address' => 'required|string|max:255',
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:6|confirmed',
        );
        if($this->isForDealer) {
            $formValidate = array(
                'username' => 'required|string|max:12|unique:users',
                'name' => 'required|string|max:255',
                'company_address' => 'required|string|max:255',
                'company_phone' => 'required|numeric',
                'PIC_name' => 'required|string|max:100',
                'email' => 'required|email|max:50|unique:users', //actually this is PIC email
                'PIC_phone' => 'required|numeric',
                'password' => 'required|string|min:6|confirmed',
            );
        }

        return $formValidate;
    }

    /**
    GET user by parent
    $parentUserParentId : COS
    $userParentId : COS/BDM
    **/
    public function getUserByParent($userParentId, $roleId) {
        $parentUserParentId = Input::get('parent_user_parent_id');
        
        if($roleId == Config::get('constants.COS')) { //For BDM data
            if($userParentId=="all") {
                $userData = UserDetail::join('users', 'users.id', 'user_details.user_id')
                ->join('user_roles', 'user_roles.user_id', 'users.id')
                ->where('user_roles.role_id', Config::get('constants.BDM'))->get();
            } else {
                $userData = UserDetail::join('users', 'users.id', 'user_details.user_id')
                ->where('created_by', $userParentId)->get();
            }
        } else if($roleId == Config::get('constants.BDM')) { //For Dealer data
            if($userParentId=="all") {
                if($parentUserParentId=="all") {
                    $userData = Dealer::join('users', 'users.id', 'dealers.user_id')->get();
                } else {
                    if(empty($parentUserParentId)) { //login as COS
                        $parentUserParentId = Auth::user()->id;
                    }
                    $userData = Dealer::join('users', 'users.id', 'dealers.user_id')
                    ->whereIn('created_by', function($query) use ($parentUserParentId) {
                        $query->select('users.id')
                        ->from('users')
                        ->join('user_details', 'user_details.user_id', 'users.id')
                        ->where('user_details.created_by', $parentUserParentId);
                    })->get();
                }
            } else {
                $userData = Dealer::join('users', 'users.id', 'dealers.user_id')
                ->where('created_by', $userParentId)->get();
            }

        } 

        return response()->json($userData);   
    }

    private function deleteUserDetail($userId) {

        if(!$this->isForDealer) {
            $userDetail = UserDetail::where('user_id', $userId);
            $userDetail->delete();
        } else {
            $userDealer = Dealer::where('user_id', $userId);
            $userDealer->delete();
        }
        
    }

    private function deleteRole($userId) {
        $userRole = UserRoles::where('user_id', $userId);
        $userRole->delete();
    }


}
