<?php

namespace App\Http\Controllers\admin;

use Config;
use App\User;
use App\SellThrough;
use App\SellThroughDetail;
use App\SellThroughDetailProduct;
use App\Achievement;
use App\AchievementDetail;
use App\Dealer;
use App\Month;
use App\UserDetail;
use App\Notifications\ActionInSystem;
use App\ProductCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AchievementController extends Controller
{
    public $redirectUrl = ADMIN_PREFIX_URL . 'achievement';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $roleId = Auth::user()->roles[0]->id;
        $dealers = Dealer::all();
        if($roleId==Config::get('constants.PM')) {
            $achievements = Achievement::all();
        } else {
            $achievements = Achievement::where('created_by', Auth::user()->id)->get();
        }
        
        return view('admin.achievement.index', compact('dealers', 'achievements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dealers = Dealer::where('created_by', Auth::user()->id)->get();
        $months = Month::orderBy('id')->get();
        $monthArray = $months->toArray();
        array_unshift($monthArray, array('name' => 'month_dummy'));
        $months = collect($monthArray);
        
        $productCategories = ProductCategories::all();
        return view('admin.achievement.create', compact('dealers', 'months', 'productCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $months = Month::all();
        $productCategories = ProductCategories::all();

        $achievementFor = "DEALER";
        $userId = $request->get('dealer');

        $createdBy = Auth::user()->id;
        $year = $request->get('year');

        $achievementCreate = Achievement::create([
            'achievement_for' => $achievementFor,
            'user_id' => $userId,
            'year' => $year,
            'created_by' => $createdBy
        ]);


        $achievementId = $achievementCreate->id;

        $index = 0;
        foreach ($months as $month) {
            $monthId = $month->id;
            $dataAchievement[] = array(
                'achievement_id' => $achievementId,
                'month_id' => $monthId
            );
            foreach ($productCategories as $productCategory) {
                $fieldTargetRevenue = 'target_'.$productCategory->category.'_revenue';
                $fieldTargetCount = 'target_'.$productCategory->category.'_count';

                $dataAchievement[$index][$fieldTargetRevenue] = $request->get($fieldTargetRevenue . '_' . $monthId);
                $dataAchievement[$index][$fieldTargetCount] = $request->get($fieldTargetCount . '_' . $monthId);
            }
            $index++;
        }


        $insertAchievementDetail = $this->insertAchievementDetail($dataAchievement);
        if($insertAchievementDetail) {

            return redirect($this->redirectUrl);
        } else {
            return redirect($this->redirectUrl . '?error=1');
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roleId = Auth::user()->roles[0]->id;
        if($roleId == Config::get('constants.PM')) { 
            $dealers = Dealer::all();
        } else {
            $dealers = Dealer::where('created_by', Auth::user()->id)->get();
        }

        $months = Month::orderBy('id')->get();
        $monthArray = $months->toArray();
        array_unshift($monthArray, array('name' => 'month_dummy'));
        $months = collect($monthArray);

        $productCategories = ProductCategories::all();
        $achievement = Achievement::find($id);
        $achievementDetail = AchievementDetail::where('achievement_id', $id)->get();
        $achievementDetail = $achievementDetail->toArray();


        return view('admin.achievement.edit', compact('dealers','months', 'productCategories', 'achievement', 'achievementDetail', 'roleId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $roleId = Auth::user()->roles[0]->id;
        $months = Month::all();
        $productCategories = ProductCategories::all();

        $achievementFor = "DEALER";
        $userId = $request->get('dealer');


        $createdBy = Auth::user()->id;
        $year = $request->get('year');

        $achievement = Achievement::find($id);
        $achievement->achievement_for = $achievementFor;
        $achievement->user_id = $userId;
        $achievement->year = $year;
        $achievement->created_by = $createdBy;
        $achievement->save();


        $index = 0;
        foreach ($months as $month) {
            $monthId = $month->id;
            $dataAchievement[] = array(
                'achievement_id' => $id,
                'month_id' => $monthId
            );
            foreach ($productCategories as $productCategory) {
                $fieldTargetRevenue = 'target_'.$productCategory->category.'_revenue';
                $fieldTargetCount = 'target_'.$productCategory->category.'_count';

                $dataAchievement[$index][$fieldTargetRevenue] = $request->get($fieldTargetRevenue . '_' . $monthId);
                $dataAchievement[$index][$fieldTargetCount] = $request->get($fieldTargetCount . '_' . $monthId);
            }
            $index++;
            
            $updateAchievementDetail = $this->updateAchievementDetail($dataAchievement[$index-1], $id, $monthId);
        }


        //send notification
        if($roleId != Config::get('constants.PM')) { 
            $userPM = User::join('user_roles', 'users.id', '=', 'user_roles.user_id')->where('role_id', Config::get('constants.PM'))->first();
            $actionInSystemNotif = new ActionInSystem();
            $actionInSystemNotif->setAchievement($achievement);
            $userPM->notify($actionInSystemNotif);
        }
        return redirect($this->redirectUrl);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $achievementDetail = AchievementDetail::where('achievement_id', $id);
        $achievementDetail->delete();
        $achievement = Achievement::find($id);
        $achievement->delete();

        return redirect($this->redirectUrl);
    }


    //REPORT
    public function report() {
        //Master data
        $productCategories = ProductCategories::all();
        $months = Month::all();
        $years = Achievement::select('year')->groupBy('year')->orderBy('year', 'asc')->get();

        //User Data Hierarchy
        $cosId = 0;
        $coses = null;
        $bdmId = 0;
        $bdms = null;
        $dealerId = 0;
        $dealers = null;
        $roleId = Auth::user()->roles[0]->id;
        

        //Number of Colom and user data By RoleId
        $bootstrapColSize = 6;
        if($roleId == Config::get('constants.PM')) { 
            $bootstrapColSize = 3;
            $coses = UserDetail::where('created_by', Auth::user()->id)->get();
        } else if($roleId == Config::get('constants.COS')) { 
            $bootstrapColSize = 4;
            $bdms = UserDetail::where('created_by', Auth::user()->id)->get();
        } else if($roleId == Config::get('constants.BDM')) { 
            $dealers = Dealer::where('created_by', Auth::user()->id)->get();
        }

        $dataAchievementReport = null;

        //User Current data by Query parameter (harusnya ini bisa pake json di usercontroller)
        if(request()->has('cos')) {
            if(request()->get('cos')=="all") {
                $bdms = UserDetail::join('users', 'users.id', '=', 'user_details.user_id')
                ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
                ->where('role_id', Config::get('constants.BDM'))->get();
            } else {
                $bdms = UserDetail::where('created_by', request()->get('cos'))->get();
            }
        }
        if(request()->has('bdm')) {
            if(request()->get('bdm')=="all") {
                $parentUserParentId = request()->get('cos');

                $dealers = Dealer::join('users', 'users.id', '=', 'dealers.user_id');
                if(request()->get('cos')!="all") {
                    $dealers = $dealers->whereIn('created_by', function($query) use ($parentUserParentId) {
                            $query->select('users.id')
                            ->from('users')
                            ->join('user_details', 'user_details.user_id', 'users.id')
                            ->where('user_details.created_by', $parentUserParentId);
                        });
                }
                $dealers = $dealers->get();
            } else {
                $dealers = Dealer::where('created_by', request()->get('bdm'))->get();
            }
        }
        if(request()->has('cos') || request()->has('bdm') || request()->has('dealer') || ($roleId == Config::get('constants.DEALER') && request()->has('target'))) {
            if(request()->has('dealer')) {
                $dealerId = request()->dealer;    
            } 
            if($roleId == Config::get('constants.DEALER')) {
                $dealerId = Auth::user()->id;    
            }

            if(request()->has('bdm')) {
                $bdmId = request()->bdm;
            }
            if(request()->has('cos')) {
                $cosId = request()->cos;
            }
            $dataUser['dealer_id'] = $dealerId;
            $dataUser['bdm_id'] = $bdmId;
            $dataUser['cos_id'] = $cosId;
            // print_r($dataUser);
            // die();

            $tren = request()->tren;
            $year = request()->year;
            $month = request()->month;
            
            $dataAchievementReport = $this->getAchievementReport('array', $year, $month, $tren, $dataUser);
        }

        return view('admin.achievement.report', compact('roleId', 'coses', 'bdms', 'dealers', 'dataAchievementReport', 'productCategories', 'months', 'years', 'bootstrapColSize'));
    }

    public function getReportTable() {
        $target = request()->target;
        $dataAchievementJson = request()->data;
        $dataAchievement = json_decode($dataAchievementJson, true);
        return view('admin.achievement.report_table', compact('dataAchievement', 'target'));
    }

    public function getAchievementReport($responseType='json', $year, $month, $tren, $dataUser)
    {
        $response = array();
        $reportData = array();
        $sellThrough = new SellThrough();
        $achievement = new Achievement();

        $currentYear = $year;
        $lastYear = $currentYear-1;
        $isMonthToDate = true;
        if($tren=="ytd") {
            $isMonthToDate = false;
        }

        $productCategories = ProductCategories::where('category','!=','optional')->get();
        foreach ($productCategories as $key => $value) {
            $category = $value->category;
            $reportData['category'] = $category;

            //set parameter variable
            $achievement->setParameterTarget($currentYear, $month, $isMonthToDate, $category, $dataUser);

            $planAndActualData = $achievement->getTargetPlanAndActual();

            //Plan
            $plan = $planAndActualData->target_plan;
            // print_r($planAndActualData);

            //Revenue variable
            $actualRevenueData = $planAndActualData->revenue_actual;
            $actualRevenue = $actualRevenueData['current_year']->revenue;
            $lastYearActualRevenue = $actualRevenueData['last_year']->revenue;
            $planRevenue = $plan->revenue;

             /*Revenue Formula*/
            $grArRevenueData = $this->processGrArRevenue($actualRevenue, $planRevenue, $lastYearActualRevenue);
            

            //Number Of Item variable
            $actualNumberOfItemData = $planAndActualData->number_of_item_actual;
            $actualNumberOfItem = $actualNumberOfItemData['current_year']->number_of_item;
            $lastYearActualNumberOfItem = $actualNumberOfItemData['last_year']->number_of_item;
            $planNumberOfItem = $plan->number_of_item;
            
            /*NumBer of Item Formula*/
            $grArNumberOfItemData = $this->processGrArNumberOfItem($actualNumberOfItem, $planNumberOfItem, $lastYearActualNumberOfItem);


            $reportData['actual_revenue'] = $actualRevenue;
            $reportData['actual_revenue_last_year'] = $lastYearActualRevenue;
            $reportData['plan_revenue'] = $planRevenue;

            $reportData['actual_number_of_item'] = $actualNumberOfItem;
            $reportData['actual_number_of_item_last_year'] = $lastYearActualNumberOfItem;
            $reportData['plan_number_of_item'] = $planNumberOfItem;
            
            foreach ($grArRevenueData as $keyData => $valueData) {
                $reportData[$keyData] = $valueData;
            }
           
            foreach ($grArNumberOfItemData as $keyData => $valueData) {
                $reportData[$keyData] = $valueData;
            }
            
            $response[] = $reportData;
        }



        //UNIT MONO COLOR
        $indexMono = 0;
        $indexColor = 1;
        foreach ($response[$indexMono] as $key => $value) {
            $dataMono = $response[$indexMono][$key];
            $dataColor = $response[$indexColor][$key];
            $reportData[$key] = "unitmonocolor";
            if($key != "category") {
                $reportData[$key] = $response[$indexMono][$key] + $response[$indexColor][$key];
            }
            if($key=="gr_plan_revenue") {
                $reportData[$key] = 0;
            }
        }

        /*Revenue*/
        $actualRevenueUnit = $reportData['actual_revenue'];
        $planRevenueUnit = $reportData['plan_revenue'];
        $lastYearActualRevenueUnit = $reportData['actual_revenue_last_year'];
        
        $grArRevenueData = $this->processGrArRevenue($actualRevenueUnit, $planRevenueUnit, $lastYearActualRevenueUnit);

        /*Number of Item*/
        $actualNumberOfItemUnit = $reportData['actual_number_of_item'];
        $planNumberOfItemUnit = $reportData['plan_number_of_item'];
        $lastYearActualNumberOfItemUnit = $reportData['actual_number_of_item_last_year'];

        $grArNumberOfItemData = $this->processGrArNumberOfItem($actualNumberOfItemUnit, $planNumberOfItemUnit, $lastYearActualNumberOfItemUnit);


        //Process Data 
        foreach ($reportData as $keyData => $valueData) {
            if(isset($grArRevenueData[$keyData])){
                $reportData[$keyData] = $grArRevenueData[$keyData];
            }
            if(isset($grArNumberOfItemData[$keyData])){
                $reportData[$keyData] = $grArNumberOfItemData[$keyData];
            }
        }
        
        $response[] = $reportData;


        //Ordering
        //Unitmonocolor, mono, color, cons
        $orderedKey = array(3,0,1,2);
        $orderedResponse = array_replace(array_flip($orderedKey), $response);

        if($responseType=="json") {
            return response()->json($orderedResponse, 200);
        } else {
            return $orderedResponse;
        }

    }
    private function processGrArRevenue($actualRevenue, $planRevenue, $lastYearActualRevenue) {
        if($planRevenue==null) {
            $planRevenue=1;
        }
        if($lastYearActualRevenue==null) {
            $lastYearActualRevenue=1;
        }

        // GR
        $grPlanRevenue = $this->calculateGr($actualRevenue, $planRevenue, $planRevenue);
        $grLYRevenue = $this->calculateGr($actualRevenue, $lastYearActualRevenue, $lastYearActualRevenue);
        // AR
        $arPlanRevenue = $this->calculateAr($actualRevenue, $planRevenue);
        $arLYRevenue = $this->calculateAr($actualRevenue, $lastYearActualRevenue);

        $reportData['gr_plan_revenue'] = $grPlanRevenue;
        $reportData['gr_last_year_revenue'] = $grLYRevenue;
        $reportData['ar_plan_revenue'] = $arPlanRevenue;
        $reportData['ar_last_year_revenue'] = $arLYRevenue;
        return $reportData;
    }

    private function processGrArNumberOfItem($actualNumberOfItem, $planNumberOfItem, $lastYearActualNumberOfItem) {
        if($planNumberOfItem==null || $planNumberOfItem==0) {
            $planNumberOfItem=1;
        }
        if($lastYearActualNumberOfItem==null) {
            $lastYearActualNumberOfItem=1;
        }

        // GR
        $grPlanNumberOfItem = $this->calculateGr($actualNumberOfItem, $planNumberOfItem, $planNumberOfItem);
        $grLYNumberOfItem = $this->calculateGr($actualNumberOfItem, $lastYearActualNumberOfItem, $lastYearActualNumberOfItem);
        
        //AR
        $arPlanNumberOfItem = $this->calculateAr($actualNumberOfItem, $planNumberOfItem);
        $arLYNumberOfItem = $this->calculateAr($actualNumberOfItem, $lastYearActualNumberOfItem);

        $reportData['gr_plan_number_of_item'] = $grPlanNumberOfItem;
        $reportData['gr_last_year_number_of_item'] = $grLYNumberOfItem;
        $reportData['ar_plan_number_of_item'] = $arPlanNumberOfItem;
        $reportData['ar_last_year_number_of_item'] = $arLYNumberOfItem;
        return $reportData;
    }



    /* Achievement By Time*/
    public function getAchievementAnnually($year, Request $request){
        $category = $request->get('category');

        $achievementPerMonth = AchievementDetail::selectRaw('year, month_id, 
                sum(target_unitmono_revenue) as totalUnitMonoRevenue, 
                sum(target_unitcolor_revenue) as totalUnitColorRevenue, 
                sum(target_consumable_revenue) as totalConsRevenue,  
                sum(target_unitmono_count) as totalUnitMonoCount, 
                sum(target_unitcolor_count) as totalUnitColorCount')
            ->join('achievements', 'achievements.id', '=', 'achievement_details.achievement_id')
            ->where('year', $year)
            ->groupBy('year', 'month_id')
            ->get();

        if($achievementPerMonth != null) {
            foreach ($achievementPerMonth as $achievement) {

                $totalAchievement = $this->getTotalAchievement($achievement, $category);

                $annuallyAchievements[] = array(
                    'day' => 1,
                    'month' => $achievement->month_id,
                    'year' => $year,
                    'totalRevenue' => $totalAchievement['revenue'],
                    'totalCount' => $totalAchievement['count']
                );
            }
            $response = $annuallyAchievements;
        } else {//fail response
            $response = [
                'error' => true,
                'message' => 'Data tidak ditemukan'
            ];
        }



        return response()
            ->json($response);
    }
    public function getAchievementQuarterly($year, Request $request){
        $totalRevenue = 0;
        $totalCount = 0;

        $quater = 1;

        $category = $request->get('category');

        $achievementPerMonth = AchievementDetail::selectRaw('year, month_id, 
                sum(target_unitmono_revenue) as totalUnitMonoRevenue, 
                sum(target_unitcolor_revenue) as totalUnitColorRevenue, 
                sum(target_consumable_revenue) as totalConsRevenue,  
                sum(target_unitmono_count) as totalUnitMonoCount, 
                sum(target_unitcolor_count) as totalUnitColorCount')
            ->join('achievements', 'achievements.id', '=', 'achievement_details.achievement_id')
            ->where('year', $year)
            ->groupBy('year', 'month_id')
            ->get();


        if($achievementPerMonth != null) {
            foreach ($achievementPerMonth as $achievement) {
                $totalAchievement = $this->getTotalAchievement($achievement, $category);
                $totalRevenue += $totalAchievement['revenue'];
                $totalCount += $totalAchievement['count'];

                //set perquarter
                if($achievement->month_id % 3 == 0) {
                    $quarterlyAchievements[] = array(
                        'quarter' => $quater,
                        'totalRevenue' => $totalRevenue,
                        'totalCount' => $totalCount
                    );

                    $totalRevenue = 0;
                    $totalCount = 0;
                    $quater++;

                }
            }
            $response = $quarterlyAchievements;
        } else {
            $response = [
                'error' => true,
                'message' => 'Data tidak ditemukan'
            ];
        }


        return response()
            ->json($response);
    }
    public function getAchievementYearly(Request $request) {
        $category = $request->get('category');

        $achievementPerYears = AchievementDetail::selectRaw('year, 
                sum(target_unitmono_revenue) as totalUnitMonoRevenue, 
                sum(target_unitcolor_revenue) as totalUnitColorRevenue, 
                sum(target_consumable_revenue) as totalConsRevenue,  
                sum(target_unitmono_count) as totalUnitMonoCount, 
                sum(target_unitcolor_count) as totalUnitColorCount, ')
            ->join('achievements', 'achievements.id', '=', 'achievement_details.achievement_id')
            ->groupBy('achievements.year')
            ->get();

        foreach ($achievementPerYears as $achievement) {
            $year = $achievement->year;
            $totalAchievement = $this->getTotalAchievement($achievement, $category);

            $yearlyAchievements[] = array(
                'year' => $year,
                'totalRevenue' => $totalAchievement['revenue'],
                'totalCount' => $totalAchievement['count']

            );
        }

        return response()
            ->json($yearlyAchievements);

    }


    /*Achievement by Role*/
    public function getAchivementByRole($role, Request $request) {
        $year = $request->get('year');
        $quarter = $request->get('quarter');
        $month = $request->get('month');
        $category = $request->get('category');

        $tren = $request->get('tren');

        $achievementSelectRawQuery = 'achievements.user_id, dealers.company_name, `year`,
                sum(target_unitmono_revenue) as totalUnitMonoRevenue, 
                sum(target_unitcolor_revenue) as totalUnitColorRevenue, 
                sum(target_consumable_revenue) as totalConsRevenue,  
                sum(target_unitmono_count) as totalUnitMonoCount, 
                sum(target_unitcolor_count) as totalUnitColorCount';

        if($tren == "monthly") {
            $achievementSelectRawQuery .= ',month_id';
        }

        $achievementSelectRaw = AchievementDetail::selectRaw($achievementSelectRawQuery);
        $achievementJoin = $achievementSelectRaw->join('achievements', 'achievements.id', '=', 'achievement_details.achievement_id')
            ->join('dealers', 'dealers.user_id', '=', 'achievements.user_id');

        $achievementCondition = $achievementJoin->where('year', $year);
        if($tren == "monthly") {
            $achievementCondition = $achievementCondition->where('month_id', $month);
        }

        $achievementGroup = $achievementCondition->groupBy('dealers.id', 'achievements.user_id', 'year');
        if($tren == "monthly") {
            $achievementGroup = $achievementGroup->groupBy('month_id');
        }

        $achievements = $achievementGroup->get();

        foreach ($achievements as $achievement) {

            $year = $achievement->year;
            $dealer = $achievement->company_name;
            $totalAchievement = $this->getTotalAchievement($achievement, $category);


            $achievementResponse[] = array(
                'year' => $year,
                'dealer' => $dealer,
                'totalRevenue' => $totalAchievement['revenue'],
                'totalCount' => $totalAchievement['count']

            );
        }

        return response()
            ->json($achievementResponse);
    }

    private function getTotalAchievement($achievement, $category) {
        $totalUnitMonoRevenue = $achievement->totalUnitMonoRevenue;
        $totalUnitColorRevenue = $achievement->totalUnitColorRevenue;
        $totalConsRevenue = $achievement->totalConsRevenue;
        $totalOptionalRevenue = $achievement->totalOptionalRevenue;
        $totalUnitMonoCount = $achievement->totalUnitMonoCount;
        $totalUnitColorCount = $achievement->totalUnitColorCount;
        $totalConsCount = $achievement->totalConsCount;
        $totalOptionalCount = $achievement->totalOptionalCount;

        switch ($category) {
            case 'all' :
                $totalRevenue = $totalUnitMonoRevenue + $totalUnitColorRevenue + $totalConsRevenue + $totalOptionalRevenue;
                $totalCount = $totalUnitMonoCount + $totalUnitColorCount + $totalConsCount + $totalOptionalCount;
                break;
            case 'unit' :
                $totalRevenue = $totalUnitMonoRevenue + $totalUnitColorRevenue;
                $totalCount = $totalUnitMonoCount + $totalUnitColorCount;
                break;
            case 'unitmono' :
                $totalRevenue = $totalUnitMonoRevenue;
                $totalCount = $totalUnitMonoCount;
                break;
            case 'unitcolor' :
                $totalRevenue = $totalUnitColorRevenue;
                $totalCount = $totalUnitColorCount;
                break;
            case 'cons' :
                $totalRevenue = $totalConsRevenue;
                $totalCount = $totalConsCount;
                break;
            case 'optional' :
                $totalRevenue = $totalOptionalRevenue;
                $totalCount = $totalOptionalCount;
                break;
            default :
                $totalRevenue = 0;
                $totalCount = 0;
                break;
        }
        return [
            'revenue' => $totalRevenue,
            'count' => $totalCount
        ];
    }

    public function insertAchievementDetail($dataAchievementDetail) {
        $success = 0;
        foreach ($dataAchievementDetail as $achievementDetail) {
            
            $achievementDetailCreate = AchievementDetail::create($achievementDetail);
            if($achievementDetailCreate) {
                $success++;
            }
        }
        if($success == count($dataAchievementDetail)) {
            return true;
        }
        return false;
    }
    public function updateAchievementDetail($dataAchievement, $achievementId, $monthId) {
        unset($dataAchievement['target_consumable_count']);
        unset($dataAchievement['target_optional_revenue']);
        unset($dataAchievement['target_optional_count']);
        
        $achievementDetailUpdate = AchievementDetail::where('achievement_id', $achievementId)
            ->where('month_id', $monthId)
            ->update($dataAchievement);
    }

    //GR & AR Formula
    private function calculateGr($actual, $inactual, $divider) {
        return round(($actual - $inactual) / $divider, 5);
    }
    private function calculateAr($actual, $divider) {
        return round(($actual / $divider) * 100, 2);
    }



}
