<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\UploadExcelRequest;
use App\Http\Requests\UploadPhotoRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ProductCategories;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Exception;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'product';
    public function index()
    {
        $productCategories = ProductCategories::all();
        $products = Product::orderBy('id', 'desc')->get();
        return view('admin.product.index', compact('products', 'productCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UploadPhotoRequest $uploadPhotoRequest)
    {
        $request->validate([
            'type' => 'required|string|max:255',
            'product_code' => 'required|string|max:255',
            'category' => 'required|string|max:255'
        ]);


        //Create product
        $photo = "static_image/default.png";
        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('products');
            $photo = $photoFileName;
        }


        $productType = $request->get('type');
        $productCode = $request->get('product_code');
        $productCategory = $request->get('category');

        $this->insert($productCode, $productType, $productCategory, $photo);

        return redirect($this->redirectUrl);
    }

    public function import(Request $request, UploadExcelRequest $uploadExcelRequest) {
        $products = $uploadExcelRequest->get();

        foreach ($products as $product) {
            $productType = $product->type;
            $productCode = $product->product_code;
            $productCategory = $product->category;


            try {
                $this->insert($productCode, $productType, $productCategory);
            } catch (Exception $exception) {
                dd($exception);
                $request->session()->flash('error', true);
                $request->session()->flash('message', "Product Code Telah Terdaftar");
            }

        }
        return redirect($this->redirectUrl);
    }

    private function insert($productCode, $productType, $productCategory, $photo) {
        $productCategory = strtolower($productCategory);
        if($productCategory == "mono mfd") {
            $productCategory = "unit";
        }
        $productCreated = Product::create([
            'user_id' => Auth::user()->id,
            'type' => $productType,
            'product_code' => $productCode,
            'category' => $productCategory,
            'photo' => $photo
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function productByCategory($category = "unit") {
        $products = Product::where('category', $category)->get();
        return response()
            ->json($products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.product.edit', compact('product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, UploadPhotoRequest $uploadPhotoRequest)
    {

        $product = Product::find($id);

        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('products');
            $product->photo = $photoFileName;
        }


        $product->type = $request->get('type');
        $product->product_code = $request->get('product_code');
        $product->category = $request->get('category');
        $product->save();


        return redirect($this->redirectUrl);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect($this->redirectUrl);
    }
}
