<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\SellOut;
use App\Slide;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::all();
        return view('admin.dashboard', compact('slides'));
    }
}
