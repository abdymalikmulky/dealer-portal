<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\UploadPhotoRequest;
use App\SalesTool;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SalesToolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'salestool';
    public $title = "Sales Tools";
    public function index()
    {
        $salesTools = SalesTool::orderBy('created_at', 'desc')->get();
        return view('admin.salestool.index', compact('salesTools'))->with('title', $this->title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.salestool.create')->with('title', $this->title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UploadPhotoRequest $uploadPhotoRequest)
    {
        $request->validate([
            'title' => 'required|string',
            'description' => 'required|string'
        ]);

        $file = "static_image/default.png";
        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('salestools');
            $file = $photoFileName;
        }
        //Create
        $create = SalesTool::create([
            'created_by' => Auth::user()->id,
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'file' => $file
        ]);

        return redirect($this->redirectUrl);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $SalesTool = SalesTool::find($id);
        return view('admin.salestool.show', compact('SalesTool'))->with('title', $this->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salesTool = SalesTool::find($id);
        return view('admin.salestool.edit', compact('salesTool'))->with('title', $this->title);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, UploadPhotoRequest $uploadPhotoRequest)
    {
        $request->validate([
            'title' => 'required|string',
            'description' => 'required|string'
        ]);

        $salesTool = SalesTool::find($id);

        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('salestools');
            $salesTool->file = $photoFileName;
        }

        $salesTool->title = $request->get('title');
        $salesTool->description = $request->get('description');
        $salesTool->save();


        return redirect($this->redirectUrl);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $salesTool = SalesTool::find($id);
        $salesTool ->delete();
        return redirect($this->redirectUrl);
    }
}
