<?php

namespace App\Http\Controllers\admin;

use Config;
use DB;
use App\Util;
use App\Dealer;
use App\Http\Requests\UploadExcelRequest;
use App\Notifications\ActionInSystem;
use App\UserDetail;
use App\Product;
use App\ProductCategories;
use App\Reseller;
use App\SellThrough;
use App\SellThroughDetail;
use App\SellThroughDetailProduct;
use App\Achievement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Imports\SellThroughImport;
use Maatwebsite\Excel\Facades\Excel;


class SellThroughController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'sellthrough';
    public $title = "Sell Through of ";
    public $isInitstock = 0;
    public $productType = "unit";

    public $bdm = 0;

    public function __construct()
    {
        if(app('request')->input('isInitStock') != "") {
            $this->isInitstock = app('request')->input('isInitStock');
        }
        $this->bdm = app('request')->input('bdm');
        $this->productType = app('request')->input('product_type');

        if ($this->isInitstock) {
            $this->title = "Stock On Hand";

        } else {
            $productCategories = ProductCategories::all();
            foreach ($productCategories as $productCategory) {
                if ($this->productType == $productCategory->category) {
                    $this->title .= $productCategory->description;
                }
            }


        }
    }
    public function index()
    {
        $roleId = Auth::user()->roles[0]->id;
        //Data BDM For COS
        $bdms = UserDetail::where('created_by', Auth::user()->id)->get();
        
        //Data Dealers for BDM 
        if($roleId == Config::get('constants.PM')) {
            $dealers = Dealer::all();
        } else {
            $dealers = Dealer::where('created_by', Auth::user()->id)->get();
        }

        if($this->bdm != 0) { //Only filtered dealer by BDM
            $dealers = Dealer::where('created_by', $this->bdm)->get();
        }

        $productCategories = ProductCategories::all();
        
        $sellThroughs = Sellthrough::where('id', '!=' , '')
        ->whereIn('dealer_id', function($query)
        {
            $bdmSubQueryFrom = $query->select(DB::raw('id'))->from('dealers');
            if(Auth::user()->roles[0]->id == Config::get('constants.BDM') || $this->bdm != 0) { //DATA FOR BDM
                $createdById = Auth::user()->id;
                if($this->bdm != 0) {
                    $createdById = $this->bdm;
                }
                $bdmSubQueryFrom->where('created_by', $createdById);

            } else { //DATA FOR COS
                $bdmSubQueryFrom->wherein('created_by', function($subQuery) 
                {
                    $cosSubQueryFrom = $subQuery->select(DB::raw('id'))->from('user_details');
                });
            }

        });

        //Data FOR PM
        if($roleId == Config::get('constants.PM')) {
            $sellThroughs = Sellthrough::whereRAW("1=1");
        }

        if($this->isInitstock) {
            $sellThroughs->where('is_init_stock', 1);
        } else {
            $sellThroughs->where('is_init_stock', 0)->where('category', $this->productType);
        }
        $sellThroughs->orderBy('id', 'desc');
        $sellThroughs = $sellThroughs->get();


        return view('admin.sellthrough.index', compact('sellThroughs', 'dealers', 'bdms', 'productCategories'))
            ->with('title', $this->title)
            ->with('isInitStock', $this->isInitstock)
            ->with('productType', $this->productType);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dealers = Dealer::where('created_by', Auth::user()->id)->get();
        return view('admin.sellthrough.create', compact('dealers'))
            ->with('title', $this->title)
            ->with('isInitStock', $this->isInitstock)
            ->with('productType', $this->productType);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request) {
        $request->validate([
            'dealer' => 'required',
            'transaction_date' => 'required',
            'product_type' => 'required',
            'product' => 'required',
            'qty' => 'required'
        ]);


        //Create product
        $createdBy = Auth::user()->id;
        $dealerId = $request->get('dealer');

        $formatedTransactionDate = date("Y-m-d", strtotime($request->get('transaction_date')));
        $transactionDate = $formatedTransactionDate;

        $productType = $request->get('product_type');
        
        $products = $request->get('product');
        $qty = $request->get('qty');
        $sn = $request->get('sn');
        $revenues = $request->get('revenue');


        // for ($month=0; $month < 12; $month++) { 
            // $revenue = $revenue + rand(15423,15423543);
            $sellThrough = $this->insertSellThrough($dealerId, $this->isInitstock, $productType, $transactionDate, $createdBy);
            $sellThroughId = $sellThrough->id;
            
            for ($i=0; $i < count($products); $i++) { 
                try {
                    $revenueAmount = $qty[$i] * $revenues[$i];
                    $sellThroughDetail = $this->insertSellThroughProducts($sellThroughId, $products[$i], $qty[$i], $revenueAmount);

                    $sellThroughDetailId = $sellThroughDetail->id;
                    
                    for($j=0; $j < count($sn[$i]); $j++) {
                        $sellThroughDetailProduct = $this->insertSellThroughDetailProducts($sellThroughDetailId, $sn[$i][$j]);
                    }
                } catch (Exception $exception) {
                    $request->session()->flash('error', true);
                    $request->session()->flash('message', "Product Code Telah Terdaftar");
                }
            }
            
            $dateConverted = Util::addMonth($transactionDate, 1);
            $transactionDate = $dateConverted->format('Y-m-d');
            
        // }
        
        

        if($this->isInitstock) {
            $this->redirectUrl .= "?isInitStock=1";
        } else {
            $this->redirectUrl .= "?product_type=" . $this->productType;
        }

        $actionInSystemNotif = new ActionInSystem();
        $actionInSystemNotif->setSellThrough($sellThrough);
        $sellThrough->dealer->user->notify($actionInSystemNotif);
        
        return redirect($this->redirectUrl);
    }


    public function importExcel() 
    {
        $order = 0;
        $sellThroughData = Excel::toCollection(new SellThroughImport, 'sellthrough.xlsx');
        foreach($sellThroughData[0] as $key => $value) {
            $year = $value[0];
            $category = strtolower($value[1]);
            $bpName = $value[2];
            $transactionDate = date('Y-m-d', strtotime(str_replace(".", "-", $value[3])));
            $productCode = $value[4];
            $quantity = $value[5];
            $revenue = $value[6];
            
            $bpId = 0;
            $bdmId = 0;

            $bp = Dealer::where('company_name', $bpName);
            echo $order;
            echo ". ";
            echo $bpName;
            if($bp->exists()) {
                $product = Product::where('product_code', $productCode)->first();
                
                $bpData = $bp->first();
                $productId = $product->id;

                $bpId = $bpData->id;
                $bdmId = $bpData->created_by;
                
                echo " | ";
                echo $transactionDate;
                echo " | ";
                echo $bpId;
                echo " | ";
                echo $bdmId;
                echo " | ";
                echo $product->id;

                // $sellThrough = $this->insertSellThrough($bpId, 0, $category, $transactionDate, $bdmId);
                // $sellThroughId = $sellThrough->id;

                // $created = $this->insertSellThroughProducts($sellThroughId, $productId, $quantity, $revenue);
                // if($created) {
                    echo " | SUCCESS";
                // } else {
                //     echo "FAILED INSERT";
                // }
                // die();
            } else {
                echo "FAILED BP";
            }

            

            echo "<hr>";
            $order++;
        }
        
    }

    public function import(Request $request, UploadExcelRequest $uploadExcelRequest) {

        $sellThroughId = 0;
        $createdBy = Auth::user()->id;

        $dealerId = $request->get('dealer');
        $isInitStock = $request->get('is_init_stock');
        if($isInitStock == "") {
            $isInitStock = 0;
        }
        $productType = $request->get('product_type');

        $sellThroughProducts = $uploadExcelRequest->formatDates(true, 'Y-m-d')->get();

        $index = 0;
        $isInvalidProduct = false;

        foreach ($sellThroughProducts as $key => $sellThroughProduct) {
            $productCode = $sellThroughProduct->product_code;
            $products[$index] = Product::where('product_code', $productCode)->where('category', $productType)->first();
            if (!$products[$index]) {
                $request->session()->flash('error', "Proses Import Anda Terhenti di Product " . $productCode . ". Disebabkan karena Product yang anda upload tidak ditemukan atau tidak sesuai");
                $isInvalidProduct = true;
            }
            $index++;
        }

        if(!$isInvalidProduct) {
            $index = 0;
            foreach ($sellThroughProducts as $key => $sellThroughProduct) {
                $productCode = $sellThroughProduct->product_code;
                $product = $products[$index];
                $productId = $product->id;

                $qty = $sellThroughProduct->qty;

                //row pertama dengan value revenue & transaction date
                if($index == 0) {
                    $transactionDate = $sellThroughProduct->tanggal_Sellout;
                    $revenue = $sellThroughProduct->revenue;

                    $sellThrough = $this->insertSellThrough($dealerId, $isInitStock, $productType, $transactionDate, $createdBy);
                    $sellThroughId = $sellThrough->id;
                }

                try {
                    $this->insertSellThroughProducts($sellThroughId, $productId, $qty);
                } catch (Exception $exception) {
                    $request->session()->flash('error', true);
                    $request->session()->flash('message', "Product Code Telah Terdaftar");
                }

                $index++;
            }
        }


        if($isInitStock) {
            $this->redirectUrl .= "?isInitStock=1";
        } else {
            $this->redirectUrl .= "?product_type=" . $productType;
        }
        return redirect($this->redirectUrl);
    }

    private function insertSellThrough($dealerId, $isInitStock, $productType, $transactionDate, $createdBy) {
        $sellThroughCreated = SellThrough::create([
            'dealer_id' => $dealerId,
            'is_init_stock' => $isInitStock,
            'category' => $productType,
            'transaction_date' => $transactionDate,
            'created_by' => $createdBy
        ]);
        
        return $sellThroughCreated;
    }

    private function insertSellThroughProducts($sellThroughId, $productId, $qty, $revenue) {
        $sellThroughProductCreated = SellThroughDetail::create([
            'sell_through_id' => $sellThroughId,
            'product_id' => $productId,
            'qty' => $qty,
            'revenue' => $revenue
        ]);
        return $sellThroughProductCreated;
    }

    private function insertSellThroughDetailProducts($sellThroughDetailId, $serialNumber) {
        $sellThroughDetailProductCreated = SellThroughDetailProduct::create([
            'sell_through_detail_id' => $sellThroughDetailId,
            'serial_number' => $serialNumber
        ]);
        return $sellThroughDetailProductCreated;
    }
    private function deleteSellThroughDetailProducts($sellThroughDetailId) {
        $sellThroughDetailProduct = SellThroughDetailProduct::where('sell_through_detail_id', $sellThroughDetailId);
        $sellThroughDetailProduct->delete();
    }




    public function storeByDealer(Request $request, $dealerId) {

        $request->validate([
            'product' => 'required',
            'reseller' => 'required',
            'customer_name' => 'required|string|max:50',
            'invoice' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
        ]);

        $invoicePhoto = $request->file('invoice');
        $fileOriginalName = $invoicePhoto->getClientOriginalName();
        $fileName = time().'_'.$fileOriginalName;
        //upload ~
        $invoicePhoto->move(public_path('images'), $fileName);



        $create = sellthrough::create([
            'product_id' => $request->get('product'),
            'reseller_id' => $request->get('reseller'),
            'customer_name' => $request->get('customer_name'),
            'invoice_photo' => $fileName,
            'by_who' => $request->get('user_id') //creator
        ]);
        return redirect($this->redirectUrlByDealer . $dealerId);
    }


    public function sellThroughProductByCategory($category) {
        $products = new Product();

        $dealerProductStock = $products->dealerProductStock($category);
        return response()
            ->json($dealerProductStock);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sellThrough = SellThrough::find($id);

        $isInitstock = $sellThrough->is_init_stock;
        $productType = $sellThrough->category;

        if ($isInitstock) {
            $this->title = "Stock On Hand";
        } else {
            $this->title = "Sell Through of " . strtoupper($productType);
        }

        $sellThroughProducts = SellThroughDetail::where('sell_through_id', $id)->get();

        return view('admin.sellthrough.detail.index', compact('sellThrough', 'sellThroughProducts'))
            ->with('title', $this->title)
            ->with('isInitStock', $isInitstock)
            ->with('productType', $productType);
    }

    public function createDetail($id)
    {
        $sellThrough = SellThrough::find($id);
        //$products = Product::where('category', $productCategory)->get();
        $sellThroughId = $id;

        return view('admin.sellthrough.detail.create', compact('sellThroughId'))
            ->with('title', $this->title);
    }

    public function storeDetail($sellThroughId, Request $request) {
        $request->validate([
            'product' => 'required',
            'qty' => 'required',
            'revenue' => 'required'
        ]);


        $products = $request->get('product');
        $qty = $request->get('qty');
        $revenue = $request->get('revenue');
        $sn = $request->get('sn');
        $revenueAmount = $qty * $revenue;
        

        try {
            $sellthroughDetail = $this->insertSellThroughProducts($sellThroughId, $products, $qty, $revenueAmount);
            $sellThroughDetailId = $sellthroughDetail->id;
            for($j=0; $j < count($sn[0]); $j++) {
                $sellThroughDetailProduct = $this->insertSellThroughDetailProducts($sellThroughDetailId, $sn[0][$j]);
            }
        } catch (Exception $exception) {
            $request->session()->flash('error', true);
            $request->session()->flash('message', "Product Code Telah Terdaftar");
        }


        return redirect($this->redirectUrl. '/' .$sellThroughId);
    }

    public function editDetail($id)
    {
        $sellThroughDetail = SellThroughDetail::find($id);
        $sellThroughId = $sellThroughDetail->sell_through_id;

        $sellThrough = SellThrough::find($sellThroughId);
        $productCategory = $sellThrough->category;

        $sellThroughDetailProducts = SellThroughDetailProduct::where('sell_through_detail_id', $id)->get();

        $products = Product::where('category', $productCategory)->get();
        $sellThroughDetail = SellThroughDetail::find($id);
        return view('admin.sellthrough.detail.edit', compact('sellThroughDetail', 'products', 'sellThroughDetailProducts'))
            ->with('title', $this->title)
            ->with('productType', $this->productType);
    }

    public function updateDetail(Request $request, $sellThroughId, $sellThroughDetailId)
    {

        $request->validate([
            'product' => 'required'
        ]);


        $product = $request->get('product');
        $sn = $request->get('sn');
        $qty = count($sn);
        if($sn=="") {
            $qty = $request->get('qty');
        }
        $revenue = $request->get('revenue');
        $revenueAmount = $qty * $revenue;
        
        if($request->get('qty') != "") {
            $qty = $request->get('qty');
        } 

        $sellThroughDetail = SellThroughDetail::find($sellThroughDetailId);
        $sellThroughDetail->product_id = $product;
        $sellThroughDetail->qty = $qty;
        $sellThroughDetail->revenue = $revenueAmount;

        //Update sell throught 
        $sellThrough = SellThrough::find($sellThroughId);
        $isInitstock = $sellThrough->is_init_stock;
        if($isInitstock) {
            $category = $request->get('product_type');
            $sellThrough->category = $category;
            $sellThrough->save();
        }


        //delete and then insert (update)
        $this->deleteSellThroughDetailProducts($sellThroughDetailId);
        for($j=0; $j < count($sn); $j++) {
            $sellThroughDetailProduct = $this->insertSellThroughDetailProducts($sellThroughDetailId, $sn[$j]);
        }

        $sellThroughDetail->save();
        return redirect($this->redirectUrl . '/' . $sellThroughId);
    }

    public function destroyDetail($sellThroughId, $sellThroughDetailId) {
        $sellThroughDetail = SellThroughDetail::find($sellThroughDetailId);
        $sellThroughDetail->delete();
        $this->deleteSellThroughDetailProducts($sellThroughDetailId);

        return redirect($this->redirectUrl. '/' .$sellThroughId);
    }

    public function showByDealer($dealerId) {
        $dealer = Dealer::find($dealerId);
        $sellthroughs = sellthrough::join('resellers', 'resellers.id', '=', 'sell_out.reseller_id')
            ->where('resellers.dealer_id', $dealerId)
            ->select('sell_out.*')
            ->get();

        return view('admin.sellthrough.dealer.index', compact('dealer', 'sellthroughs'));
    }

    public function showByReseller($resellerId) {
        $reseller = Reseller::find($resellerId);
        $dealer = Dealer::find($reseller->dealer_id);
        $dealerId = $dealer->id;
        $sellthroughs = sellthrough::join('resellers', 'resellers.id', '=', 'sell_out.reseller_id')
            ->where('resellers.id', $resellerId)
            ->select('sell_out.*')
            ->get();

        return view('admin.sellthrough.dealer.index', compact('dealer', 'reseller', 'sellthroughs'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sellThrough = SellThrough::find($id);
        $dealers = Dealer::where('created_by', Auth::user()->id)->get();
        return view('admin.sellthrough.edit', compact('dealers', 'sellThrough'))
            ->with('title', $this->title)
            ->with('isInitStock', $this->isInitstock)
            ->with('productType', $this->productType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'dealer' => 'required',
            'transaction_date' => 'required'
        ]);

        //Create product
        $createdBy = Auth::user()->id;
        $dealerId = $request->get('dealer');
        $formatedTransactionDate = date("Y-m-d", strtotime($request->get('transaction_date')));
        $transactionDate = $formatedTransactionDate;
        $revenue = $request->get('revenue');

        $sellThrough = SellThrough::find($id);
        $sellThrough->dealer_id = $dealerId;
        $sellThrough->transaction_date = $transactionDate;
        $sellThrough->save();
        

        if($this->isInitstock) {
            $this->redirectUrl .= "?isInitStock=1";
        } else {
            $this->redirectUrl .= "?product_type=" . $this->productType;
        }
        return redirect($this->redirectUrl);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sellThroughDetail = SellThroughDetail::where('sell_through_id', $id);
        $sellThroughDetail->delete();
        
        $sellthrough = SellThrough::find($id);
        $sellthrough->delete();
        if($this->isInitstock) {
            $this->redirectUrl .= "?isInitStock=1";
        } else {
            $this->redirectUrl .= "?product_type=" . $this->productType;
        }
        return redirect($this->redirectUrl);
    }

    public function validation($id, $status) {
        $sellthrough = sellthrough::find($id);
        $sellthrough->status_id = $status;
        $sellthrough->save();
        return redirect()->back();
    }

}
