<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UploadPhotoRequest;
use Illuminate\Http\Request;
use App\Slide;
use Illuminate\Support\Facades\Auth;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'slide';

    public function __construct()
    {
    }

    public function index()
    {
        $slides = Slide::all();
        return view('admin.slide.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UploadPhotoRequest $uploadPhotoRequest)
    {
        $request->validate([
            // 'photo' => 'required|dimensions:ratio=2/1'
            'photo' => 'required'
        ]);

        //Upload photo
        $photo = "static_image/default.png";
        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('static_image');
            $photo = $photoFileName;
        }


        //Create slide
        $createSlide = Slide::create([
            'headline' => $request->get('headline'),
            'picture' => $photo,
            'url' => $request->get('url'),
            'description' => $request->get('desc')
        ]);
    
        return redirect($this->redirectUrl);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slide = Slide::find($id);
        return view('admin.slide.show', compact('slide'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::find($id);
        return view('admin.slide.edit', compact('slide','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, UploadPhotoRequest $uploadPhotoRequest)
    {
        //Update slide
        $slide = Slide::find($id);

        if($uploadPhotoRequest->photo) {
            $photoFileName = $uploadPhotoRequest->photo->store('static_image');
            $slide->picture = $photoFileName;
        }

        $slide->headline = $request->get('headline');
        $slide->url = $request->get('url');
        $slide->description = $request->get('desc');
        $slide->save();


        return redirect($this->redirectUrl);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        $slide->delete();

        return redirect($this->redirectUrl);
    }


}
