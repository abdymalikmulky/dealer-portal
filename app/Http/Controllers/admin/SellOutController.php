<?php

namespace App\Http\Controllers\admin;

use DB;
use Config;
use App\Month;
use App\User;
use App\Dealer;
use App\Product;
use App\ProductCategories;
use App\Reseller;
use App\SellOut;
use App\SellOutDetail;
use App\SellOutDocument;
use App\SellThroughDetailProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadPhotoRequest;
use Illuminate\Support\Facades\Auth;

class SellOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $redirectUrl = ADMIN_PREFIX_URL . 'sellout';
    public $title = "Sell Out ";
    public $isInitstock = 0;
    public $productType = "unit";

    public function __construct()
    {

        $this->productType = app('request')->input('product_type');

        if ($this->isInitstock) {
            $this->title = "Stock On Hand";

        } else {
            $productCategories = ProductCategories::all();
            foreach ($productCategories as $productCategory) {
                if ($this->productType == $productCategory->category) {
                    $this->title .= $productCategory->description;
                }
            }


        }
    }
    public function index()
    {
        $userId = Auth::user()->id;
        $roleId = Auth::user()->roles[0]->id;
        $month = Month::all();
        $bdm = null;
        $cos = null;

        if($roleId==Config::get('constants.DEALER')) {
            $bdm = User::find(Auth::user()->dealer->created_by);
            $cos = User::find($bdm->user_detail->created_by);
            $sellOuts = SellOut::where('category', $this->productType)->where('created_by', $userId)->orderBy('id', 'DESC')->get();
        } else if($roleId==Config::get('constants.BDM')){
            $sellOuts = SellOut::where('category', $this->productType)
            ->whereIn('sell_out.created_by', function($query) use ($userId) 
            {
                $dealerSubQueryFrom = $query->select(DB::raw('user_id'))->from('dealers');    
                $dealerSubQueryFrom->wherein('dealers.created_by', function($subQuery) use ($userId)
                {
                    $bdmSubQueryFrom = $subQuery->select(DB::raw('id'))->from('users');
                    $bdmSubQueryFrom->where('id', $userId);
                });
            })->orderBy('id', 'DESC')->get();
        } else if($roleId==Config::get('constants.COS')){
            $sellOuts = SellOut::where('category', $this->productType)
            ->whereIn('sell_out.created_by', function($query) use ($userId) 
            {
                $dealerSubQueryFrom = $query->select(DB::raw('user_id'))->from('dealers');    
                $dealerSubQueryFrom->wherein('dealers.created_by', function($subQuery) use ($userId)
                {
                    $bdmSubQueryFrom = $subQuery->select(DB::raw('user_id'))->from('user_details');
                    $bdmSubQueryFrom->wherein('user_details.created_by', function($subQuery) use ($userId)
                    {
                        $cosSubQueryFrom = $subQuery->select(DB::raw('id'))->from('users');
                        $cosSubQueryFrom->where('id', $userId);
                    });
                });
            })->orderBy('id', 'DESC')->get();
        } else {
            $sellOuts = SellOut::where('category', $this->productType)->orderBy('id', 'DESC')->get();
        }
        
        $years = SellOut::selectRaw('YEAR(transaction_date) as year')->groupBy('year')->get();

        $dealers = Dealer::where('created_by', Auth::user()->id)->get();

        return view('admin.sellout.index', compact('sellOuts', 'dealers', 'bdm', 'cos', 'month', 'years'))
            ->with('title', $this->title)
            ->with('productType', $this->productType);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $request = Request();
        $isInitProduct = app('request')->input('init_product');
        //remove temp product
        if($isInitProduct){
            $request->session()->forget('products');
        }


        return view('admin.sellout.create')
            ->with('title', $this->title)
            ->with('productType', $this->productType);

    }
    public function addProduct() {
        $dealerId = Auth::user()->dealer->id;
        $dealerProductSerialNumbers = SellThroughDetailProduct::dealerProductItem($dealerId, $this->productType);
        
        
        $product = new Product();
        $productsStock = $product->dealerProductStock($this->productType);
        

        return view('admin.sellout.addproduct', compact('productsStock', 'dealerProductSerialNumbers'))
            ->with('title', $this->title)
            ->with('category', $this->productType)
            ->with('productType', $this->productType);
    }

    public function storeProduct(Request $request) {
        $redirectPath = "/create";

        $request->validate([
            'product' => 'required',
            'qty' => 'required'
        ]);

        $sellOutId = app('request')->input('sell_out_id');

        $sn = $request->get('sn');
        $productId = $request->get('product');
        $product = Product::find($productId);
        $qty = $request->get('qty');


        //set data | edit
        if($sellOutId){
            $redirectPath = "/" . $sellOutId . "/edit";

            $sellOutDetail = SellOutDetail::where('product_id', $productId)
                                ->where('sell_out_id', $sellOutId)->first();
            if($sellOutDetail && strpos($this->productType, "unit") === false) {
                $sellOutDetailQTY = $sellOutDetail->qty;
                $sellOutDetail->qty = $sellOutDetailQTY + $qty;
                $sellOutDetail->save();

            } else {
                $sellThroughDetailProduct = SellThroughDetailProduct::where('serial_number', $sn)->first();
                $sellThroughDetailProductId = null;
                if(count($sellThroughDetailProduct)) {
                    $sellThroughDetailProductId = $sellThroughDetailProduct->id;
                } 
                $sellOutDetailData = [
                    'sell_out_id' => $sellOutId,
                    'product_id' => $productId,
                    'sell_through_detail_product_id' => $sellThroughDetailProductId,
                    'qty' => $qty
                ];
                SellOutDetail::create($sellOutDetailData);
            }



        } else {
            //set data | tambah
            $isExist = false;
            $products = $request->session()->get('products');
            if($request->session()->exists('products')) {
                /*foreach($products as $key => $productSession) {
                    if($productSession['product']->id == $productId) {
                        $isExist = true;
                        $products[$key]['qty'] = $products[$key]['qty'] + $qty;
                    }
                }*/
                $request->session()->put('products', $products);
            }



            $productSellOut = array(
                'product' => $product,
                'sn' => $sn,
                'qty' => $qty
            );
            if(!$isExist) {
                $request->session()->push('products', $productSellOut);
            }

        }

        return redirect($this->redirectUrl . $redirectPath ."?product_type=" . $this->productType);

    }
    public function deleteProduct($index) {
        $sellOutDetailId = app('request')->input('id');
        $sellOutId = app('request')->input('sell_out_id');
        $redirectPath = "/create";

        if($sellOutDetailId) {
            $sellOutDetail = SellOutDetail::find($sellOutDetailId);
            $sellOutDetail->delete();
            $redirectPath = "/" . $sellOutId . "/edit";
        } else {
            $request = Request();
            $products = $request->session()->get('products');
            unset($products[$index]);
            $request->session()->put('products', $products);
        }

        return redirect($this->redirectUrl . $redirectPath . "?product_type=" . $this->productType);
    }
    public static function getQtyProductById($productId) {
        $request = Request();
        $products = $request->session()->get('products');
        $productFound = null;
        if(count($products) > 0) {
            foreach ($products as $product) {
                if($product['product']->id == $productId) {
                    $productFound = $product;
                    break;
                }
            }
        }
        return $productFound['qty'];
    }

    public function createByDealer($dealerId) {
        $dealer = Dealer::find($dealerId);
        $resellers = Reseller::where("dealer_id", $dealerId)->get();
        $products = Product::all();

        return view('admin.sellout.dealer.create', compact('dealer', 'resellers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'transaction_date' => 'required',
            'company_name' => 'required',
            'company_address' => 'required',
            'phone' => 'required',
            'name' => 'required',
            'email' => 'required'
        ]);

        $transactionDate = date("Y-m-d", strtotime($request->get('transaction_date')));
        $companyName = $request->get('company_name');
        $companyAddress = $request->get('company_address');
        $phone = $request->get('phone');
        $name = $request->get('name');
        $email = $request->get('email');
        $dealerId = Auth::user()->dealer->id;
        $userId = Auth::user()->id;



        $createSellOut = SellOut::create([
            'dealer_id' => $dealerId,
            'transaction_date' => $transactionDate,
            'category' => $this->productType,
            'cust_company_name' => $companyName,
            'cust_company_address' => $companyAddress,
            'cust_phone' => $phone,
            'cust_name' => $name,
            'cust_email' => $email,
            'created_by' => $userId
        ]);
        $sellOutId = $createSellOut->id;

        //Upload sellout document
        $this->uploadDocuments($request->documents, $sellOutId, $companyName);
        


        $products = $request->session()->get('products');
        foreach ($products as $product) {
            $productId = $product['product']->id;
            $qty = $product['qty'];
            $sn = $product['sn'];
            $sellThroughDetailProduct = SellThroughDetailProduct::where('serial_number', $sn)->first();
            $sellThroughDetailProductId = null;
            if(count($sellThroughDetailProduct)) {
                $sellThroughDetailProductId = $sellThroughDetailProduct->id;
            } 
            
            $sellOutDetailData = [
                'sell_out_id' => $sellOutId,
                'product_id' => $productId,
                'sell_through_detail_product_id' => $sellThroughDetailProductId,
                'qty' => $qty
            ];
            
            $createSellOutDetail = SellOutDetail::create($sellOutDetailData);
            
        }
        if($createSellOutDetail){
            $request->session()->forget('products');
        }
        return redirect($this->redirectUrl . "?product_type=" . $this->productType);
    }

    public function storeByDealer(Request $request, $dealerId) {

        $request->validate([
            'product' => 'required',
            'reseller' => 'required',
            'customer_name' => 'required|string|max:50',
            'invoice' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024'
        ]);

        $invoicePhoto = $request->file('invoice');
        $fileOriginalName = $invoicePhoto->getClientOriginalName();
        $fileName = time().'_'.$fileOriginalName;
        //upload ~
        $invoicePhoto->move(public_path('images'), $fileName);

        $create = SellOut::create([
            'product_id' => $request->get('product'),
            'reseller_id' => $request->get('reseller'),
            'customer_name' => $request->get('customer_name'),
            'invoice_photo' => $fileName,
            'by_who' => $request->get('user_id') //creator
        ]);
        return redirect($this->redirectUrlByDealer . $dealerId);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function showByDealer($dealerId) {
        $dealer = Dealer::find($dealerId);
        $sellOuts = SellOut::join('resellers', 'resellers.id', '=', 'sell_out.reseller_id')
            ->where('resellers.dealer_id', $dealerId)
            ->select('sell_out.*')
            ->get();

        return view('admin.sellout.dealer.index', compact('dealer', 'sellOuts'));
    }

    public function showByReseller($resellerId) {
        $reseller = Reseller::find($resellerId);
        $dealer = Dealer::find($reseller->dealer_id);
        $dealerId = $dealer->id;
        $sellOuts = SellOut::join('resellers', 'resellers.id', '=', 'sell_out.reseller_id')
            ->where('resellers.id', $resellerId)
            ->select('sell_out.*')
            ->get();

        return view('admin.sellout.dealer.index', compact('dealer', 'reseller', 'sellOuts'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $request = Request();
        $isInitProduct = app('request')->input('init_product');
        //remove temp product
        if($isInitProduct){
            $request->session()->forget('products');
        }

        $sellOut = SellOut::find($id);
        $sellOutDetail = SellOutDetail::where('sell_out_id', $id)->get();

        return view('admin.sellout.edit', compact('sellOut', 'sellOutDetail'))
            ->with('title', $this->title)
            ->with('productType', $this->productType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'transaction_date' => 'required',
            'company_name' => 'required',
            'company_address' => 'required',
            'phone' => 'required',
            'name' => 'required',
            'email' => 'required'
        ]);

        $transactionDate = date("Y-m-d", strtotime($request->get('transaction_date')));
        $companyName = $request->get('company_name');
        $companyAddress = $request->get('company_address');
        $phone = $request->get('phone');
        $name = $request->get('name');
        $email = $request->get('email');
        $dealerId = Auth::user()->dealer->id;

        $sellOut = SellOut::find($id);
        $sellOut->transaction_date = $transactionDate;
        $sellOut->cust_company_name = $companyName;
        $sellOut->cust_company_address = $companyAddress;
        $sellOut->cust_phone = $phone;
        $sellOut->cust_name = $name;
        $sellOut->cust_email = $email;
        $sellOut->save();

        //Upload sellout document
        if(count($request->documents) > 0) {
            $this->uploadDocuments($request->documents, $id, $companyName);
        }
        


        return redirect($this->redirectUrl . "?product_type=" . $this->productType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete sellout document/file
        $sellOutDocument = SellOutDocument::where('sell_out_id', $id);
        $sellOutDocument->delete();

        //delete sellout detail
        $sellOutDetail = SellOutDetail::where('sell_out_id', $id);
        $sellOutDetail->delete();

        //delete sellout master
        $sellOut = SellOut::find($id);
        $sellOut->delete();

        return redirect()->back();
    }

    public function validation($id, $status) {
        $sellOut = SellOut::find($id);
        $sellOut->status_id = $status;
        $sellOut->save();
        return redirect()->back();
    }


    public function deleteDocument($id) {
        $sellOutDocument = SellOutDocument::find($id);
        $sellOutDocument->delete();
        return redirect()->back();
    }

    public function uploadDocuments($documents, $sellOutId, $companyName) {
        foreach ($documents as $document) {
            //$documentFileName = $document->store('sellout');
            
            $uploadPath = storage_path('app/sellout');
            $originalFileName = $document->getClientOriginalName();
            $fileName =  $companyName . "_" . date('ymdhis') . "_" . str_replace(" ", "_", $originalFileName);
            $uploadFile = $document->move($uploadPath, $fileName);  

            SellOutDocument::create([
                'sell_out_id' => $sellOutId,
                'filename' => $fileName
            ]);
        }
    }

}
