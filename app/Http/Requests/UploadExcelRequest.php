<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Files\ExcelFile;

class UploadExcelRequest extends ExcelFile {

    public function getFile()
    {
        // Import a user provided file
        $file = Input::file('file')->getRealPath();
        // Return it's location
        return $file;
    }

    public function getFilters()
    {
        return [
            'chunk'
        ];
    }

}