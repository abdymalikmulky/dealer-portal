<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Config;
use Illuminate\Support\Facades\Auth;
use App\AchievementDetail;
use App\SellThrough;

class Achievement extends Model
{
    protected $fillable = ['user_id', 'achievement_for', 'year', 'created_by'];
    protected $category = "";
    protected $year = "";
    protected $month = "";
    protected $isMonthToDate = "";
    protected $monthCondition = "";
    protected $dataUser = array();
    private $targetRevenue = "revenue";
    private $targetNOI = "noi"; //numberofitem

    public function detail() {
        return $this->hasMany('App\AchievementDetail', 'achievement_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function setParameterTarget($year, $month, $isMonthToDate, $category, $dataUser) {
        $this->year = $year;
        $this->month = $month;
        $this->isMonthToDate = $isMonthToDate;
        $this->category = $category;
        $this->dataUser = $dataUser;
    }

    public function getTargetPlanAndActual() {
        $userLoggedInRole = Auth::user()->roles[0]->pivot->role_id;        
        $dataUser = $this->dataUser;
        

        $this->monthCondition = '=';
        if(!$this->isMonthToDate) {
            $this->monthCondition = '<=';
        }

        $targetPlanReport = null;
        $revenueTotalReport = null;


        if($dataUser['dealer_id'] == "all" AND $dataUser['bdm_id'] == "all" AND $dataUser['cos_id'] == "all") {
            $targetPlanReport = $this->targetPlanInitialQuery();

            $revenueTotalReport['current_year'] = $this->actualRevenueInitialQuery()->first();
            $revenueTotalReport['last_year'] = $this->actualRevenueInitialQuery(true)->first();
            $numberOfItemTotalReport['current_year'] = $this->actualNumberOfItemInitialQuery()->first();
            $numberOfItemTotalReport['last_year'] = $this->actualNumberOfItemInitialQuery(true)->first();
        } else {
            if($userLoggedInRole==Config::get('constants.PM')) {
                if($dataUser['dealer_id'] != "all") {

                    $targetPlanReport = $this->getTargetPlanByDealer($dataUser['dealer_id']);
                    $revenueTotalReport = $this->getActualDataByDealer($dataUser['dealer_id'], $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataByDealer($dataUser['dealer_id'], $this->targetNOI);


                } else if($dataUser['bdm_id'] != "all") {
                    
                    $targetPlanReport = $this->getTargetPlanAllDealerByBDM($dataUser['bdm_id']);
                    $revenueTotalReport = $this->getActualDataAllDealerByBDM($dataUser['bdm_id'], $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataAllDealerByBDM($dataUser['bdm_id'], $this->targetNOI);    
                
                } else if ($dataUser['cos_id'] != "all") {
                
                    $targetPlanReport = $this->getTargetPlanAllBDMByCos($dataUser['cos_id']);
                    $revenueTotalReport = $this->getActualDataAllBDMByCos($dataUser['cos_id'], $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataAllBDMByCos($dataUser['cos_id'], $this->targetNOI);    


                }
            } else if($userLoggedInRole==Config::get('constants.COS')) {
                if($dataUser['dealer_id'] != "all") {
                    $targetPlanReport = $this->getTargetPlanByDealer($dataUser['dealer_id']);
                    $revenueTotalReport = $this->getActualDataByDealer($dataUser['dealer_id'], $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataByDealer($dataUser['dealer_id'], $this->targetNOI);
                } else if($dataUser['bdm_id'] != "all") {
                    $targetPlanReport = $this->getTargetPlanAllDealerByBDM($dataUser['bdm_id']);   
                    $revenueTotalReport = $this->getActualDataAllDealerByBDM($dataUser['bdm_id'], $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataAllDealerByBDM($dataUser['bdm_id'], $this->targetNOI);   

                } else {
                    $targetPlanReport = $this->getTargetPlanAllBDMByCos(Auth::user()->id);
                    $revenueTotalReport = $this->getActualDataAllBDMByCos(Auth::user()->id, $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataAllBDMByCos(Auth::user()->id, $this->targetNOI);    

                }
            } else if($userLoggedInRole==Config::get('constants.BDM')) {
                if($dataUser['dealer_id'] != "all") {
                    $targetPlanReport = $this->getTargetPlanByDealer($dataUser['dealer_id']);
                    $revenueTotalReport = $this->getActualDataByDealer($dataUser['dealer_id'], $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataByDealer($dataUser['dealer_id'], $this->targetNOI);

                } else {
                    $targetPlanReport = $this->getTargetPlanAllDealerByBDM(Auth::user()->id);
                    $revenueTotalReport = $this->getActualDataAllDealerByBDM(Auth::user()->id, $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataAllDealerByBDM(Auth::user()->id, $this->targetNOI);    
                }
            } else if($userLoggedInRole==Config::get('constants.DEALER')) {
                $targetPlanReport = $this->getTargetPlanByDealer(Auth::user()->id);
                    $revenueTotalReport = $this->getActualDataByDealer(Auth::user()->id, $this->targetRevenue);
                    $numberOfItemTotalReport = $this->getActualDataByDealer(Auth::user()->id, $this->targetNOI); 
            }
        }
        $targetPlanReport = $targetPlanReport->first();

        $targetPlanActual = [
            'target_plan' => $targetPlanReport,
            'revenue_actual' => $revenueTotalReport,
            'number_of_item_actual' => $numberOfItemTotalReport
        ];
        $targetPlanActualObj = (object) $targetPlanActual;
        
        
        // echo "<h1>TARGET PLAN</h1>";
        // echo "<pre>";
        // print_r($targetPlanActual);
        // echo "</pre>";
        // die();


        
        
        return $targetPlanActualObj;
    }

    public function actualRevenue($dataUser, $year, $month, $isMonthToDate=true, $category) {
        $userLoggedInRole = Auth::user()->roles[0]->pivot->role_id;
        
        $monthCondition = '=';
        if(!$this->isMonthToDate) {
            $monthCondition = '<=';
        }

        $revenueTotal = $this->actualRevenueInitialQuery();
        if($userLoggedInRole==Config::get('constants.BDM') AND $userId=="all") {
            $revenueTotal->where('dealers.created_by', Auth::user()->id);
        }

        $revenueTotal = $revenueTotal->first();
        
        return $revenueTotal->revenue;    
    }
    
    public function actualNumberOfItem($userId, $year, $month, $isMonthToDate=true, $category) {
        $userLoggedInRole = Auth::user()->roles[0]->pivot->role_id;
        if($userId!="all") {
            $user = User::find($userId);
            $userRole = $user->roles[0]->pivot->role_id;
        }

        $monthCondition = '=';
        if(!$isMonthToDate) {
            $monthCondition = '<=';
        }
    
        

        if($userId != "all") {
            $numberOfItem->where('user_id', $userId);
        } else {
            if($userLoggedInRole==Config::get('constants.BDM') AND $userId=="all") {
                $numberOfItem->where('dealers.created_by', Auth::user()->id);
            }
        }

        $numberOfItem = $numberOfItem->first();

        return $numberOfItem->number_of_item;    
    }


    //PLAN

    public function targetPlanInitialQuery() {
        $targetPlanReport = AchievementDetail::select(DB::raw('SUM(target_'.$this->category.'_revenue) as revenue, SUM(target_'.$this->category.'_count) as number_of_item'))
        ->join('achievements', 'achievements.id', '=', 'achievement_details.achievement_id')
        ->whereRaw('year = ' . $this->year)
        ->whereRaw('month_id ' . $this->monthCondition . $this->month);
        return $targetPlanReport;
    }

    public function getTargetPlanByDealer($dealerId) {
        $targetPlanReport = $this->targetPlanInitialQuery();
        $targetPlanReport->where('user_id', $dealerId);
        return $targetPlanReport;
    }

    public function getTargetPlanAllDealerByBDM($bdmId) {
        $targetPlanReport = $this->targetPlanInitialQuery();
        $targetPlanReport->join('dealers', 'dealers.user_id', '=', 'achievements.user_id')
                    ->where('dealers.created_by', $bdmId);

        return $targetPlanReport;
    }

    public function getTargetPlanAllBDMByCos($cosId) {
        $targetPlanReport = $this->targetPlanInitialQuery();
        
        $targetPlanReport->join('dealers', 'dealers.user_id', '=', 'achievements.user_id')
                    ->whereIn('dealers.created_by', function($query) use ($cosId) {
                        $query->select('users.id')
                        ->from('users')
                        ->join('user_details', 'user_details.user_id', 'users.id')
                        ->where('user_details.created_by', $cosId);
                    });

        return $targetPlanReport;
    }


    //ACTUAL

    public function actualRevenueInitialQuery($isLastYear=false) {
        $year = $this->year;
        if($isLastYear) {
            $year = $this->year - 1;
        }

        $revenueTotalReport = SellThrough::select(DB::raw('SUM(revenue) as revenue'))
        ->join('dealers', 'dealers.id', '=', 'sell_through.dealer_id')
        ->join('sell_through_detail', 'sell_through.id', '=', 'sell_through_detail.sell_through_id')
        ->where('category', $this->category)
        ->whereRaw('YEAR(transaction_date) = ' . $year)
        ->whereRaw('MONTH(transaction_date) ' . $this->monthCondition . $this->month);

        return $revenueTotalReport;
    }

    public function actualNumberOfItemInitialQuery($isLastYear=false) {
        $year = $this->year;
        if($isLastYear) {
            $year = $this->year - 1;
        }

        if($this->category!="consumable") {
            $numberOfItemReport = SellThroughDetailProduct::select(DB::raw("COUNT(sell_through_detail_product.id) as number_of_item"))
            ->join('sell_through_detail', 'sell_through_detail.id', '=', 'sell_through_detail_product.sell_through_detail_id')
            ->join('sell_through', 'sell_through.id', '=', 'sell_through_detail.sell_through_id');
        } else  {
            $numberOfItemReport = SellThroughDetail::select(DB::raw("SUM(qty) as number_of_item"))
            ->join('sell_through', 'sell_through.id', '=', 'sell_through_detail.sell_through_id');
        }
        $numberOfItemReport = $numberOfItemReport->join('dealers', 'dealers.id', '=', 'sell_through.dealer_id')
            ->where('category', $this->category)
            ->whereRaw('YEAR(transaction_date) = ' . $year)
            ->whereRaw('MONTH(transaction_date) ' . $this->monthCondition . $this->month); 

        return $numberOfItemReport;
    }


    public function getActualDataByDealer($dealerId, $target) {

        if($target=="revenue") {
            $actualDataCurrentYear = $this->actualRevenueInitialQuery();
            $actualDataLastYear = $this->actualRevenueInitialQuery(true);
        } else {
            $actualDataCurrentYear = $this->actualNumberOfItemInitialQuery();
            $actualDataLastYear = $this->actualNumberOfItemInitialQuery(true);
        }
        $actualData['current_year'] = $actualDataCurrentYear->where('user_id', $dealerId)->first();
        $actualData['last_year'] = $actualDataLastYear->where('user_id', $dealerId)->first();

        return $actualData;
    }

    

    public function getActualDataAllDealerByBDM($bdmId, $target) {
        if($target=="revenue") {
            $actualDataCurrentYear = $this->actualRevenueInitialQuery();
            $actualDataLastYear = $this->actualRevenueInitialQuery(true);
        } else {
            $actualDataCurrentYear = $this->actualNumberOfItemInitialQuery();
            $actualDataLastYear = $this->actualNumberOfItemInitialQuery(true);
        }

        $actualData['current_year'] = $actualDataCurrentYear->where('dealers.created_by', $bdmId)->first();
        $actualData['last_year'] = $actualDataLastYear->where('dealers.created_by', $bdmId)->first();

        return $actualData;
    }

    public function getActualDataAllBDMByCos($cosId, $target) {
        if($target=="revenue") {
            $actualDataCurrentYear = $this->actualRevenueInitialQuery();
            $actualDataLastYear = $this->actualRevenueInitialQuery(true);
        } else {
            $actualDataCurrentYear = $this->actualNumberOfItemInitialQuery();
            $actualDataLastYear = $this->actualNumberOfItemInitialQuery(true);
        }

        
        $actualData['current_year'] = $actualDataCurrentYear->whereIn('dealers.created_by', function($query) use ($cosId) {
                        $query->select('users.id')
                        ->from('users')
                        ->join('user_details', 'user_details.user_id', 'users.id')
                        ->where('user_details.created_by', $cosId);
                    })->first();;
        $actualData['last_year'] = $actualDataLastYear->whereIn('dealers.created_by', function($query) use ($cosId) {
                        $query->select('users.id')
                        ->from('users')
                        ->join('user_details', 'user_details.user_id', 'users.id')
                        ->where('user_details.created_by', $cosId);
                    })->first();

        return $actualData;
    }





    public function totalAchievement($achievementDetails) {
        $totalRevenue = 0;

        $totalCount = 0;


        foreach ($achievementDetails as $achievementDetail) {
            $totalRevenueUnitMono = $achievementDetail->target_unitmono_revenue;
            $totalRevenueUnitColor = $achievementDetail->target_unitcolor_revenue;
            $totalRevenueCons = $achievementDetail->target_consumable_revenue;
            $totalRevenue += $totalRevenueUnitMono + $totalRevenueUnitColor + $totalRevenueCons;

            $totalCountUnitMono = $achievementDetail->target_unitmono_count;
            $totalCountUnitColor = $achievementDetail->target_unitcolor_count;
            $totalCountCons = $achievementDetail->target_consumable_count;
            $totalCount += $totalCountUnitMono + $totalCountUnitColor + $totalCountCons;

        }
        $totalAchievement = array(
            'totalRevenue' => $totalRevenue,
            'totalCount' => $totalCount
        );

        return $totalAchievement;
    }


}
