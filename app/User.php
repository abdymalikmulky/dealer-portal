<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'status_id', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function user_detail()
    {
        return $this->hasOne('App\UserDetail', 'user_id');
    }

    public function dealer()
    {
        return $this->hasOne('App\Dealer', 'user_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles');
    }
}
