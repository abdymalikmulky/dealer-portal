<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellOutDocument extends Model
{
    protected $table = 'sell_out_documents';
    protected $fillable = ['id', 'sell_out_id', 'filename'];
}
