<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellThroughDetailProduct extends Model
{
    protected $table = 'sell_through_detail_product';
    protected $fillable = ['id', 'sell_through_detail_id', 'serial_number'];

    public static function dealerProductItem($dealerId, $category) {
    	

        $dealerProductSerialNumbers = SellThrough::join('sell_through_detail', 'sell_through.id', 'sell_through_detail.sell_through_id')
        ->join('sell_through_detail_product', 'sell_through_detail.id', 'sell_through_detail_product.sell_through_detail_id')
        ->join('products', 'sell_through_detail.product_id', 'products.id')
        ->where('dealer_id', $dealerId)
        ->where('sell_through.category', $category)
        ->whereNotIn('sell_through_detail_product.id',function($query) {
		   $query->select('sell_through_detail_product_id')->from('sell_out_detail')->whereNotNull('sell_through_detail_product_id');
		})
        ->orderBy('product_id', 'asc')
        ->get();

        return $dealerProductSerialNumbers;
    }
}

?>