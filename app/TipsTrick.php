<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipsTrick extends Model
{
    protected $fillable = [
        'title', 'content', 'picture', 'created_by'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

}
