<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellOutDetail extends Model
{
    protected $table = 'sell_out_detail';
    protected $fillable = ['id', 'sell_out_id', 'product_id', 'qty', 'sell_through_detail_product_id'];

    public function product() {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function sellThroughDetailProduct() {
        return $this->belongsTo('App\SellThroughDetailProduct', 'sell_through_detail_product_id');
    }
}
