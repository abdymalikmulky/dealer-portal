<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'user_details';
    protected $fillable = ['id', 'user_id', 'NIP', 'phone', 'photo', 'address', 'information', 'created_by'];

	public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
