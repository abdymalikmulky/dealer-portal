<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class ActionInSystem extends Notification
{
    use Queueable;

    protected $sellThrough;
    protected $achievement;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function setSellThrough($sellThrough) {
        $this->sellThrough = $sellThrough;
    }

    public function setAchievement($achievement) {
        $this->achievement = $achievement;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        // return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //notification for sellthrough
        if($this->sellThrough!=null) {  
            $subject = "Sell Through Added";
            $line = $this->sellThrough->createdBy->name . 'Menambahkan Stok Product ' . $this->sellThrough->category . ' Anda';
            $actionMessage = 'Lihat Daftar Stok';
            $actionRoute = route('stockproduct', array('product_type' => $this->sellThrough->category));
        } else if($this->achievement!=null) {
            $subject = "Achievement Has Changed";
            $line = "Achievement has changed for " . $this->achievement->user->name .
            " by " . Auth::user()->name . " ON " . $this->achievement->created_at;
            $actionMessage = 'Lihat Perubahan';
            $actionRoute = route('achievement.edit',  $this->achievement->id);
        } else{
            $subject = "Notification";
            $line = 'Line Notification';
            $actionMessage = 'Action Message Notification';
            $actionRoute = route('home');
        }
        return (new MailMessage)
            ->subject($subject)
            ->line($line)
            ->action($actionMessage, $actionRoute)
            ->line('Terimakasih');
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return database
     */
    public function toDatabase($notifiable)
    {
         //notification for sellthrough
        if($this->sellThrough!=null) {  
            $notificationObject = [
                'sellthrough' => $this->sellThrough,
                'user' => Auth::user()
            ];
        } else{
            $notificationObject = [
                'achievement' => $this->achievement,
                'user' => Auth::user()
            ];
        }

        return $notificationObject;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
