<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesTool extends Model
{
    protected $fillable = [
        'title', 'description', 'file', 'created_by'
    ];
}
