<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementDetail extends Model
{
    protected $fillable = ['achievement_id', 'month_id', 'year', 'target_unitmono_revenue', 'target_unitcolor_revenue', 'target_consumable_revenue', 'target_unitmono_count', 'target_unitcolor_count'];


}
