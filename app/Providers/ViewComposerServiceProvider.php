<?php
/**
 * Created by PhpStorm.
 * User: abdymalikmulky
 * Date: 10/8/17
 * Time: 5:40 PM
 */

namespace App\Providers;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot() {
        view()->composer('*',"App\Http\ViewComposers\ProductViewComposer");
        view()->composer('*',"App\Http\ViewComposers\MessageViewComposer");
    }

    public function register() {

    }

}