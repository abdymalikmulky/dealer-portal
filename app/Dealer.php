<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
    protected $fillable = ['user_id', 'created_by', 'company_name', 'company_city', 'company_address', 'company_phone', 'company_type', 'company_photo', 'company_websites', 'PIC_name', 'PIC_email', 'PIC_phone',  'PIC_jabatan', 'PIC2_name', 'PIC2_email', 'PIC2_phone', 'PIC2_jabatan'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
