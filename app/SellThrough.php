<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Config;
use Illuminate\Support\Facades\Auth;
use App\SellThroughDetail;
use App\SellThroughDetailProduct;

class SellThrough extends Model
{
    protected $table = 'sell_through';
    protected $fillable = ['id', 'dealer_id', 'is_init_stock', 'category', 'transaction_date', 'created_by'];

    public function sellthrough_products()
    {
        return $this->hasMany('App\SellThroughDetail', 'sell_through_id');
    }

    public function sellthroughRevenue() {
        $revenue = 0;
        $sellThroughDetail = $this->hasMany('App\SellThroughDetail', 'sell_through_id')->get();
        foreach ($sellThroughDetail as $key => $value) {
            $revenue += $value->revenue;
        }
        return $revenue;
    }

    public function sellthroughQTY() {
        $qty = 0;
        $sellThroughDetail = $this->hasMany('App\SellThroughDetail', 'sell_through_id')->get();
        foreach ($sellThroughDetail as $key => $value) {
            $qty += $value->qty;
        }
        return $qty;
    }

    public function dealer() {
    	return $this->belongsTo('App\Dealer', 'dealer_id');
    }

    public function createdBy() {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function productCategory($category) {
         return ProductCategories::where('category', $category)->first();
    }

    


}
