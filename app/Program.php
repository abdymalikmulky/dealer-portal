<?php
/**
 * Created by PhpStorm.
 * User: abdymalikmulky
 * Date: 10/7/17
 * Time: 9:37 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Program extends Model
{
    protected $fillable = ['user_id', 'name', 'type', 'desc', 'start', 'end', 'status_id'];

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function ligaResellers()
    {
        return $this->belongsToMany('App\Product', 'liga_reseller_product')->withPivot('id', 'point');
    }

}{}